﻿using System.Collections.Generic;
using NUnit.Framework;
using YAPM;
using YAPM.Configuration;

namespace Core.Tests.DependencyResolver
{
    public class Specs_For_Version_Resolution
    {
        [TestFixture]
        public class Given_A_Simple_Tree : DependencyResolverBase
        {
            [Test]
            public void It_Can_Resolve_A_Flat_Tree()
            {
                var foo = CreatePackageAndSpec("Foo", "1.0");
                var bar = CreatePackageAndSpec("Bar", "1.0");

                var packageSet = new HashSet<PackageSpec>{foo.Spec, bar.Spec};
                var resolver = new YAPM.Graph.DependencyResolver(packageSet, Source);
                Assert.That(resolver.IsResolvable);
                Assert.That(resolver.ResolvedDependencies, Has.Count.EqualTo(2));
                Assert.That(resolver.ResolvedDependencies, Contains.Item(foo.Package));
                Assert.That(resolver.ResolvedDependencies, Contains.Item(bar.Package));
            }

            [Test]
            public void It_Can_Resolve_A_Simple_Tree()
            {
                var bar = CreatePackageAndSpec("Bar", "1.0");
                var foo = CreatePackageAndSpec("Foo", "1.0", ConstraintType.Fixed, bar);

                var packageSet = new HashSet<PackageSpec>{ foo };
                var resolver = new YAPM.Graph.DependencyResolver(packageSet, Source);
                Assert.That(resolver.IsResolvable);
                Assert.That(resolver.ResolvedDependencies, Has.Count.EqualTo(2));
                Assert.That(resolver.ResolvedDependencies, Contains.Item(bar.Package));
                Assert.That(resolver.ResolvedDependencies, Contains.Item(foo.Package));
            }

            [Test]
            public void It_Can_Resolve_A_Tree_With_An_Upgrade()
            {
                var bar = CreatePackageAndSpec("Bar", "1.0", ConstraintType.AnyGreater);
                var bar12 = CreatePackageAndSpec("Bar", "1.2", ConstraintType.AnyGreater);
                var foo = CreatePackageAndSpec("Foo", "1.0", ConstraintType.Fixed, bar12);

                var packageSet = new HashSet<PackageSpec> { foo, bar };
                var resolver = new YAPM.Graph.DependencyResolver(packageSet, Source);
                Assert.That(resolver.IsResolvable);
                Assert.That(resolver.ResolvedDependencies, Has.Count.EqualTo(2));
                Assert.That(resolver.ResolvedDependencies, Contains.Item(foo.Package));
                Assert.That(resolver.ResolvedDependencies, Contains.Item(bar12.Package));
            }
        }

        [TestFixture]
        public class Given_A_Tree_That_Will_Fail : DependencyResolverBase
        {
            [Test]
            public void It_Will_Fail_When_Version_Constrains_Conflict()
            {
                var bar = CreatePackageAndSpec("Bar", "1.0");
                var bar12 = CreatePackageAndSpec("Bar", "1.2");
                var foo = CreatePackageAndSpec("Foo", "1.0", ConstraintType.Fixed, bar12);

                var packageSet = new HashSet<PackageSpec> { foo, bar };
                var resolver = new YAPM.Graph.DependencyResolver(packageSet, Source);
                Assert.That(resolver.IsResolvable, Is.False);
            }

        }
    }
}
