﻿using System.Linq;
using NUnit.Framework;
using YAPM;
using YAPM.Configuration;
using YAPM.Package;
using YAPM.Source;

namespace Core.Tests.DependencyResolver
{
    public struct PackageAndSpec
    {
        public PackageSpec Spec;
        public IPackage Package;

        public static implicit operator PackageSpec(PackageAndSpec me)
        {
            return me.Spec;
        }
    }

    /// <summary>
    /// Base class for the dependency resolver tests. Provides convinience methods for constructing
    /// PackageSpecs, IPackages and an IPackageSource that automatically resolves the spec/package pairs.
    /// </summary>
    public class DependencyResolverBase
    {
        private Moq.Mock<IPackageSource> _backingSource;

        public IPackageSource Source
        {
            get { return _backingSource.Object; }
        }

        [SetUp]
        public void SetupMoq()
        {
            _backingSource = new Moq.Mock<IPackageSource>();
        }

        protected PackageAndSpec CreatePackageAndSpec(string id, string version, ConstraintType constraintType = ConstraintType.Fixed, params PackageAndSpec[] dependencies )
        {
            var spec = new PackageSpec { PackageId = id, Version = new VersionConstraint(version, constraintType), Satisfier = new AssemblyPackageSatisifier() };
            var package = new Moq.Mock<LocalPackage>();
            dependencies = dependencies ?? new PackageAndSpec[0];
            var depList = dependencies.Select(d => d.Package.Spec).ToList();
            package.SetupGet(d => d.Dependencies).Returns(depList);
            package.SetupGet(d => d.Spec).Returns(spec);

            _backingSource.Setup(s => s.GetPackage(spec.PackageId, new SemanticVersion(version), null)).Returns(package.Object);

            return new PackageAndSpec {Spec = spec, Package = package.Object};
        }
    }
}
