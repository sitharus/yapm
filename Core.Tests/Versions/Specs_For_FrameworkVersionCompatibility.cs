﻿using System;
using System.Runtime.Versioning;
using YAPM.Core.Utilities;
using NUnit.Framework;

namespace Core.Tests.Versions
{
    class Specs_For_FrameworkVersionCompatibility
    {

        static readonly FrameworkName One = new FrameworkName(".NETFramework", Version.Parse("1.0.3705"));
        static readonly FrameworkName OnePointOne = new FrameworkName(".NETFramework", Version.Parse("1.1.4322"));
        static readonly FrameworkName OneClient = new FrameworkName(".NETFramework", Version.Parse("1.0.3705"), "Client");
        static readonly FrameworkName Two = new FrameworkName(".NETFramework", Version.Parse("2.0.50727"));
        static readonly FrameworkName Three = new FrameworkName(".NETFramework", Version.Parse("3.0"));
        static readonly FrameworkName ThreePointFive = new FrameworkName(".NETFramework", Version.Parse("3.5"));
        static readonly FrameworkName Four = new FrameworkName(".NETFramework", Version.Parse("4.0.30319"));

        [TestFixture]
        [Category("Framework compatibility")]
        public class Given_Version_One_Source_Framework
        {
            [Test]
            public void It_Should_Be_Compatible_With_Version_One()
            {
                Assert.That(FrameworkVersionCompatibility.IsCompatible(One, OnePointOne));
            }

            [Test]
            public void It_Should_Not_Be_Compatible_With_Full_Profile_If_It_Is_Client()
            {
                Assert.That(FrameworkVersionCompatibility.IsCompatible(OnePointOne, OneClient), Is.False);
            }

            [Test]
            public void It_Should_Be_Compatible_With_Client_Profile()
            {
                Assert.That(FrameworkVersionCompatibility.IsCompatible(OneClient, One));
            }

            [Test]
            public void It_Should_Not_Be_Compatible_With_Version_Two()
            {
                Assert.That(FrameworkVersionCompatibility.IsCompatible(One, Two), Is.False);
            }
        }

        [TestFixture]
        public class Given_Version_Two_Source_Framework
        {
            [Test]
            public void It_Should_Be_Compatible_With_Version_Three()
            {
                Assert.That(FrameworkVersionCompatibility.IsCompatible(Two, Three));
            }

            [Test]
            public void It_Should_Be_Compatible_With_Version_Three_Point_Five()
            {
                Assert.That(FrameworkVersionCompatibility.IsCompatible(Two, ThreePointFive));
            }

            [Test]
            public void It_Should_Not_Be_Compatible_With_Version_Four()
            {
                Assert.That(FrameworkVersionCompatibility.IsCompatible(Two, Four), Is.False);
            }

            [Test]
            public void It_Should_Not_Be_Compatible_With_Version_One()
            {
                Assert.That(FrameworkVersionCompatibility.IsCompatible(Two, Four), Is.False);
            }
        }

        [TestFixture]
        public class Given_A_Version_Four_Source_Framework
        {
            [Test]
            public void It_Should_Be_Compatible_With_Version_3_Point_5()
            {
                Assert.That(FrameworkVersionCompatibility.IsCompatible(Four, ThreePointFive));
            }

            [Test]
            public void It_Should_Be_Compatible_With_Version_3()
            {
                Assert.That(FrameworkVersionCompatibility.IsCompatible(Four, Three));
            }

            [Test]
            public void It_Should_Be_Compatible_With_Version_2()
            {
                Assert.That(FrameworkVersionCompatibility.IsCompatible(Four, Two));
            }

            [Test]
            public void It_Should_Not_Be_Compatible_With_Version_1()
            {
                Assert.That(FrameworkVersionCompatibility.IsCompatible(Four, One), Is.False);
            }
        }

        [TestFixture]
        public class Given_A_List_Of_Possible_Versions
        {

            [Test]
            public void It_Should_Return_The_Highest()
            {
                var highest = FrameworkVersionCompatibility.HighestCompatibleVersion(Four, new[] { One, Two, Four, Three });
                Assert.That(highest, Is.EqualTo(Four));
            }
        }
    }
}
