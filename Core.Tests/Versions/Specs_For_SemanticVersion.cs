﻿using NUnit.Framework;
using YAPM;

namespace Specs_For_SemanticVersion
{
    public class Specs_For_SemanticVersion
    {
        [TestFixture]
        public class Given_A_Two_Different_Major_Versions
        {
            SemanticVersion v1 = new SemanticVersion("1.0.0.0");
            SemanticVersion v2 = new SemanticVersion("2.0.0.0");

            [Test]
            public void They_Should_Not_Be_Equal()
            {
                Assert.That(v1, Is.Not.EqualTo(v2));
            }

            [Test]
            public void V1_Should_Be_Less_Than_V2()
            {
                Assert.That(v1, Is.LessThan(v2));
            }
        }

        [TestFixture]
        public class Given_A_Two_Different_Minor_Versions
        {
            SemanticVersion v1 = new SemanticVersion("1.1.0.0");
            SemanticVersion v2 = new SemanticVersion("1.2.0.0");

            [Test]
            public void They_Should_Not_Be_Equal()
            {
                Assert.That(v1, Is.Not.EqualTo(v2));
            }

            [Test]
            public void V1_Should_Be_Less_Than_V2()
            {
                Assert.That(v1, Is.LessThan(v2));
            }
        }

        [TestFixture]
        public class Given_A_Two_Different_Build_Versions
        {
            SemanticVersion v1 = new SemanticVersion("1.1.1.0");
            SemanticVersion v2 = new SemanticVersion("1.1.2.0");

            [Test]
            public void They_Should_Not_Be_Equal()
            {
                Assert.That(v1, Is.Not.EqualTo(v2));
            }

            [Test]
            public void V1_Should_Be_Less_Than_V2()
            {
                Assert.That(v1, Is.LessThan(v2));
            }
        }

        [TestFixture]
        public class Given_A_Two_Different_SubBuild_Versions
        {
            SemanticVersion v1 = new SemanticVersion("1.1.1.1");
            SemanticVersion v2 = new SemanticVersion("1.1.1.2");

            [Test]
            public void They_Should_Not_Be_Equal()
            {
                Assert.That(v1, Is.Not.EqualTo(v2));
            }

            [Test]
            public void V1_Should_Be_Less_Than_V2()
            {
                Assert.That(v1, Is.LessThan(v2));
            }
        }

        [TestFixture]
        public class Given_A_Two_Completely_Different_Versions
        {
            SemanticVersion v1 = new SemanticVersion("1.2.3.4");
            SemanticVersion v2 = new SemanticVersion("2.3.1.2");

            [Test]
            public void They_Should_Not_Be_Equal()
            {
                Assert.That(v1, Is.Not.EqualTo(v2));
            }

            [Test]
            public void V1_Should_Be_Less_Than_V2()
            {
                Assert.That(v1, Is.LessThan(v2));
            }
        }
    }
}