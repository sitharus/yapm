﻿using NUnit.Framework;
using YAPM;

namespace Specs_For_VersionConstraint
{
    public class VersionConstraintComparisons
    {
        public class Given_A_Greater_Constraint
        {
            static VersionConstraint c = new VersionConstraint("1.2.3.4", ConstraintType.AnyGreater);

            [TestFixture]
            public class And_The_Same_SemanticVersion
            {
                SemanticVersion v = new SemanticVersion("1.2.3.4");

                [Test]
                public void They_Should_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.True);
                }
            }

            [TestFixture]
            public class And_A_Higher_Major_Version
            {
                SemanticVersion v = new SemanticVersion("2.2.3.4");

                [Test]
                public void They_Should_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.True);
                }
            }

            [TestFixture]
            public class And_A_Higher_Minor_Version
            {
                SemanticVersion v = new SemanticVersion("1.3.3.4");

                [Test]
                public void They_Should_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.True);
                }
            }

            [TestFixture]
            public class And_A_Higher_Build_Version
            {
                SemanticVersion v = new SemanticVersion("1.2.4.4");

                [Test]
                public void They_Should_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.True);
                }
            }

            [TestFixture]
            public class And_A_Higher_SubBuild_Version
            {
                SemanticVersion v = new SemanticVersion("1.2.3.5");

                [Test]
                public void They_Should_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.True);
                }
            }

            [TestFixture]
            public class And_A_Lower_Major_Version
            {
                SemanticVersion v = new SemanticVersion("0.2.3.4");

                [Test]
                public void They_Should_Not_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.False);
                }
            }

            [TestFixture]
            public class And_A_Lower_Minor_Version
            {
                SemanticVersion v = new SemanticVersion("1.1.3.4");

                [Test]
                public void They_Should_Not_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.False);
                }
            }

            [TestFixture]
            public class And_A_Lower_Build_Version
            {
                SemanticVersion v = new SemanticVersion("1.2.2.4");

                [Test]
                public void They_Should_Not_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.False);
                }
            }

            [TestFixture]
            public class And_A_Lower_SubBuild_Version
            {
                SemanticVersion v = new SemanticVersion("1.2.3.3");

                [Test]
                public void They_Should_Not_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.False);
                }
            }
        }

        public class Given_A_SameMajor_Constraint
        {
            static VersionConstraint c = new VersionConstraint("1.2.3.4", ConstraintType.SameMajor);

            [TestFixture]
            public class And_The_Same_SemanticVersion
            {
                SemanticVersion v1 = new SemanticVersion("1.2.3.4");

                [Test]
                public void They_Should_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v1), Is.True);
                }
            }

            [TestFixture]
            public class And_A_SemanticVersion_With_a_Different_Major_Version
            {
                SemanticVersion v = new SemanticVersion("2.2.3.4");

                [Test]
                public void They_Should_Not_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.False);
                }
            }

            [TestFixture]
            public class And_A_SemanticVersion_With_a_Higher_Minor_Version
            {
                SemanticVersion v = new SemanticVersion("1.3.3.4");

                [Test]
                public void They_Should_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.True);
                }
            }

            [TestFixture]
            public class And_A_SemanticVersion_With_a_Lower_Minor_Version
            {
                SemanticVersion v = new SemanticVersion("1.1.3.4");

                [Test]
                public void They_Should_Not_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.False);
                }
            }

            [TestFixture]
            public class And_A_SemanticVersion_With_a_Lower_Build_Version
            {
                SemanticVersion v = new SemanticVersion("1.2.2.4");

                [Test]
                public void They_Should_Not_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.False);
                }
            }

            [TestFixture]
            public class And_A_SemanticVersion_With_a_Lower_SubBuild_Version
            {
                SemanticVersion v = new SemanticVersion("1.2.2.2");

                [Test]
                public void They_Should_Not_Be_Compatible()
                {
                    Assert.That(c.CompatibleWith(v), Is.False);
                }
            }
        }

        public class Given_A_Fixed_Constraint
        {
            static VersionConstraint c1 = new VersionConstraint("1.2.3.4", ConstraintType.Fixed);

            [TestFixture]
            public class And_The_Same_SemanticVersion
            {
                SemanticVersion v1 = new SemanticVersion("1.2.3.4");

                [Test]
                public void They_Should_Be_Compatible()
                {
                    Assert.That(c1.CompatibleWith(v1), Is.True);
                }
            }

            [TestFixture]
            public class And_A_Different_SemanticVersion
            {
                SemanticVersion v2 = new SemanticVersion("2.3.4.5");

                [Test]
                public void They_Should_Not_Be_Compatible()
                {
                    Assert.That(c1.CompatibleWith(v2), Is.False);
                }
            }
        }
    }
}