# Yet Another Package Manager

(aka I shouldn't be allowed to name things)

This is a Visual Studio integrated Nuget-compatible dependency package
manager that can cope with more complex dependency graphs than NuGet,
especially where you have a solution that depends on projects in other
solutions that depend on projects in the current solution (don't ask).

It does this by dividing the dependencies in to two main lists, the
direct dependency list and the resolved dependency list. The former
lists the dependencies required at compile time and the latter
describes all the packages required at runtime.

The core assumption is that a solution will only reference a single
version of a package, so package versions are versioned globally. Only
one version of a package will exist on disk at a time

## Features we do have

 * Deep dependency analysis.
 * Integration with nuget server.
 * Ability to fix package version either to a set version or to a set
   major version.
 * Support for content in nuget packages in addition to DLL references.
     Unlike nuget we always overwrite content. You're using source control
     right?
 * Ability to build nupkgs with a correct list of direct dependencies.
 * Visual Studio extension to install and manage packages

## Features we have that Nuget doesn't

 * Package reference fix-up.
    Since visual studio sometimes messes up
    the hint paths there's an integrated function to check all
    package references are correct.
 * A pretty graph so you can see the interdependencies.
 
## Features Nuget has that we don't

 * Installation scripts
 * Powershell integration
 * CLI use
 
## Things we're working on
  
 * Improving the VS integration
   * Ability to see which projects in the solution depend on which
     package.
     
 
 
