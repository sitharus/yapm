using System;
using System.Linq;
using System.Reflection;
using Microsoft.Build.Evaluation;

namespace YAPM.Common
{
    static public class MSBuildHelpers
    {
        /// <summary>
        /// Finds the MSBuild project file given a Visual Studio project file
        /// </summary>
        /// <param name="fullPath">The full path to the project file</param>
        /// <returns></returns>
        public static Project FindMSBuildProject(string fullPath)
        {
            if (String.IsNullOrEmpty(fullPath))
            {
                return null;
            }
            var msbuildProject = ProjectCollection.GlobalProjectCollection.GetLoadedProjects(fullPath).FirstOrDefault()
                                 ?? ProjectCollection.GlobalProjectCollection.LoadProject(fullPath);
            return msbuildProject;
        }
    }
}