﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.Build.Evaluation;

namespace YAPM.Common
{
    public static class MSBuildExtensions
    {
        public static IEnumerable<Tuple<ProjectItem, AssemblyName>> GetAssemblyReferences(this Project project)
        {
            var assemblyNames = new List<Tuple<ProjectItem, AssemblyName>>();
            foreach (var referenceProjectItem in project.GetItems("Reference"))
            {
                AssemblyName assemblyName = null;
                try
                {
                    assemblyName = new AssemblyName(referenceProjectItem.EvaluatedInclude);
                }
                catch (Exception exception)
                {
                    // Swallow any exceptions we might get because of malformed assembly names
                }

                // We can't yield from within the try so we do it out here if everything was successful
                if (assemblyName != null)
                {
                    assemblyNames.Add(Tuple.Create(referenceProjectItem, assemblyName));
                }
            }
            return assemblyNames;
        }
    }
}
