﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using YAPM.Source;
using YAPM.YAPM_Extension.ViewModels;

namespace YAPM.YAPM_Extension
{
    /// <summary>
    /// Interaction logic for MyControl.xaml
    /// </summary>
    public partial class PackageManagerControl : INotifyPropertyChanged
    {
        private readonly PackageControlModel _Model;

        public List<PackageSearchResultModel> SearchResults
        {
            get; private set;
        }

        public PackageControlModel Model {
            get { return _Model; }
        }


        public PackageManagerControl(PackageControlModel model)
        {
            _Model = model;
            DataContext = model;
            InitializeComponent();
            NotifyPropertyChanged("SearchResults");
        }

        private void YAPMSearchButton_Click(object sender, RoutedEventArgs e)
        {
            var context = TaskScheduler.FromCurrentSynchronizationContext();
            var progress = FindName("YAPMSearchProgress") as ProgressBar;
            progress.Visibility = Visibility.Visible;
            var comp = FindName("YAPMSearch") as TextBox;
            var value = comp.Text;
            var taskFactory = new TaskFactory<List<PackageSearchResultModel>>();
            var task = taskFactory.StartNew(() =>
                {
                    var packages = ServiceLocator.GetService<IPackageSource>().SearchPackage(value);
                    return packages.Select(p => new PackageSearchResultModel(p, _Model)).ToList();
                });

            task.ContinueWith(t =>
            {
                progress.Visibility = Visibility.Hidden;
                if (task.Exception != null)
                {

                }
                else
                {
                    SearchResults = t.Result;
                    NotifyPropertyChanged("SearchResults");
                }
            }, context);
        }

        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}