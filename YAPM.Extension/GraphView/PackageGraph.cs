﻿using System.Linq;
using GraphSharp;
using GraphSharp.Controls;
using QuickGraph;
using YAPM.Graph;

namespace YAPM.YAPM_Extension.GraphView
{
    public class PackageVertex
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public bool Installed { get; set; }
    }

    public class PackageEdge : Edge<PackageVertex>
    {
        public bool CausesCycle { get; set; }

        public PackageEdge(PackageVertex source, PackageVertex target)
            : base(source, target)
        {
        }
    }

    public class NodeGraph : BidirectionalGraph<PackageVertex, PackageEdge>
    {
        public static NodeGraph FromDependencyGraph(DependencyResolver deps)
        {
            var graph = new NodeGraph();
            var topLevel = deps.DirectDependencies.Select(p => p.PackageId).ToArray();

            foreach (var vertex in deps.Graph.Vertices)
            {
                graph.AddVertex(new PackageVertex
                    {
                        Name = vertex.PackageId,
                        Version = vertex.PackageSpec.Version.BaseVersion.ToString(),
                        Installed = topLevel.Contains(vertex.PackageId)
                    });
            }

            foreach (var edge in deps.Graph.Edges)
            {
                var source = graph.Vertices.First(v => v.Name == edge.Source.PackageId);
                var target = graph.Vertices.First(v => v.Name == edge.Target.PackageId);
                graph.AddEdge(new PackageEdge(source, target));
            }

            return graph;
        }
    }

    public class NodeGraphLayout : GraphLayout<PackageVertex, PackageEdge, NodeGraph>
    {
    }
}
