﻿// Guids.cs
// MUST match guids.h
using System;

namespace YAPM.YAPM_Extension
{
    static class GuidList
    {
        public const string guidYAPM_ExtensionPkgString = "f26662e8-a04d-46ae-bb5b-c97901784818";
        public const string guidYAPM_ExtensionCmdSetString = "acc40ba5-943a-45c7-8ba6-8c13c529e625";
        public const string guidToolWindowPersistanceString = "31229ede-b13c-42c6-b1c9-200e2ebd4001";

        public static readonly Guid guidYAPM_ExtensionCmdSet = new Guid(guidYAPM_ExtensionCmdSetString);
    };
}