﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using NLog;
using NLog.Config;
using NLog.Targets;
using YAPM.Utilities;
using YAPM.YAPM_Extension.Support;

namespace YAPM.YAPM_Extension.Views
{
    class ProgressViewTarget : Target
    {
        private ProgressView _View;

        public ProgressViewTarget(ProgressView view)
        {
            _View = view;
        }

        protected override void Write(LogEventInfo logEvent)
        {
            if (_View != null)
            {
                Execute.OnUIThread(() => _View.AppendLog(logEvent));
            }
        }

        public void Stop()
        {
            _View = null;
        }
    }

    /// <summary>
    /// Interaction logic for ProgressView.xaml
    /// </summary>
    public partial class ProgressView
    {
        private bool _Done;
        private readonly ProgressViewTarget _Target;
        private readonly LoggingRule _ExtensionLoggingRule;
        private readonly LoggingRule _ManagementLoggingRule;
        private LoggingRule _GraphLoggingRule;

        public ProgressView()
        {
            InitializeComponent();
            var config = LogManager.Configuration;
            _Target = new ProgressViewTarget(this);
            _ExtensionLoggingRule = new LoggingRule("YAPM.YAPM_Extension.*", LogLevel.Trace, _Target);
            _ManagementLoggingRule = new LoggingRule("YAPM.Management.*", LogLevel.Info, _Target);
            _GraphLoggingRule = new LoggingRule("YAPM.Graph.*", LogLevel.Info, _Target);
            config.LoggingRules.Add(_ExtensionLoggingRule);
            config.LoggingRules.Add(_ManagementLoggingRule);
            config.LoggingRules.Add(_GraphLoggingRule);
            LogManager.Configuration = config;
            Closing += HandleClose;
            var hwnd = new WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
        }

        public void Finished()
        {
            Execute.OnUIThread(() =>
                {
                    _Done = true;
                    Close();
                });
        }

        private void HandleClose(object sender, CancelEventArgs e)
        {
            if (_Done)
            {
                _Target.Stop();
                var config = LogManager.Configuration;
                config.LoggingRules.Remove(_ExtensionLoggingRule);
                config.LoggingRules.Remove(_ManagementLoggingRule);
                config.LoggingRules.Remove(_GraphLoggingRule);
                LogManager.Configuration = config;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        public void AppendLog(LogEventInfo logEvent)
        {
            Progress.AppendText(logEvent.FormattedMessage);
            Progress.AppendText("\n");
            Progress.ScrollToEnd();
        }
    }
}
