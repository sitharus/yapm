using System.Collections.Generic;
using System.Runtime.Versioning;
using EnvDTE;
using YAPM.Core.Utilities;
using YAPM.YAPM_Extension.Support;

namespace YAPM.YAPM_Extension
{
    static internal class Utilities
    {
        public static FrameworkName TargetFramework(Project project)
        {
            FrameworkName targetFramework = null;
            string targetVersion = null;
            string targetProfile = null;
            foreach (Property prop in project.Properties)
            {
                if (prop.Name.Contains("TargetFrameworkVersion"))
                {
                    targetVersion = (string) prop.Value;
                }
                else if (prop.Name.Contains("TargetFrameworkProfile"))
                {
                    targetProfile = (string)prop.Value;
                }
                else if (prop.Name.Contains("TargetFrameworkMoniker"))
                {
                    targetFramework = new FrameworkName((string) prop.Value);
                    break;
                }
            }
            if (targetVersion != null)
            {
                return FrameworkVersionCompatibility.ParseVersionFromProject(targetVersion, targetProfile);
            }
            return targetFramework;
        }

        public static IEnumerable<Project> AllSolutionProjects(Solution solution)
        {
            foreach (Project project in solution.Projects)
            {
                yield return project;
                foreach (var subproject in SubProjects(project.ProjectItems))
                {
                    yield return subproject;
                }
            }
        }

        private static IEnumerable<Project> SubProjects(ProjectItems items)
        {
            if (items == null) yield break;
            foreach (ProjectItem subItem in items)
            {
                if (subItem.SubProject != null) yield return subItem.SubProject;
                foreach (var subsubproject in SubProjects(subItem.ProjectItems))
                {
                    yield return subsubproject;
                }
            }
        }
    }
}