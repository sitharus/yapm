﻿using Ninject;
using Ninject.Activation;
using Ninject.Modules;
using YAPM.Source;
using EnvDTE;
using YAPM.YAPM_Extension.ViewModels;

namespace YAPM.YAPM_Extension
{
    public static class ServiceLocator
    {
        private static IKernel _kernel;
        private static PackageManagerModule _packageModule;
        private static string _serverUrl;

        public static void InitaliseServiceLocator(DTE environment)
        {
            if (_kernel == null)
            {
                _packageModule = new PackageManagerModule(environment);
                _kernel = new StandardKernel();
                _kernel.Load(_packageModule);
            }
            else
            {
                _packageModule.UpdateEnvironment(environment);
            }
        }

        public static T GetService<T>()
        {
            if (_kernel == null)
            {
                throw new System.InvalidOperationException("You haven't initialised the service environment");
            }
            return _kernel.Get<T>();
        }
    }

    public class PackageManagerModule : NinjectModule
    {
        private DTE _Environment;
        private static CachingPackageSource _source;
        private static string _packageServer;

        public PackageManagerModule(DTE environment)
        {
            _Environment = environment;
        }

        public void UpdateEnvironment(DTE environment)
        {
            _Environment = environment;
        }

        public override void Load()
        {
            Bind<IPackageSource>().ToMethod(GetPackageSource);
            Bind<PackageControlModel>().ToSelf().InSingletonScope();
            Bind<DTE>().ToConstant(_Environment);
        }

        private IPackageSource GetPackageSource(IContext arg)
        {
            var props = _Environment.Properties[YAPM_ExtensionPackage.OptionsMainGroup, YAPM_ExtensionPackage.OptionsGeneral];
            var server = (string)props.Item(OptionsPane.PackageServerOption).Value;
            if (server != _packageServer)
            {
                _packageServer = server;
                _source = new CachingPackageSource(new RemotePackageSource(server));
            }
            return _source;
        }
    }
}
