﻿using EnvDTE;
using Microsoft.VisualStudio.Shell;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace YAPM.YAPM_Extension
{
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [CLSCompliant(false), ComVisible(true)]
    public class OptionsPane : DialogPage
    {
        public static string PackageServerOption = "PackageServer";

        private string _packageServer = "https://nuget.org/api/v2/";

        [Category("Servers")]
        [DisplayName("Package Server")]
        [Description("Full URL of the server to get packages from")]
        public string PackageServer
        {
            get
            {
                return _packageServer;
            }
            set { 
                _packageServer = value;
                ServiceLocator.InitaliseServiceLocator((DTE)GetService(typeof(DTE)));
            }
        }
        
    }
}
