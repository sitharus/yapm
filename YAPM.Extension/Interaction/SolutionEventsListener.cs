﻿using System;
using System.Diagnostics;
using EnvDTE;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell.Interop;

namespace YAPM.YAPM_Extension.Interaction
{
    public class SolutionEventsListener : IVsSelectionEvents, IVsSolutionEvents, IDisposable
    {
        private readonly IVsMonitorSelection _MonitorSelectionService;
        private readonly uint _SolutionExistsCookie;
        private uint _SelectionEventsCookie;
        private IVsSolution _Solution;
        private uint _SolutionEventsCookie;
        private Project _CurrentProject;

        public SolutionEventsListener()
        {
            InitNullEvents();

            Guid solutionExists = VSConstants.UICONTEXT_SolutionExists;
            _MonitorSelectionService = Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof (SVsShellMonitorSelection)) as IVsMonitorSelection;

            if (_MonitorSelectionService == null)
            {
                throw new InvalidOperationException("Could not get VS shell monitor.");
            }
            _MonitorSelectionService.GetCmdUIContextCookie(ref solutionExists, out _SolutionExistsCookie);

            _MonitorSelectionService.AdviseSelectionEvents(this, out _SelectionEventsCookie);

            _Solution = Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof (SVsSolution)) as IVsSolution;

            if (_Solution != null)
            {
                _Solution.AdviseSolutionEvents(this, out _SolutionEventsCookie);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_Solution != null && _SolutionEventsCookie != 0)
            {
                GC.SuppressFinalize(this);
                if (_Solution != null)
                {
                    _Solution.UnadviseSolutionEvents(_SolutionEventsCookie);
                }
                AfterSolutionLoaded = null;
                BeforeSolutionClosed = null;
                AfterProjectLoaded = null;
                _SolutionEventsCookie = 0;
                _Solution = null;
            }

            if (_SelectionEventsCookie != 0)
            {
                GC.SuppressFinalize(this);
                _MonitorSelectionService.UnadviseSelectionEvents(_SelectionEventsCookie);
                _SelectionEventsCookie = 0;
            }
        }

        #endregion

        #region IVsSelectionEvents Members

        public int OnSelectionChanged(IVsHierarchy pHierOld, uint itemidOld, IVsMultiItemSelect pMISOld, ISelectionContainer pSCOld, IVsHierarchy pHierNew, uint itemidNew,
            IVsMultiItemSelect pMISNew, ISelectionContainer pSCNew)
        {
            var project = GetProject(pHierNew);
            if (project != _CurrentProject)
            {
                _CurrentProject = project;
                OnProjectSelectionChanged(project);
            }

            return VSConstants.S_OK;
        }

        public int OnElementValueChanged(uint elementid, object varValueOld, object varValueNew)
        {
            return VSConstants.S_OK;
        }

        public int OnCmdUIContextChanged(uint dwCmdUICookie, int fActive)
        {
            int active;
            _MonitorSelectionService.IsCmdUIContextActive(_SolutionExistsCookie, out active);

            Trace.WriteLine("Solution Exists: " + active);

            if (active != 0 && _SolutionEventsCookie == 0)
            {
                // The solution is loaded. Now we can get hold of our selection events. 
                _Solution = Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof (SVsSolution)) as IVsSolution;
                Debug.Assert(_Solution != null);

                _Solution.AdviseSolutionEvents(this, out _SolutionEventsCookie);
            }
            else if (active == 0 && _Solution != null)
            {
                _Solution.UnadviseSolutionEvents(_SolutionEventsCookie);
                _SolutionEventsCookie = 0;
            }
            return VSConstants.S_OK;
        }

        #endregion

        #region IVsSolutionEvents Members

        int IVsSolutionEvents.OnAfterCloseSolution(object pUnkReserved)
        {
            return VSConstants.S_OK;
        }

        int IVsSolutionEvents.OnAfterLoadProject(IVsHierarchy pStubHierarchy, IVsHierarchy pRealHierarchy)
        {
            return VSConstants.S_OK;
        }

        int IVsSolutionEvents.OnAfterOpenProject(IVsHierarchy pHierarchy, int fAdded)
        {
            AfterProjectLoaded(pHierarchy);
            return VSConstants.S_OK;
        }

        int IVsSolutionEvents.OnAfterOpenSolution(object pUnkReserved, int fNewSolution)
        {
            AfterSolutionLoaded();
            return VSConstants.S_OK;
        }

        int IVsSolutionEvents.OnBeforeCloseProject(IVsHierarchy pHierarchy, int fRemoved)
        {
            return VSConstants.S_OK;
        }

        int IVsSolutionEvents.OnBeforeCloseSolution(object pUnkReserved)
        {
            BeforeSolutionClosed();
            return VSConstants.S_OK;
        }

        int IVsSolutionEvents.OnBeforeUnloadProject(IVsHierarchy pRealHierarchy, IVsHierarchy pStubHierarchy)
        {
            return VSConstants.S_OK;
        }

        int IVsSolutionEvents.OnQueryCloseProject(IVsHierarchy pHierarchy, int fRemoving, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        int IVsSolutionEvents.OnQueryCloseSolution(object pUnkReserved, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        int IVsSolutionEvents.OnQueryUnloadProject(IVsHierarchy pRealHierarchy, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        #endregion

        public event Action<IVsHierarchy> AfterProjectLoaded;
        public event Action AfterSolutionLoaded;
        public event Action BeforeSolutionClosed;
        public event Action<Project> OnProjectSelectionChanged;

        private void InitNullEvents()
        {
            AfterSolutionLoaded += () => { };
            BeforeSolutionClosed += () => { };
            OnProjectSelectionChanged += o => { };
            AfterProjectLoaded += o => { };
        }

        public Project GetProject(IVsHierarchy hierarchy)
        {
            if (hierarchy == null) return null;
            object project;

            var result = hierarchy.GetProperty(VSConstants.VSITEMID_ROOT,
                    (int) __VSHPROPID.VSHPROPID_ExtObject,
                    out project);
            if (result == VSConstants.S_OK)
            {
                return (project as Project);
            }
            return null;
        }
    }
}
