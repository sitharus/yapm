﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using Ionic.Zip;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell.Interop;
using NLog;
using Project = EnvDTE.Project;
using YAPM.Common;

namespace YAPM.YAPM_Extension.BuildSetup
{

    internal class SupportLibraries
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private const string YapmBuildSupportDirectory = "YAPM Build Support";

        /// <summary>
        /// Installs the YAPM build task binaries to the solution if they're needed.
        /// </summary>
        /// <param name="solution"></param>
        public void InstallBinaries(IVsSolution solution)
        {
            var packageBase = GetSupportDirectory(solution);

            var zipStream = GetType().Assembly.GetManifestResourceStream("YAPM.YAPM_Extension.BuildTasks.zip");
            var buildTasks = ZipFile.Read(zipStream);

            
            if (!Directory.Exists(packageBase))
            {
                Directory.CreateDirectory(packageBase);
            }
            bool needsOverwrite = false;
            buildTasks.ExtractProgress += (sender, args) =>
            {
                if (args.EventType == ZipProgressEventType.Extracting_ExtractEntryWouldOverwrite)
                {
                    try
                    {
                        var existingFile = File.OpenWrite(Path.Combine(args.ExtractLocation, args.CurrentEntry.FileName));
                        existingFile.Close();
                        args.CurrentEntry.ExtractExistingFile = ExtractExistingFileAction.OverwriteSilently;
                    }
                    catch (Exception)
                    {
                        needsOverwrite = true;
                        var newName = Path.ChangeExtension(args.CurrentEntry.FileName, ".overwrite");
                        args.CurrentEntry.ExtractExistingFile = ExtractExistingFileAction.DoNotOverwrite;
                        using (var output = File.OpenWrite(Path.Combine(args.ExtractLocation, newName)))
                        {
                            args.CurrentEntry.Extract(output);
                        }
                    }
                    
                }
            };
            buildTasks.ExtractAll(packageBase, ExtractExistingFileAction.InvokeExtractProgressEvent);
            if (needsOverwrite) // Already installed
            {
                MessageBox.Show("Could not update YAPM. Please restart Visual Studio to complete.");
                return;
            }

            var tasksPath = Path.Combine(packageBase, YapmTasks);
            using (var tasks = File.OpenWrite(tasksPath))
            {
                var taskStream = GetType().Assembly.GetManifestResourceStream("YAPM.YAPM_Extension.BuildSetup.YAPM.tasks");
                taskStream.CopyTo(tasks);
            }
        }

        public static string GetSupportDirectory(IVsSolution solution)
        {
            string solutionBase = GetSolutionPath(solution);
            var packageBase = Path.Combine(solutionBase, YapmBuildSupportDirectory);
            return packageBase;
        }

        /// <summary>
        /// Gets the full path to the current solution
        /// </summary>
        /// <param name="solution"></param>
        /// <returns></returns>
        private static string GetSolutionPath(IVsSolution solution)
        {
            string solutionBase;
            string solutionFile;
            string solutionUserOpts;
            var result = solution.GetSolutionInfo(out solutionBase, out solutionFile, out solutionUserOpts);
            if (result != VSConstants.S_OK)
            {
                throw new InvalidOperationException("Tried to use YAPM without a solution file");
            }
            return solutionBase;
        }

        /// <summary>
        /// Adds the YAPM build tasks to the current project, configuring the project to
        /// build the nupkg on release build.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="solution"></param>
        public void AddBuildTasks(Project project, IVsSolution solution)
        {
            var msbuildProject = FindMSBuildProject(project);
            if (msbuildProject == null)
            {
                return;
            }

            var projectPath = project.FullName;
            var solutionPath = GetSolutionPath(solution);
            var buildInfoPath = Path.Combine(solutionPath, YapmBuildSupportDirectory, YapmTasks);
            var relative = GetRelativePath(projectPath, buildInfoPath);
            var solutionRelativePath = GetRelativePath(projectPath, Path.GetDirectoryName(solutionPath));

            if (msbuildProject.Xml.Properties.All(p => p.Name != "SolutionDirectory"))
            {
                msbuildProject.Xml.AddProperty("SolutionDirectory", solutionRelativePath);
            }
            else
            {
                msbuildProject.Xml.Properties.First(p => p.Name == "SolutionDirectory").Value = solutionRelativePath;
            }

            // adds an <Import> element to this project file.
            if (msbuildProject.Xml.Imports.All(import => !relative.Equals(import.Project, StringComparison.OrdinalIgnoreCase)))
            {
                msbuildProject.Xml.AddImport(relative);
            }

            var releaseConfig = msbuildProject.Xml.PropertyGroups.FirstOrDefault(c => c.Condition.Contains("Release"));
            if (releaseConfig != null)
            {
                if (releaseConfig.Properties.All(p => p.Name != "BuildPackage"))
                {
                    releaseConfig.AddProperty("BuildPackage", "true");
                }
            }

            msbuildProject.ReevaluateIfNecessary();
            project.Save();
        }

        /// <summary>
        /// Removes the NuGet build task import line from the project
        /// </summary>
        /// <param name="project"></param>
        public void ClearNugetInclude(Project project)
        {
            var msbuildproject = FindMSBuildProject(project);
            if (msbuildproject == null)
            {
                return;
            }

            var nugetInclude = msbuildproject.Xml.Imports.Where(i => i.Project.Contains("nuget")).ToArray();
            if (nugetInclude.Length > 0)
            {
                foreach (var i in nugetInclude)
                {
                    msbuildproject.Xml.RemoveChild(i);
                }
            }
            var property = msbuildproject.GetProperty("RestorePackages");
            if (property != null)
            {
                msbuildproject.RemoveProperty(property);
            }
            msbuildproject.ReevaluateIfNecessary();


            project.Save();
        }

        /// <summary>
        /// Finds the MSBuild project file given a Visual Studio project file
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static Microsoft.Build.Evaluation.Project FindMSBuildProject(Project project)
        {
            return MSBuildHelpers.FindMSBuildProject(project.FullName);
        }

        /// <summary>
        /// Gets a relative path from fromPath to toPath
        /// </summary>
        /// <param name="fromPath"></param>
        /// <param name="toPath"></param>
        /// <returns></returns>
        public static string GetRelativePath(string fromPath, string toPath)
        {
            int fromAttr = GetPathAttribute(fromPath);
            int toAttr = GetPathAttribute(toPath);

            var path = new StringBuilder(260); // MAX_PATH
            if (PathRelativePathTo(
                path,
                fromPath,
                fromAttr,
                toPath,
                toAttr) == 0)
            {
                throw new ArgumentException("Paths must have a common prefix");
            }
            return path.ToString();
        }

        private static int GetPathAttribute(string path)
        {
            var di = new DirectoryInfo(path);
            if (di.Exists)
            {
                return FILE_ATTRIBUTE_DIRECTORY;
            }

            var fi = new FileInfo(path);
            if (fi.Exists)
            {
                return FILE_ATTRIBUTE_NORMAL;
            }

            throw new FileNotFoundException();
        }

        private const int FILE_ATTRIBUTE_DIRECTORY = 0x10;
        private const int FILE_ATTRIBUTE_NORMAL = 0x80;
        private const string YapmTasks = "YAPM.tasks";

        [DllImport("shlwapi.dll", SetLastError = true)]
        private static extern int PathRelativePathTo(StringBuilder pszPath,
            string pszFrom, int dwAttrFrom, string pszTo, int dwAttrTo);
    }
}
