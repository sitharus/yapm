﻿using EnvDTE;
using NLog.Targets;

namespace YAPM.YAPM_Extension.Support
{
    public class OutputPaneTarget : TargetWithLayout
    {
        private readonly OutputWindowPane _Pane;

        public OutputPaneTarget(OutputWindowPane pane)
        {
            _Pane = pane;
        }

        protected override void Write(NLog.LogEventInfo logEvent)
        {
            _Pane.OutputString(Layout.Render(logEvent) + "\n");
        }
    }
}
