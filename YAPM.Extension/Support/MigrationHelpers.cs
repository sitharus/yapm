﻿using System.IO;
using System.Xml.Linq;
using EnvDTE;
using Microsoft.VisualStudio.Shell.Interop;
using NLog;
using YAPM.Management;
using YAPM.Source;
using YAPM.YAPM_Extension.BuildSetup;

namespace YAPM.YAPM_Extension.Support
{
    public class MigrationHelpers
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Migrates a project using NuGet to YAPM.
        /// </summary>
        /// <param name="solution"></param>
        /// <param name="project"></param>
        /// <param name="ipm"></param>
        internal static void M‏igrateProject(IVsSolution solution, Project project, InstalledPackageManager ipm)
        {
            var migrator = new Migration.Nuget();
            var source = ServiceLocator.GetService<IPackageSource>();
            var targetFramework = Utilities.TargetFramework(project);
            var packagesConfigPath = PackageHelpers.GetNugetPackagesConfigPath(project);

            Logger.Info("Enabling YAPM for {0}", project.Name);
            EnableYAPM(solution, project);
            var support = new SupportLibraries();
            support.ClearNugetInclude(project);

            if (packagesConfigPath != null)
            {
                Logger.Info("Found nuget config, migrating");
                var config = XDocument.Load(packagesConfigPath);

                migrator.Migrate(source, ipm, config.Root, targetFramework, e =>
                    {
                        if (e.Kind == FileKind.Reference)
                            ReferenceManager.AddOrUpdateReference(e.EntryPath, project);
                    });

                Execute.OnUIThread(() =>
                    {
                        var projEnum = project.ProjectItems.GetEnumerator();
                        while (projEnum.MoveNext())
                        {
                            var pi = (ProjectItem) projEnum.Current;
                            if (pi.FileNames[1].EndsWith("packages.config"))
                            {
                                File.Delete(pi.FileNames[1]);
                                pi.Remove();
                                break;
                            }
                        }
                    });
            }
            project.Save();
        }

        /// <summary>
        /// Enables YAPM on a project not using a package manager
        /// </summary>
        /// <param name="solution"></param>
        /// <param name="currentProject"></param>
        internal static void EnableYAPM(IVsSolution solution, Project currentProject)
        {
            var support = new SupportLibraries();
            support.InstallBinaries(solution);
            support.AddBuildTasks(currentProject, solution);
        }
    }
}
