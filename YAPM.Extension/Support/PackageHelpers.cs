﻿using System.Threading.Tasks;
using EnvDTE;
using YAPM.Configuration;
using YAPM.Management;
using YAPM.Source;

namespace YAPM.YAPM_Extension.Support
{
    internal static class PackageHelpers
    {
        internal static Task InstallPackage( IPackageSource source, 
            InstalledPackageManager packageManager, string packageName, SemanticVersion version, Project project)
        {
            var taskFactory = new TaskFactory();
            return taskFactory.StartNew(() =>
                {
                    var targetFramework = Utilities.TargetFramework(project);
                    var package = source.GetPackage(packageName, version, null);
                    packageManager.InstallPackage(package, source, targetFramework, e => AddFile(e, project));
                });
        }

        public static void AddFile(InstalledPackageManager.ClassifiedEntry e, Project project)
        {
            switch (e.Kind)
            {
                case FileKind.Content:
                    ReferenceManager.AddFile(e.EntryPath, e.TargetPath, project);
                    break;
                case FileKind.Reference:
                    ReferenceManager.AddOrUpdateReference(e.EntryPath, project);
                    break;
            }
        }


        internal static string GetNugetPackagesConfigPath(Project obj)
        {
            if (obj.ProjectItems == null) // If the project isn't fully initialised.
            {
                return null;
            }
            var projEnum = obj.ProjectItems.GetEnumerator();
            while (projEnum.MoveNext())
            {
                var pi = (ProjectItem)projEnum.Current;
                if (pi.FileNames[1] != null && pi.FileNames[1].EndsWith("packages.config"))
                {
                    return pi.FileNames[1];
                }
            }
            return null;
        }

        public static Task InstallProject(PackageSpec package, InstalledPackageManager packageManager, Project project, IPackageSource source)
        {
            var taskFactory = new TaskFactory();
            return taskFactory.StartNew(() => packageManager.InstallProjectFromSpec(package, source));
        }
    }
}
