﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YAPM.YAPM_Extension.Support
{
    static class DllHelper
    {
        public static void SwapPendingOverwrites(String directory)
        {
            var toUpdate = Directory.GetFiles(directory, "*.overwrite");
            if (toUpdate.Any())
            {
                var msBuildProcs = System.Diagnostics.Process.GetProcessesByName("MSBuild");
                foreach (var msBuildProc in msBuildProcs)
                {
                    try
                    {
                        msBuildProc.Kill();
                        msBuildProc.WaitForExit();
                    }
                    catch (Exception)
                    {
                    }
                    
                }
            }
            foreach (var file in toUpdate)
            {
                var newName = Path.ChangeExtension(file, ".dll");
                if (File.Exists(newName))
                {
                    File.Delete(newName);
                }
                File.Move(file, newName);
            }
        }
    }
}
