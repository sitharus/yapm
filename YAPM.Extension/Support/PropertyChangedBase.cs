﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.VisualStudio.Shell;

namespace YAPM.YAPM_Extension.Support
{
    // Property changed support, using Caliburn.Micro as a base.

    public class PropertyChangedBase : INotifyPropertyChanged
    {
        public bool IsNotifying { get; set; }
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public PropertyChangedBase()
        {
            IsNotifying = true;
        }

        public virtual void NotifyOfPropertyChange<TProperty>(Expression<Func<TProperty>> property)
        {
            NotifyOfPropertyChange(property.GetMemberInfo().Name);
        }

        public virtual void NotifyOfPropertyChange(string propertyName)
        {
            if (IsNotifying)
            {
                Execute.OnUIThread(() => OnPropertyChanged(new PropertyChangedEventArgs(propertyName)));
            }
        }

        private void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }


    /// <summary>
    ///     Generic extension methods used by the framework.
    /// </summary>
    public static class ExtensionMethods
    {
        public static MemberInfo GetMemberInfo(this Expression expression)
        {
            var lambda = (LambdaExpression) expression;

            MemberExpression memberExpression;
            if (lambda.Body is UnaryExpression)
            {
                var unaryExpression = (UnaryExpression) lambda.Body;
                memberExpression = (MemberExpression) unaryExpression.Operand;
            }
            else
            {
                memberExpression = (MemberExpression) lambda.Body;
            }

            return memberExpression.Member;
        }
    }

    /// <summary>
    ///     Enables easy marshalling of code to the UI thread.
    /// </summary>
    public static class Execute
    {
        /// <summary>
        ///     Executes the action on the UI thread asynchronously.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        public static void BeginOnUIThread(this Action action)
        {
            ThreadHelper.Generic.BeginInvoke(action);
        }


        /// <summary>
        ///     Executes the action on the UI thread.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        public static void OnUIThread(this Action action)
        {
            ThreadHelper.Generic.Invoke(action);
        }
    }
}
