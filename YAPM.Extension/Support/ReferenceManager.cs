﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using EnvDTE;
using Microsoft.Win32;
using NLog;
using VSLangProj;
using YAPM.Common;
using YAPM.Configuration;
using YAPM.Management;
using YAPM.Source;
using YAPM.Utilities;
using YAPM.YAPM_Extension.BuildSetup;
using Constants = EnvDTE.Constants;

namespace YAPM.YAPM_Extension.Support
{
    internal class ReferenceManager
    {
        private static readonly char[] PathSeparatorChars = new[] {Path.DirectorySeparatorChar};
        private static readonly IEnumerable<string> Folders = new[] {Constants.vsProjectItemKindPhysicalFolder};
        private static bool _inUpdate;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static readonly ConcurrentDictionary<string, AssemblyReference> AssemblyNameCache = new ConcurrentDictionary<string, AssemblyReference>();

        class AssemblyReference
        {
            public DateTime Updated { get; set; }
            public AssemblyName AssemblyName { get; set; }
        }

        internal static void AddOrUpdateReference(string referencePath, Project project)
        {
            string dll = Path.GetFileNameWithoutExtension(referencePath);
            var vsProj =  project.Object as VSProject;
            
            if (vsProj != null)
            {
                try
                {
                    _inUpdate = true;
                    Reference existing = vsProj.References.OfType<Reference>().FirstOrDefault(r => r.Name == dll);

                    if (existing != null)
                    {
                        existing.Remove();
                    }

                    try
                    {
                        var relativePath = SupportLibraries.GetRelativePath(project.FullName, referencePath);
                        Reference reference = vsProj.References.Add(referencePath);
                        var hintPath = reference.Path;
                        if (hintPath != referencePath)
                        {
                            reference.Remove();
                            try
                            {
                                // Because F#. I don't know why this works.
                                vsProj.References.Add(relativePath);
                            }
                            catch (COMException)
                            {
                                // Sometimes VS really doesn't take the hint, so fall back to full path
                                // and patch up below.
                                vsProj.References.Add(referencePath);
                            }
                        }

                        // Get the msbuild project for this project
                        var buildProject = SupportLibraries.FindMSBuildProject(project);

                        if (buildProject != null)
                        {
                            // Get the assembly name of the reference we are trying to add
                            AssemblyName assemblyName = GetAssemblyName(referencePath);

                            var allAssemblies = buildProject.GetAssemblyReferences().ToArray();
                            // Try to find the item for the assembly name
                            var item = allAssemblies.Where(assemblyReferenceNode => assemblyName.Matches(assemblyReferenceNode.Item2))
                                                    .Select(assemblyReferenceNode => assemblyReferenceNode.Item1)
                                                    .FirstOrDefault();

                            if (item != null)
                            {
                                item.SetMetadataValue("SpecificVersion", "False");
                                item.SetMetadataValue("CopyLocal", "True");
                                item.SetMetadataValue("HintPath", relativePath);
                            }
                        }

                    }
                    catch (InvalidOperationException e)
                    {
                        Logger.ErrorException(String.Concat("Failed to add reference ", referencePath), e);
                    }
                }
                finally
                {
                    _inUpdate = false;
                }
                project.Save();
            }
        }

        private static AssemblyName GetAssemblyName(string referencePath)
        {
            DateTime modificationDate = new FileInfo(referencePath).LastWriteTimeUtc;
            AssemblyReference reference;
            if (!AssemblyNameCache.TryGetValue(referencePath, out reference) || modificationDate > reference.Updated)
            {
                reference = LoadAssemblyName(referencePath, modificationDate);
                AssemblyNameCache[referencePath] = reference;
            }
            return reference.AssemblyName;
        }

        private static AssemblyReference LoadAssemblyName(string referencePath, DateTime modificationDate)
        {
            return new AssemblyReference
                {
                    AssemblyName = AssemblyName.GetAssemblyName(referencePath),
                    Updated = modificationDate
                };
        }

        public static void RemoveReference(string referencePath, Project project)
        {
            string dll = Path.GetFileNameWithoutExtension(referencePath);
            var vsProj = (VSProject) project.Object;

            Reference existing = vsProj.References.Find(dll);

            if (existing != null)
            {
                try
                {
                    _inUpdate = true;
                    existing.Remove();
                }
                finally
                {
                    _inUpdate = false;
                }
            }
        }

        public static void AddFile(string fileLocation, string projectTarget, Project project)
        {
            string projectLocation = Path.GetDirectoryName(project.FullName);
            if (projectLocation == null) return;

            string target = Path.Combine(projectLocation, projectTarget);
            string targetDirectory = Path.GetDirectoryName(target);
            if (targetDirectory == null) return;

            if (!Directory.Exists(targetDirectory))
            {
                Directory.CreateDirectory(targetDirectory);
            }

            ProjectItems container = GetProjectItems(project, Path.GetDirectoryName(projectTarget), true);

            // So VS overwrites it
            if (File.Exists(target))
            {
                File.Delete(target);
            }
            container.AddFromFileCopy(fileLocation);
        }

        // All this is borrowed heavily from nuget.

        public static ProjectItems GetProjectItems(Project project, string folderPath, bool createIfNotExists = false)
        {
            if (String.IsNullOrEmpty(folderPath))
            {
                return project.ProjectItems;
            }

            // Traverse the path to get at the directory
            string[] pathParts = folderPath.Split(PathSeparatorChars, StringSplitOptions.RemoveEmptyEntries);

            var cursor = new ProjectItemContainer(project);

            string fullPath = GetFullPath(project);
            string folderRelativePath = String.Empty;

            foreach (string part in pathParts)
            {
                fullPath = Path.Combine(fullPath, part);
                folderRelativePath = Path.Combine(folderRelativePath, part);
                cursor = GetOrCreateFolder(cursor, fullPath, part, createIfNotExists);
                if (cursor == null)
                {
                    return null;
                }
            }

            return cursor.ProjectItems;
        }

        private static ProjectItemContainer GetOrCreateFolder(ProjectItemContainer parentItem, string fullPath, string folderName, bool createIfNotExists)
        {
            if (parentItem == null) return null;

            ProjectItems projectItems = parentItem.ProjectItems;
            ProjectItem subFolder = FindItemOfKind(projectItems, folderName, Folders);

            if (subFolder != null) return new ProjectItemContainer(subFolder);

            return createIfNotExists ? new ProjectItemContainer(projectItems.AddFromDirectory(fullPath)) : null;
        }

        public static string GetFullPath(Project project)
        {
            return Path.GetDirectoryName(project.FullName);
        }

        private static ProjectItem FindItemOfKind(ProjectItems items, string name, IEnumerable<string> kinds)
        {
            ProjectItem projectItem = null;
            try
            {
                projectItem = items.Item(name);
            }
            catch (ArgumentException) // If the item doesn't exist.
            {
            }

            if (projectItem != null && kinds.Contains(projectItem.Kind, StringComparer.OrdinalIgnoreCase))
            {
                return projectItem;
            }
            return null;
        }

        public static void CleanReference(DTE dte, Project project, Reference reference, InstalledPackageManager ipm)
        {
            if (_inUpdate) return;

            var allReferences = (from Project p in Utilities.AllSolutionProjects(dte.Solution) select p.Object)
                .OfType<VSProject>()
                .SelectMany(l => l.References
                                  .OfType<Reference>()
                                  .Select(r => !string.IsNullOrEmpty(r.Path) ? r.Path : r.Name))
                .ToList();

            var framework = Utilities.TargetFramework(project);
            ipm.RemovePackageIfUnreferenced(reference.Path, reference.Name, ServiceLocator.GetService<IPackageSource>(), allReferences, framework);
        }

        public static void RemoveReference(DTE dte, Project project, PackageSpec spec, InstalledPackageManager ipm)
        {
            if (_inUpdate) return;
            var source = ServiceLocator.GetService<IPackageSource>();
            var framework = Utilities.TargetFramework(project);
            var dllReferences = ipm.DLLPaths(spec, source, framework);

            var langProj = (VSProject) project.Object;
            var references = langProj.References.OfType<Reference>().Where(r => dllReferences.Contains(r.Path)).ToArray();

            var allReferences = Utilities.AllSolutionProjects(dte.Solution)
                .Where(p => p.UniqueName != project.UniqueName)
                .Select(p => p.Object)
                .OfType<VSProject>()
                .SelectMany(l => l.References
                                  .OfType<Reference>()
                                  .Where(r => !string.IsNullOrEmpty(r.Path) && dllReferences.Contains(r.Path)))
                .ToArray();


            if (allReferences.Length == 0)
            {
                var refPaths = allReferences.Select(r => r.Path).ToList();
                foreach (var reference in references)
                {
                    ipm.RemovePackageIfUnreferenced(reference.Path, reference.Name, source, refPaths, framework);
                }
            }

            foreach (var reference in references)
            {
                reference.Remove();
            }
        }

        public static void MaybeAddReference(VSProject langproj, Project project, Reference reference, InstalledPackageManager ipm)
        {
            if (_inUpdate || string.IsNullOrEmpty(reference.Path)) return;
            ipm.AddReferenceIfInLibrary(reference.Path, ServiceLocator.GetService<IPackageSource>());
        }

        public static List<ReferenceError> FixReferences(DTE dte, InstalledPackageManager ipm)
        {
            var suspiciousReferences = new List<ReferenceError>();
            var projectAssemblies = new List<string>();

            foreach (Project project in Utilities.AllSolutionProjects(dte.Solution)
                .Where(project => project.ConfigurationManager != null && project.ConfigurationManager.ActiveConfiguration != null))
            {
                try
                {
                    projectAssemblies.Add((string)project.Properties.Item("AssemblyName").Value);
                }
                catch (Exception) // This will only lose use some basic info if it fails.
                {}
            }

            foreach (Project project in Utilities.AllSolutionProjects(dte.Solution))
            {
                suspiciousReferences.AddRange(FixReferencesForProject(ipm, project, projectAssemblies));
            }
            return suspiciousReferences;
        }

        private static IEnumerable<ReferenceError> FixReferencesForProject(InstalledPackageManager ipm, Project project, IEnumerable<string> projectAssemblies)
        {
            var suspiciousReferences = new List<ReferenceError>();
            Logger.Trace(String.Concat("Fixing references for ", project.Name));
            var nativeProject = project.Object as VSProject;
            if (nativeProject != null && project.Properties != null)
            {
                var framework = Utilities.TargetFramework(project);
                var allDlls = ipm.GetAllDllReferences(ServiceLocator.GetService<IPackageSource>(), framework).ToArray();
                var badReferences = nativeProject.References
                                                 .OfType<Reference>()
                                                 .Select(r => new {Reference = r, Correct = allDlls.FirstOrDefault(d => d.Name == r.Name && d.RelativePath != r.Path)})
                                                 .Where(r => r.Correct != null)
                                                 .ToArray();

                foreach (var bad in badReferences)
                {
                    Logger.Info(String.Format("Fixing reference {0} to hintpath {1} in {2}", bad.Reference.Name, bad.Correct.RelativePath, project.Name));
                    AddOrUpdateReference(bad.Correct.RelativePath, project);
                }

                suspiciousReferences.AddRange(nativeProject.References
                                                           .OfType<Reference>()
                                                           .Where(r => r.Path != null && projectAssemblies.All(p => r.Name != p) && !IsSystemOrYapmReference(r.Path, ipm))
                                                           .Select(r => new ReferenceError(project, r.Name, r.Path)));
            }
            else
            {
                Logger.Trace(String.Concat(project.Name, " doesn't look like a project with references"));
            }
            return suspiciousReferences;
        }

        private static bool IsSystemOrYapmReference(string path, InstalledPackageManager ipm)
        {
            if (path.StartsWith(GetFrameworkDirectory()))
            {
                return true;
            }
            if (path.Contains("Reference Assemblies"))
            {
                return true;
            }
            if (ipm.IsInPackagePath(path))
            {
                return true;
            }

            return false;
        }

        private static string _FrameworkPath;
        public static string GetFrameworkDirectory()
        {
            if (_FrameworkPath != null) return _FrameworkPath;
            // This is the location of the .Net Framework Registry Key
            const string framworkRegPath = @"Software\Microsoft\.NetFramework";
            RegistryKey netFramework = Registry.LocalMachine.OpenSubKey(framworkRegPath, false);
            _FrameworkPath = netFramework.GetValue("InstallRoot").ToString();
            return _FrameworkPath;
        }
    }


    internal class ProjectItemContainer
    {
        public ProjectItemContainer(Project p)
        {
            ProjectItems = p.ProjectItems;
        }

        public ProjectItemContainer(ProjectItem p)
        {
            ProjectItems = p.ProjectItems;
        }

        public ProjectItems ProjectItems { get; private set; }
    }

    public class ReferenceError
    {
        public Project Project { get; private set; }
        public string ReferenceName { get; private set; }
        public string Path { get; private set; }

        public string ProjectName {
            get { return Project.Name; }
        }

        public string RelativeDllPath
        {
            get
            {
                try
                {
                    return SupportLibraries.GetRelativePath(Project.FullName, Path);
                }
                catch (ArgumentException)
                {
                    return Path;
                }
            }
        }

        public ReferenceError(Project project, string referenceName, string path)
        {
            Project = project;
            ReferenceName = referenceName;
            Path = path;
        }
    }
}
