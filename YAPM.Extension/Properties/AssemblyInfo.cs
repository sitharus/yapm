﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("YAPM.Extension")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("YAPM")]
[assembly: AssemblyProduct("YAPM.Extension")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]   
[assembly: ComVisible(false)]     
[assembly: CLSCompliant(false)]
[assembly: NeutralResourcesLanguage("en-US")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("1.0.0.*")]
[assembly: AssemblyFileVersion("1.0.0.*")]

[assembly: InternalsVisibleTo("YAPM.Extension_IntegrationTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100a3bef906086d3c67bdec2ece77dacaf12adb519427de829358de2184e432d67e790c5294b315330aee2c28e59750ff00e69e59f62b2f623ee38694674c14809729cdabf99b9671c4402e2f194ffb89f4091a87d9dc95ea3ad7a43ede26c5120b86777d387edd185e7014381f5c3e9de1f22f0983d0aa24a7f4cf51ef92b66cf2")]
[assembly: InternalsVisibleTo("YAPM.Extension_UnitTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100a3bef906086d3c67bdec2ece77dacaf12adb519427de829358de2184e432d67e790c5294b315330aee2c28e59750ff00e69e59f62b2f623ee38694674c14809729cdabf99b9671c4402e2f194ffb89f4091a87d9dc95ea3ad7a43ede26c5120b86777d387edd185e7014381f5c3e9de1f22f0983d0aa24a7f4cf51ef92b66cf2")]
