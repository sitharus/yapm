﻿using System.Windows.Input;
using YAPM.Source;
using YAPM.YAPM_Extension.Support;
using YAPM.YAPM_Extension.Views;

namespace YAPM.YAPM_Extension.ViewModels
{
    public class PackageSearchResultModel
    {
        public string Name { get; private set; }
        public VersionConstraint Version { get; private set; }
        public IPackage Package { get; private set; }
        public PackageControlModel Context { get; private set; }
        public ICommand InstallCommand { get; private set; }
        public ICommand SelectVersionCommand { get; private set; }

        public PackageSearchResultModel(IPackage package, PackageControlModel context)
        {
            InstallCommand = new DelegateCommand(InstallButton_Click);
            SelectVersionCommand = new DelegateCommand(SelectVersion_Click);
            Name = package.Id;
            Version = package.Version;
            Package = package;
            Context = context;
        }

        public void InstallButton_Click(object sender)
        {
            var source = ServiceLocator.GetService<IPackageSource>();
            PackageHelpers.InstallPackage(source, Context.PackageManager, Name, Version.BaseVersion, Context.CurrentProject);
        }

        public void SelectVersion_Click(object sender)
        {
            var source = ServiceLocator.GetService<IPackageSource>();
            var dialog = new SelectPackageVersion(new SelectPackageVersionViewModel(Context.PackageManager, source, Name, Context.CurrentProject, Context.AllProjects));
            dialog.ShowModal();
        }
    }
}
