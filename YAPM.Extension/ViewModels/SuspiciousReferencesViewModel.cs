﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using YAPM.YAPM_Extension.Support;

namespace YAPM.YAPM_Extension.ViewModels
{
    public class SuspiciousReferencesViewModel : PropertyChangedBase
    {
        private ObservableCollection<ReferenceError> _SuspiciousReferences;

        public ObservableCollection<ReferenceError> SuspiciousReferences
        {
            get { return _SuspiciousReferences; }
            set
            {
                _SuspiciousReferences = value;
                NotifyOfPropertyChange(() => SuspiciousReferences);
            }
        }

        public SuspiciousReferencesViewModel(IEnumerable<ReferenceError> references)
        {
            SuspiciousReferences = new ObservableCollection<ReferenceError>(references);
        }
    }
}
