﻿using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Threading.Tasks;
using EnvDTE;
using YAPM.Configuration;
using YAPM.Graph;
using YAPM.Management;
using YAPM.Source;
using YAPM.YAPM_Extension.Support;
using YAPM.YAPM_Extension.Views;

namespace YAPM.YAPM_Extension.ViewModels
{
    public class SelectPackageUpdateViewModel : SelectPackageVersionViewModel
    {
        public SelectPackageUpdateViewModel(InstalledPackageManager packageManager, IPackageSource source, PackageSpec package, Project project, List<Project> allProjects)
            : base(packageManager, source, package.PackageId, project, allProjects)
        {
        }

        public override string InstallButtonText
        {
            get { return "Upgrade"; }
        }

        protected override void OnInstall(object obj)
        {
            var taskFactory = new TaskFactory();
            taskFactory.StartNew(() =>
                {
                    var local = _Source.GetPackage(_SelectedPackage.Id, _SelectedPackage.Version.BaseVersion, null);
                    var spec = _SelectedPackage.Spec;
                    var framework = new FrameworkName(".NETFramework", new Version(4, 0, 0, 0), "");
                    UpdateResult result = _PackageManager.CheckUpdate(local, _Source);
                    Action completion = () => _PackageManager.FinaliseUpdate(framework, _Source, result);

                    if (result.RequiredUpdates.Count > 0)
                    {
                        Execute.OnUIThread(() =>
                            {
                                var vm = new ConfirmUpdatesViewModel(result.RequiredUpdates, spec, completion);
                                var view = new ConfirmUpdatesView {DataContext = vm};
                                vm.View = view;
                                view.ShowDialog();
                            });
                    }
                    else
                    {
                        completion();
                    }
                });
            Window.Close();
        }
    }
}
