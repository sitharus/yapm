﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Versioning;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using EnvDTE;
using Microsoft.VisualStudio.Shell.Interop;
using VSLangProj;
using YAPM.Configuration;
using YAPM.Graph;
using YAPM.Management;
using YAPM.Source;
using YAPM.YAPM_Extension.GraphView;
using YAPM.YAPM_Extension.Support;

namespace YAPM.YAPM_Extension.ViewModels
{
    /// <summary>
    /// This is the base viewmodel object for the UI stack. It actually handles a bit more
    /// state management than a viewmodel should, but I didn't want to pattern myself in to
    /// a corner like nuget did.
    /// </summary>
    public class PackageControlModel : PropertyChangedBase
    {
        private Project _CurrentProject;
        private NodeGraphLayout _Graph;
        private InstalledPackageManager _PackageManager;
        private TabItem _SelectedTab;

        public TabItem SelectedTab
        {
            get { return _SelectedTab; }
            set
            {
                _SelectedTab = value;
                Execute.OnUIThread(OnTabChange);
                NotifyOfPropertyChange(() => SelectedTab);
            }
        }

        public NodeGraphLayout Graph
        {
            get { return _Graph; }
            set
            {
                _Graph = value;
                NotifyOfPropertyChange(() => Graph);
            }
        }

        /// <summary>
        /// The list of installed package for the UI
        /// </summary>
        public ObservableCollection<InstalledPackageViewModel> InstalledPackages { get; set; }

        public Project CurrentProject
        {
            get { return _CurrentProject; }
            set
            {
                _CurrentProject = value;
                NotifyOfPropertyChange(() => CurrentProject);
                NotifyOfPropertyChange(() => HasProject);
            }
        }

        public InstalledPackageManager PackageManager
        {
            get { return _PackageManager; }
            set
            {
                if (_PackageManager != null) _PackageManager.OnPackagesUpdated -= UpdateInstalledPackages;
                _PackageManager = value;
                if (_PackageManager != null) _PackageManager.OnPackagesUpdated += UpdateInstalledPackages;
                UpdateInstalledPackages(null);
            }
        }

        public bool HasProject
        {
            get { return _CurrentProject != null; }
        }

        public List<Project> AllProjects
        {
            get { return Utilities.AllSolutionProjects(ServiceLocator.GetService<DTE>().Solution).Where(p => p.Object is VSProject).ToList(); }
        }

        /// <summary>
        /// Called on tab change to avoid keeping the pretty graphical graph
        /// in RAM when we don't need to.
        /// </summary>
        private void OnTabChange()
        {
            if (_SelectedTab != null && _SelectedTab.Name == "Graph")
            {
                new TaskFactory().StartNew(() =>
                    {
                        DependencyResolver packageGraph = PackageManager.GetGraph(ServiceLocator.GetService<IPackageSource>());


                        NodeGraph g = NodeGraph.FromDependencyGraph(packageGraph);
                        Execute.OnUIThread(() =>
                                           Graph = new NodeGraphLayout
                                               {
                                                   OverlapRemovalAlgorithmType = "FSA",
                                                   HighlightAlgorithmType = "Simple",
                                                   Margin = new Thickness(10),
                                                   LayoutAlgorithmType = "BoundedFR",
                                                   Graph = g
                                               });
                    });
            }
            else
            {
                Graph = null;
            }
        }

        /// <summary>
        /// Called by the InstalledPackageManager when it updates the packages list. This
        /// rebuilds the collection used by the UI.
        /// </summary>
        /// <param name="_"></param>
        private void UpdateInstalledPackages(object _)
        {
            if (PackageManager != null)
            {
                InstalledPackages = new ObservableCollection<InstalledPackageViewModel>(
                    PackageManager.BuildDependencies
                                  .Select(p => new InstalledPackageViewModel(p, this))
                                  .OrderBy(o => o.Name)
                    );
            }
            Execute.OnUIThread(OnTabChange);
            NotifyOfPropertyChange(() => InstalledPackages);
        }

        /// <summary>
        /// Called when the solution is loaded. Builds up a map of all projects to the package
        /// references they use
        /// </summary>
        /// <param name="dte"></param>
        public void ConfigureSolutionProjectMap(DTE dte)
        {
            foreach (Project project in Utilities.AllSolutionProjects(dte.Solution))
            {
                UpdateProjectMap(project);
            }
        }

        /// <summary>
        /// Called when a project is loaded. Builds the list of package references
        /// the project uses.
        /// </summary>
        /// <param name="project"></param>
        public void UpdateProjectMap(Project project)
        {
            string name = project.Name;

            var nativeProject = project.Object as VSProject;
            if (nativeProject != null && project.Properties != null)
            {
                FrameworkName framework = Utilities.TargetFramework(project);
                DllReference[] allDlls = _PackageManager.GetAllDllReferences(ServiceLocator.GetService<IPackageSource>(), framework).ToArray();
                References references = nativeProject.References;
                PackageSpec[] packages = references.OfType<Reference>()
                                                   .Select(reference => allDlls.FirstOrDefault(dll => dll.Name == reference.Name))
                                                   .Where(package => package != null)
                                                   .Select(p => p.SourcePackage)
                                                   .Distinct()
                                                   .ToArray();
                Execute.OnUIThread(() =>
                    {
                        foreach (PackageSpec package in packages)
                        {
                            var installed = InstalledPackages.FirstOrDefault(p => p.Name == package.PackageId);
                            if (installed == null)
                            {
                                installed = new InstalledPackageViewModel(package, this);
                                InstalledPackages.Add(installed);
                            }
                            installed.AddUser(name);
                        }
                    });
            }
        }

        /// <summary>
        ///  Called when a project's reference list changes, rebuilds the list
        ///  of package references used.
        /// </summary>
        /// <param name="langproj"></param>
        public void UpdateReferences(VSProject langproj)
        {
            string name = langproj.Project.Name;

            foreach (InstalledPackageViewModel package in InstalledPackages)
            {
                package.RemoveUser(name);
            }
            UpdateProjectMap(langproj.Project);
        }
    }
}
