﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using EnvDTE;
using Microsoft.VisualStudio.PlatformUI;
using VSLangProj;
using YAPM.Common;
using YAPM.Configuration;
using YAPM.Management;
using YAPM.Source;
using YAPM.YAPM_Extension.BuildSetup;
using YAPM.YAPM_Extension.Support;
using YAPM.YAPM_Extension.Views;

namespace YAPM.YAPM_Extension.ViewModels
{
    public class SelectPackageVersionViewModel : PropertyChangedBase
    {
        public readonly InstalledPackageManager _PackageManager;
        private readonly string _PackageName;
        private readonly BackgroundWorker _PackageVersionLoader = new BackgroundWorker();
        protected readonly Project _Project;
        protected readonly IPackageSource _Source;
        private List<IPackage> _AllPackages;
        private List<string> _Branches;
        private ICommand _Cancel;
        private ICommand _Install;
        private ICommand _SelectProject;
        private bool _IsBusy;
        private List<IPackage> _Packages;
        private List<string> _Projects;
        private string _SelectedBranch = "Release";
        protected IPackage _SelectedPackage;
        private string _SelectedProject;
        private readonly List<Project> _AllProjects;

        public SelectPackageVersionViewModel(InstalledPackageManager packageManager, IPackageSource source, string packageName, Project project, List<Project> allProjects)
        {
            _PackageManager = packageManager;
            _Source = source;
            _PackageName = packageName;
            _Project = project;
            IsBusy = true;
            _PackageVersionLoader.DoWork += LoadPackages;
            _PackageVersionLoader.RunWorkerCompleted += ShowPackages;
            _PackageVersionLoader.RunWorkerAsync();
            _Cancel = new DelegateCommand(OnCancel);
            _Install = new DelegateCommand(OnInstall);
            _SelectProject = new DelegateCommand(OnSelectPackage);
            _AllProjects = allProjects.OrderBy(p => p.Name).ToList();
            _Projects = allProjects.Select(p => p.Name).ToList();
        }

        public virtual string InstallButtonText
        {
            get { return "Install"; }
        }

        public ICommand Cancel
        {
            get { return _Cancel; }
            set
            {
                _Cancel = value;
                NotifyOfPropertyChange("Cancel");
            }
        }

        public ICommand Install
        {
            get { return _Install; }
            set
            {
                _Install = value;
                NotifyOfPropertyChange("Install");
            }
        }
        public ICommand SelectProject
        {
            get { return _SelectProject; }
            set
            {
                _SelectProject = value;
                NotifyOfPropertyChange("SelectProject");
            }
        }


        public DialogWindow Window { get; set; }

        public string SelectedBranch
        {
            get { return _SelectedBranch; }
            set
            {
                _SelectedBranch = value;
                NotifyOfPropertyChange("SelectedBranch");
                OnSelectionUpdated();
            }
        }

        public List<string> Branches
        {
            get { return _Branches; }
            set
            {
                _Branches = value;
                NotifyOfPropertyChange("Branches");
            }
        }

        public List<IPackage> Packages
        {
            get { return _Packages; }
            set
            {
                _Packages = value;
                NotifyOfPropertyChange("Packages");
            }
        }

        public bool IsBusy
        {
            get { return _IsBusy; }
            set
            {
                _IsBusy = value;
                NotifyOfPropertyChange("IsBusy");
            }
        }

        public IPackage SelectedPackage
        {
            get { return _SelectedPackage; }
            set
            {
                _SelectedPackage = value;
                NotifyOfPropertyChange("SelectedPackage");
                NotifyOfPropertyChange("IsPackageSelected");
            }
        }

        public List<string> Projects
        {
            get { return _Projects; }
            set
            {
                _Projects = value;
                NotifyOfPropertyChange(() => Projects);
            }
        }

        public string SelectedProject
        {
            get { return _SelectedProject; }
            set
            {
                _SelectedProject = value;
                NotifyOfPropertyChange(() => Projects);
                NotifyOfPropertyChange(() => IsProjectSelected);
            }
        }

        public bool IsProjectSelected
        {
            get { return _SelectedProject != null; }
        }


        public bool IsPackageSelected
        {
            get { return _SelectedPackage != null; }
        }

        private void OnCancel(object obj)
        {
            Window.Close();
        }

        protected virtual void OnInstall(object obj)
        {
            var progress = new ProgressView();
            PackageHelpers.InstallPackage(_Source, _PackageManager, _PackageName, _SelectedPackage.Version.BaseVersion, _Project)
                .ContinueWith(_ => progress.Finished());
            Window.Close();
            progress.ShowModal();
        }

        private void OnSelectPackage(object obj)
        {
            var progress = new ProgressView();
            var sourceProject = _AllProjects.First(p => p.Name == SelectedProject);
            var projectObject = sourceProject.Object as VSProject;
            if (projectObject != null)
            {
                var spec = new PackageSpec();
                var buildProject = SupportLibraries.FindMSBuildProject(sourceProject);
                var dllName = buildProject.Properties.FirstOrDefault(p => p.Name == "AssemblyName");
                if (dllName != null)
                {
                    spec.PackageId = dllName.EvaluatedValue;
                    spec.Version = new VersionConstraint("1.0.0.0", ConstraintType.Fixed);
                    spec.Satisfier = new ProjectPackageSatisification {ProjectName = sourceProject.Name};
                    PackageHelpers.InstallProject(spec, _PackageManager, _Project, _Source)
                        .ContinueWith(_ => progress.Finished());

                    Window.Close();
                    progress.ShowModal();
                    return;
                }
            }

            Window.Close();
            progress.ShowModal();
        }

        private void OnSelectionUpdated()
        {
            Packages = _AllPackages.Where(p => _SelectedBranch == "Release"
                                                   ? string.IsNullOrEmpty(p.Version.BaseVersion.Branch)
                                                   : p.Version.BaseVersion.Branch == _SelectedBranch)
                                   .OrderByDescending(p => p.Version.BaseVersion).ToList();
        }

        private void ShowPackages(object sender, RunWorkerCompletedEventArgs e)
        {
            IsBusy = false;
            _AllPackages = (List<IPackage>) e.Result;
            Branches = new[] {"Release"}.Concat(
                _AllPackages.Select(p => p.Version.BaseVersion.Branch).Distinct().OrderBy(o => o)
                ).ToList();
            Packages = _AllPackages.Where(p => string.IsNullOrEmpty(p.Version.BaseVersion.Branch)).OrderByDescending(p => p.Version.BaseVersion).ToList();
        }

        private void LoadPackages(object sender, DoWorkEventArgs e)
        {
            e.Result = _Source.VersionsForPackage(_PackageName).ToList();
        }
    }
}
