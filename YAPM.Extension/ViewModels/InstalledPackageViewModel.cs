﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using EnvDTE;
using YAPM.Configuration;
using YAPM.Source;
using YAPM.YAPM_Extension.Support;
using YAPM.YAPM_Extension.Views;

namespace YAPM.YAPM_Extension.ViewModels
{
    public class InstalledPackageViewModel : PropertyChangedBase
    {
        protected readonly PackageControlModel PackageControlModel;
        protected readonly PackageSpec Package;
        private readonly HashSet<string> _UsedBy = new HashSet<string>();

        public InstalledPackageViewModel(PackageSpec package, PackageControlModel packageControlModel)
        {
            Package = package;
            PackageControlModel = packageControlModel;
            AddCommand = new DelegateCommand(Add);
            UpdateCommand = new DelegateCommand(Update);
            RemoveCommand = new DelegateCommand(Remove);
        }

        public ICommand AddCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand RemoveCommand { get; private set; }

        public string Name
        {
            get { return Package.PackageId; }
        }

        public string Version
        {
            get
            {
                return Package.Satisfier is ProjectPackageSatisification
                    ? "Project"
                    : Package.Version.BaseVersion.ToString();
            }
        }

        public string UsedBy
        {
            get { return string.Join(", ", _UsedBy.OrderBy(m => m)); }
        }

        public void RemoveUser(string user)
        {
            _UsedBy.Remove(user);
            NotifyOfPropertyChange(() => UsedBy);
        }

        public void AddUsers(IEnumerable<string> users)
        {
            foreach (var item in users)
            {
                _UsedBy.Add(item);
            }
            NotifyOfPropertyChange(() => UsedBy);
        }

        public void AddUser(string name)
        {
            _UsedBy.Add(name);
            NotifyOfPropertyChange(() => UsedBy);
        }

        public virtual void Update(object sender)
        {
            var source = ServiceLocator.GetService<IPackageSource>();
            var window = new SelectPackageVersion(new SelectPackageUpdateViewModel(PackageControlModel.PackageManager, source, Package, PackageControlModel.CurrentProject, PackageControlModel.AllProjects));
            window.ShowModal();
        }

        public virtual void Add(object sender)
        {
            var source = ServiceLocator.GetService<IPackageSource>();
            PackageHelpers.InstallPackage(source, PackageControlModel.PackageManager, Package.PackageId, Package.Version.BaseVersion, PackageControlModel.CurrentProject);
        }

        public virtual void Remove(object sender)
        {
            var taskFactory = new TaskFactory();
            taskFactory.StartNew(() =>
                {
                    var dte = ServiceLocator.GetService<DTE>();
                    ReferenceManager.RemoveReference(dte, PackageControlModel.CurrentProject, Package, PackageControlModel.PackageManager);
                });
        }
    }
}
