﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using YAPM.Configuration;
using YAPM.Graph;
using YAPM.YAPM_Extension.Support;
using YAPM.YAPM_Extension.Views;

namespace YAPM.YAPM_Extension.ViewModels
{
    public class ConfirmUpdatesViewModel : PropertyChangedBase
    {
        private const string UpgradeMessage = "To upgrade {0} these packages must also be upgraded";
        private readonly PackageSpec _RequestedPackage;
        private readonly Action _Completion;
        private readonly ObservableCollection<PackageUpdate> _Updates;
        private readonly DelegateCommand _Upgrade;
        private readonly DelegateCommand _Cancel;

        public ObservableCollection<PackageUpdate> Updates
        {
            get { return _Updates; }
        }

        public string UpgradeMesssage
        {
            get { return String.Format(UpgradeMessage, _RequestedPackage.PackageId); }
        }

        public DelegateCommand UpgradeCommand
        {
            get { return _Upgrade; }
        }

        public DelegateCommand CancelCommand
        {
            get { return _Cancel; }
        }

        public ConfirmUpdatesView View { get; set; }

        public ConfirmUpdatesViewModel(IEnumerable<PackageUpdate> updates, PackageSpec requestedPackage, Action completion)
        {
            _RequestedPackage = requestedPackage;
            _Completion = completion;
            _Updates = new ObservableCollection<PackageUpdate>(updates);
            _Upgrade = new DelegateCommand(Upgrade);
            _Cancel = new DelegateCommand(Cancel);
        }

        public void Upgrade(object sender)
        {
            _Completion();
            Cancel(sender);
        }

        public void Cancel(object sender)
        {
            View.Close();
        }
    }
}
