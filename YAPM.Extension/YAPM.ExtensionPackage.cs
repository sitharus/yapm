﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using NLog;
using NLog.Config;
using VSLangProj;
using YAPM.Management;
using YAPM.YAPM_Extension.BuildSetup;
using YAPM.YAPM_Extension.Interaction;
using YAPM.YAPM_Extension.Support;
using YAPM.YAPM_Extension.ViewModels;
using YAPM.YAPM_Extension.Views;
using YAPM.Source;

namespace YAPM.YAPM_Extension
{
    /// <summary>
    ///     This is the class that implements the package exposed by this assembly.
    ///     The minimum requirement for a class to be considered a valid package for Visual Studio
    ///     is to implement the IVsPackage interface and register itself with the shell.
    ///     This package uses the helper classes defined inside the Managed Package Framework (MPF)
    ///     to do it: it derives from the Package class that provides the implementation of the
    ///     IVsPackage interface and uses the registration attributes defined in the framework to
    ///     register itself and its components with the shell.
    /// </summary>
    // This attribute tells the PkgDef creation utility (CreatePkgDef.exe) that this class is
    // a package.
    [PackageRegistration(UseManagedResourcesOnly = true)]
    // This attribute is used to register the informations needed to show the this package
    // in the Help/About dialog of Visual Studio.
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
    // This attribute is needed to let the shell know that this package exposes some menus.
    [ProvideMenuResource("Menus.ctmenu", 1)]
    // This attribute registers a tool window exposed by this package.
    [ProvideToolWindow(typeof (PackageManagerWindow))]
    [Guid(GuidList.guidYAPM_ExtensionPkgString)]
    [ProvideAutoLoad(VSConstants.UICONTEXT.SolutionExists_string)]
    [ProvideOptionPage(typeof (OptionsPane), OptionsMainGroup, OptionsGeneral, 0, 0, true)]
    public sealed class YAPM_ExtensionPackage : Microsoft.VisualStudio.Shell.Package
    {
        public const string OptionsMainGroup = "YAPM";
        public const string OptionsGeneral = "General";
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private PackageControlModel _ControlModel;
        private Events _Events;
        private MenuCommand _MenuEnableYapm;
        private MenuCommand _MenuFixSolutionReferences;
        private MenuCommand _MenuMigrateSolutionToYapm;
        private MenuCommand _MenuMigrateToYapm;
        private InstalledPackageManager _PackageManager;
        private ReferencesEvents _ReferencesEvents;
        private SolutionEventsListener _SolutionEventsListener;

        public OptionsPane Settings
        {
            get { return GetDialogPage(typeof (OptionsPane)) as OptionsPane; }
        }

        private MenuCommand MenuEnableYapm
        {
            get
            {
                if (_MenuEnableYapm == null)
                {
                    var enableYapm = new CommandID(GuidList.guidYAPM_ExtensionCmdSet, (int) PkgCmdIDList.cmdidYAPMConfigureProject);
                    _MenuEnableYapm = new MenuCommand(EnableYAPM, enableYapm);
                }
                return _MenuEnableYapm;
            }
        }

        private MenuCommand MenuMigrateToYapm
        {
            get
            {
                if (_MenuMigrateToYapm == null)
                {
                    var command = new CommandID(GuidList.guidYAPM_ExtensionCmdSet, (int) PkgCmdIDList.cmdidYAPMMigrateProject);
                    _MenuMigrateToYapm = new MenuCommand(MigrateToYAPM, command);
                }
                return _MenuMigrateToYapm;
            }
        }

        private MenuCommand MenuMigrateSolutionToYapm
        {
            get
            {
                if (_MenuMigrateSolutionToYapm == null)
                {
                    var command = new CommandID(GuidList.guidYAPM_ExtensionCmdSet, (int) PkgCmdIDList.cmdidYAPMMigrateSolution);
                    _MenuMigrateSolutionToYapm = new MenuCommand(MigrateSolutionToYAPM, command);
                }
                return _MenuMigrateSolutionToYapm;
            }
        }

        private MenuCommand MenuFixSolutionReferences
        {
            get
            {
                if (_MenuFixSolutionReferences == null)
                {
                    var command = new CommandID(GuidList.guidYAPM_ExtensionCmdSet, (int) PkgCmdIDList.cmdidYAPMFixSolution);
                    _MenuFixSolutionReferences = new MenuCommand(FixSolutionReferences, command);
                }
                return _MenuFixSolutionReferences;
            }
        }

        /// <summary>
        ///     This function is called when the user clicks the menu item that shows the
        ///     tool window. See the Initialize method to see how the menu item is associated to
        ///     this function using the OleMenuCommandService service and the MenuCommand class.
        /// </summary>
        private void ShowToolWindow(object sender, EventArgs e)
        {
            // Get the instance number 0 of this tool window. This window is single instance so this instance
            // is actually the only one.
            // The last flag is set to true so that if the tool window does not exists it will be created.
            var packageManagerWindow = (PackageManagerWindow) FindToolWindow(typeof (PackageManagerWindow), 0, true);
            if ((null == packageManagerWindow) || (null == packageManagerWindow.Frame))
            {
                throw new NotSupportedException(Resources.CanNotCreateWindow);
            }
            var windowFrame = (IVsWindowFrame) packageManagerWindow.Frame;
            ErrorHandler.ThrowOnFailure(windowFrame.Show());
        }

        /// <summary>
        ///     Enables YAPM management for a project
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnableYAPM(object sender, EventArgs e)
        {
            var solution = GetGlobalService(typeof (SVsSolution)) as IVsSolution;
            MigrationHelpers.EnableYAPM(solution, _ControlModel.CurrentProject);
        }

        /// <summary>
        ///   Migrates an entire solution to YAPM. Simply loops through all projects and migrates them.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MigrateSolutionToYAPM(object sender, EventArgs e)
        {
            var progress = new ProgressView();
            var solution = GetGlobalService(typeof (SVsSolution)) as IVsSolution;
            var dte = (DTE) GetService(typeof (DTE));

            new TaskFactory().StartNew(() =>
                {
                    string solutionDirectory;
                    string solutionFile;
                    string solutionUserOpts;
                    solution.GetSolutionInfo(out solutionDirectory, out solutionFile, out solutionUserOpts);

                    string nugetPath = Path.Combine(solutionDirectory, ".nuget");
                    if (Directory.Exists(nugetPath))
                    {
                        try
                        {
                            Directory.Delete(nugetPath, true);
                        }
                        catch (Exception) // Meh if this fails
                        {
                        }
                    }

                    var allProjects = Utilities.AllSolutionProjects(dte.Solution);
                    foreach (Project project in allProjects.Where(p => p.Object is VSProject))
                    {
                        MigrationHelpers.M‏igrateProject(solution, project, _PackageManager);
                    }

                    Execute.OnUIThread(() =>
                        {
                            ShowMigrateSolution(dte);
                            progress.Finished();
                        });
                });

            progress.ShowModal();
        }

        /// <summary>
        ///     Fixes all assembly references in the solution to point to YAPM packages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FixSolutionReferences(object sender, EventArgs e)
        {
            var dte = (DTE) GetService(typeof (DTE));
            var progress = new ProgressView();

            new TaskFactory<List<ReferenceError>>()
                .StartNew(() => ReferenceManager.FixReferences(dte, _PackageManager))
                .ContinueWith((task, state) => Execute.OnUIThread(() =>
                    {
                        progress.Finished();
                        if (task.Result.Count > 0)
                        {
                            var window = new SuspiciousReferencesView {DataContext = new SuspiciousReferencesViewModel(task.Result)};
                            window.ShowModal();
                        }
                    }), TaskScheduler.FromCurrentSynchronizationContext());
            progress.ShowModal();
        }

        /// <summary>
        ///     Migrates an individual project to YAPM
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MigrateToYAPM(object sender, EventArgs e)
        {
            var solution = GetGlobalService(typeof (SVsSolution)) as IVsSolution;
            Project project = _ControlModel.CurrentProject;

            var progress = new ProgressView();
            new TaskFactory().StartNew(() =>
                {
                    try
                    {
                        MigrationHelpers.M‏igrateProject(solution, project, _PackageManager);
                    }
                    finally
                    {
                        Execute.OnUIThread(progress.Finished);
                    }
                });
            progress.ShowModal();
        }

        /// <summary>
        ///     Initialization of the package; this method is called right after the package is sited, so this is the place
        ///     where you can put all the initilaization code that rely on services provided by VisualStudio.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            var logConfig = new LoggingConfiguration();
            var logTarget = new OutputPaneTarget(GetOutputPane()) {Layout = "${date:format=HH:MM:ss} ${logger} ${message} ${exception:format=ToString,StackTrace}"};


            logConfig.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, logTarget));
            LogManager.Configuration = logConfig;
            Logger.Trace("Logging initialised, starting YAPM");

            _SolutionEventsListener = new SolutionEventsListener();
            _SolutionEventsListener.AfterSolutionLoaded += SolutionLoaded;
            _SolutionEventsListener.AfterProjectLoaded += UpdateProjectMap;
            _SolutionEventsListener.OnProjectSelectionChanged += UpdateCurrentProjectView;

            // Add our command handlers for menu (commands must exist in the .vsct file)
            var mcs = GetService(typeof (IMenuCommandService)) as OleMenuCommandService;
            if (null != mcs)
            {
                // Create the command for the menu item.
                var menuCommandId = new CommandID(GuidList.guidYAPM_ExtensionCmdSet, (int) PkgCmdIDList.cmdidYAPMPackages);
                var menuItem = new MenuCommand(ShowToolWindow, menuCommandId);
                mcs.AddCommand(menuItem);
                // Create the command for the tool window
                var toolwndCommandId = new CommandID(GuidList.guidYAPM_ExtensionCmdSet, (int) PkgCmdIDList.cmdidYAPMTool);
                var menuToolWin = new MenuCommand(ShowToolWindow, toolwndCommandId);
                mcs.AddCommand(menuToolWin);
                mcs.AddCommand(MenuEnableYapm);
                mcs.AddCommand(MenuMigrateToYapm);
                mcs.AddCommand(MenuMigrateSolutionToYapm);
                mcs.AddCommand(MenuFixSolutionReferences);
                MenuMigrateToYapm.Visible = true;
                MenuEnableYapm.Visible = false;
            }

            ServiceLocator.InitaliseServiceLocator((DTE) GetService(typeof (DTE)));
            _ControlModel = ServiceLocator.GetService<PackageControlModel>();
        }

        /// <summary>
        ///     Updates menu visibility based on the currently selected project
        /// </summary>
        /// <param name="obj"></param>
        private void UpdateCurrentProjectView(Project obj)
        {
            MenuEnableYapm.Visible = false;
            var window = (PackageManagerWindow) FindToolWindow(typeof (PackageManagerWindow), 0, true);
            if (obj == null)
            {
                if (window != null)
                {
                    MenuEnableYapm.Visible = true;
                    window.FollowProjectChange(null);
                }
                return;
            }

            if (window != null)
            {
                MenuEnableYapm.Visible = true;
                window.FollowProjectChange(obj);
            }
        }


        /// <summary>
        ///  Updates the solution wide settings when a solution is loaded
        /// </summary>
        private void SolutionLoaded()
        {
            var dte = (DTE) GetService(typeof (DTE));
            var solution = (IVsSolution) GetGlobalService(typeof (SVsSolution));
            string solutionDirectory;
            string solutionFile;
            string userFile;
            solution.GetSolutionInfo(out solutionDirectory, out solutionFile, out userFile);
            _PackageManager = new InstalledPackageManager(solutionDirectory);
            _ControlModel.PackageManager = _PackageManager;
            _Events = dte.Events;
            _ReferencesEvents = (ReferencesEvents) _Events.GetObject("CSharpReferencesEvents");
            _ReferencesEvents.ReferenceRemoved += RemoveReference;
            _ReferencesEvents.ReferenceAdded += MaybeAddReference;

            DllHelper.SwapPendingOverwrites(SupportLibraries.GetSupportDirectory(solution));

            _ControlModel.ConfigureSolutionProjectMap(dte);


            var window = (PackageManagerWindow) FindToolWindow(typeof (PackageManagerWindow), 0, true);
            if (window != null)
            {
                window.SolutionLoaded();
            }

            ShowMigrateSolution(dte);

            new TaskFactory().StartNew(() => _PackageManager.RestoreAllPackages(ServiceLocator.GetService<IPackageSource>())); 
        }

        /// <summary>
        /// Called from the on reference added event. Will add the package to YAPM
        /// if it is not already known
        /// </summary>
        /// <param name="reference"></param>
        private void MaybeAddReference(Reference reference)
        {
            Project project = reference.ContainingProject;
            var langproj = project.Object as VSProject;
            if (langproj == null || !project.IsDirty) return;
            ReferenceManager.MaybeAddReference(langproj, project, reference, _PackageManager);
            _ControlModel.UpdateReferences(langproj);
        }

        /// <summary>
        /// Called from the reference removed event. Will remove the package if no
        /// other projects reference it.
        /// </summary>
        /// <param name="reference"></param>
        private void RemoveReference(Reference reference)
        {
            Project project = reference.ContainingProject;
            var langproj = project.Object as VSProject;
            if (langproj != null)
            {
                var dte = (DTE) GetService(typeof (DTE));
                ReferenceManager.CleanReference(dte, project, reference, _PackageManager);
                _ControlModel.UpdateReferences(langproj);
            }
        }

        private void ShowMigrateSolution(DTE dte)
        {
            foreach (Project p in Utilities.AllSolutionProjects(dte.Solution))
            {
                if (PackageHelpers.GetNugetPackagesConfigPath(p) != null)
                {
                    MenuMigrateSolutionToYapm.Visible = true;
                }
            }
        }

        /// <summary>
        ///     Updates the project IPM mapping when a project is loaded. We do this in the background.
        /// </summary>
        /// <param name="hierarchy"></param>
        private void UpdateProjectMap(IVsHierarchy hierarchy)
        {
            var taskFactory = new TaskFactory();
            taskFactory.StartNew(() =>
                {
                    var solution = GetGlobalService(typeof (SVsSolution)) as IVsSolution;
                    if (solution == null) return;

                    string solutionDirectory;
                    string solutionFile;
                    string solutionUserOpts;

                    solution.GetSolutionInfo(out solutionDirectory, out solutionFile, out solutionUserOpts);
                    try
                    {
                        object projectObj;
                        ErrorHandler.ThrowOnFailure(hierarchy.GetProperty(VSConstants.VSITEMID_ROOT, (int) __VSHPROPID.VSHPROPID_ExtObject, out projectObj));

                        if (solutionDirectory == null) return;

                        var project = projectObj as Project;
                        if (project != null && !string.IsNullOrEmpty(project.FullName)) // If FullName is "" it's not a real project
                        {
                            if (PackageHelpers.GetNugetPackagesConfigPath(project) != null)
                            {
                                MenuMigrateSolutionToYapm.Visible = true;
                            }
                            _ControlModel.UpdateProjectMap(project);
                        }
                    }
                    catch (Exception e) // We can safely ignore this
                    {
                        Logger.TraceException("Exception caught updating project map", e);
                    }
                });
        }

        /// <summary>
        /// Finds the output text pane to log to.
        /// </summary>
        /// <returns></returns>
        private OutputWindowPane GetOutputPane()
        {
            const string title = "YAPM";
            var dte = (DTE2) GetService(typeof (DTE));
            OutputWindowPanes panes = dte.ToolWindows.OutputWindow.OutputWindowPanes;

            try
            {
                return panes.Item(title);
            }
            catch (ArgumentException)
            {
                return panes.Add(title);
            }
        }
    }
}
