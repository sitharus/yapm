﻿// PkgCmdID.cs
// MUST match PkgCmdID.h

namespace YAPM.YAPM_Extension
{
    static class PkgCmdIDList
    {
        public const uint cmdidYAPMPackages =        0x100;
        public const uint cmdidYAPMTool = 0x101;
        public const uint cmdidYAPMConfigureProject = 0x102;

        public const uint cmdidYAPMMigrateProject = 0x103;
        public const uint cmdidYAPMMigrateSolution = 0x104;
        public const uint cmdidYAPMFixSolution = 0x105;

    };
}