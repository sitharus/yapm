﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Build.Framework;
using NLog;
using YAPM.Configuration;
using YAPM.Management;
using YAPM.Utilities;

namespace YAPM.BuildTasks
{
    /// <summary>
    /// Do a pre-build check that packages referenced are present in the
    /// project's packages folder. Mostly for clean copies and build server
    /// builds, so this should be optimised for the case where the packages
    /// are already present.
    /// </summary>
    public class RestorePackages : TaskBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        [Required]
        public string SolutionDirectory { get; set; }

        [Required]
        public string ProjectFile { get; set; }


        public override bool Execute()
        {
            using (new BuildLogScope(Log, Path.GetFileNameWithoutExtension(ProjectFile)))
            {
                try
                {
                    Logger.Info("Starting package restore for {0}", ProjectFile);
                    var projectDirectory = Path.GetFullPath(ProjectFile);
                    var solutionDirectory = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(projectDirectory), SolutionDirectory));
                    Logger.Trace("Package restore for {0}, solution {1}", projectDirectory, solutionDirectory);

                    var packageDirectory = InstalledPackageManager.LibraryPath(solutionDirectory);
                    if (!Directory.Exists(packageDirectory))
                    {
                        Directory.CreateDirectory(packageDirectory);
                    }
                    var configFile = InstalledPackageManager.ConfigPath(solutionDirectory);
                    var restoreFile = Path.Combine(packageDirectory, "lastrestore.time");
                    using (FileWithLock f = FileWithLock.Open(restoreFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, 50, 100, true))
                    {
                        var lastDate = new FileInfo(configFile).LastWriteTimeUtc;
                        if (f.Value.Length > 0)
                        {
                            using (var reader = new StreamReader(f.Value))
                            {
                                DateTime lastRestore = DateTime.MinValue;
                                var line = reader.ReadLine();
                                if (line != null)
                                {
                                    DateTime.TryParse(line.Trim(), out lastRestore);
                                }
                                if (lastRestore < lastDate)
                                {
                                    Logger.Trace("Packages are out of date, restoring.");
                                    PerformRestore(solutionDirectory);
                                    SaveRestoreDate(f);
                                }
                            }
                        }
                        else
                        {
                            Logger.Trace("Restore info file has not been created, performing restore.");
                            PerformRestore(solutionDirectory);
                            SaveRestoreDate(f);
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.FatalException("Fatal error restoring packages!", e);
                    return false;
                }
            }

            return true;
        }

        private static void SaveRestoreDate(FileWithLock f)
        {
            f.Value.Seek(0, SeekOrigin.Begin);
            f.Value.SetLength(0);
            using (var writer = new StreamWriter(f.Value))
            {
                writer.WriteLine(DateTime.Now.ToString("u"));
            }
        }

        private void PerformRestore(string solutionDirectory)
        {
            var manager = new InstalledPackageManager(solutionDirectory);

            foreach (var spec in manager.Dependencies.Where(spec => spec.Satisfier is AssemblyPackageSatisifier || spec.Satisfier == null))
            {
                Logger.Debug("Restoring package " + spec.PackageId);
                var source = GetSource(spec);
                var package = source.GetPackage(spec.PackageId, spec.Version.BaseVersion, null);
                manager.RestorePackage(package);
            }
            Logger.Info("Package restore complete");
        }
    }
}