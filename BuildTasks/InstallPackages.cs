﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Build.Framework;
using NLog;
using YAPM.Common;
using YAPM.Configuration;
using YAPM.Core.Utilities;
using YAPM.Management;
using YAPM.Source;

namespace YAPM.BuildTasks
{
    /// <summary>
    /// Performs a post-build copy of indirect dependencies to the build output
    /// directory. This shouldn't copy anything directly referenced.
    /// </summary>
    public class InstallPackages : TaskBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        [Required]
        public string SolutionDirectory { get; set; }

        [Required]
        public string OutputDirectory { get; set; }

        [Required]
        public string TargetFrameworkVersion { get; set; }

        public string TargetFrameworkProfile { get; set; }

        [Required]
        public string ProjectFile { get; set; }

        public override bool Execute()
        {
            using (new BuildLogScope(Log, Path.GetFileNameWithoutExtension(ProjectFile)))
            {
                Logger.Info("Starting package install");
                var projectFile = Path.GetFullPath(ProjectFile);
                var projectDirectory = Path.GetDirectoryName(projectFile);
                if (projectDirectory == null)
                {
                    Logger.Error("Failed to determine project location");
                    return false;
                }
                var solutionDirectory = Path.GetFullPath(Path.Combine(projectDirectory, SolutionDirectory));

                var project = MSBuildHelpers.FindMSBuildProject(projectFile);

                var assemblyReferences = project.GetAssemblyReferences().Select(i => i.Item2).ToArray();

                var manager = new InstalledPackageManager(solutionDirectory);

                var output = Path.GetFullPath(Path.Combine(projectDirectory, OutputDirectory));
                var framework = FrameworkVersionCompatibility.ParseVersionFromProject(TargetFrameworkVersion, TargetFrameworkProfile);

                // Find all DLLs that are referenced
                var deps = new List<PackageSpec>();
                foreach (var spec in manager.BuildDependencies)
                {
                    var source = GetSource(spec);
                    if (!IsReferenced(assemblyReferences, source, spec, manager, framework))
                    {
                        continue;
                    }

                    // Then get the packages and extract the dependencies
                    var package = source.GetPackage(spec.PackageId, spec.Version.BaseVersion, null);
                    deps.AddRange(RecursiveGetDependencies(source, package.Dependencies.Select(dp => FixLocation(dp, spec)).ToList()));
                }

                // Then find the resolved version in the dependency map and copy in the DLLs
                foreach (var spec in deps.Distinct().Select(dep => manager.Dependencies.First(p => p.PackageId == dep.PackageId)))
                {
                    Logger.Trace("Installing package {0} version {1}", spec.PackageId, spec.Version.BaseVersion);
                    var source = GetSource(spec);
                    var package = source.GetPackage(spec.PackageId, spec.Version.BaseVersion, null);
                    var dlls = manager.DLLPaths(package, framework);
                    foreach (var dll in dlls)
                    {
                        Logger.Trace("Installing DLL {0}", dll);
                        try
                        {
                            var name = Path.GetFileName(dll);
                            if (name == null) continue;

                            var dest = Path.Combine(output, name);
                            if (File.Exists(dest))
                            {
                                var sourceInfo = new FileInfo(name);
                                var destInfo = new FileInfo(dest);
                                if (destInfo.CreationTime > sourceInfo.CreationTime)
                                {
                                    continue;
                                }
                            }
                            File.Copy(dll, Path.Combine(output, name));
                        }
                        catch (Exception e)
                        {
                            Logger.ErrorException("Failed to install DLL", e);
                        }
                    }
                }

                return true;
            }
        }

        private IEnumerable<PackageSpec> RecursiveGetDependencies(CachingPackageSource source, List<PackageSpec> dependencies)
        {
            return dependencies.Concat(dependencies
                .SelectMany(d => RecursiveGetDependencies(source,
                    source.GetPackage(d.PackageId, d.Version.BaseVersion, source)
                    .Dependencies
                    .Select(dp => FixLocation(dp, d)).ToList())));
        }

        private static PackageSpec FixLocation(PackageSpec target, PackageSpec original)
        {
            target.OriginalLocation = original.OriginalLocation;
            return target;
        }
    }
}
