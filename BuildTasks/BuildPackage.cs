﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Versioning;
using Microsoft.Build.Framework;
using NLog;
using YAPM.Common;
using YAPM.Core.Utilities;
using YAPM.Management;
using YAPM.PackageBuilder;

namespace YAPM.BuildTasks
{
    /// <summary>
    /// A MSBuild task to construct a nupkg from a build.
    /// </summary>
    public class BuildPackage : TaskBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private ZippedPackage _Package;
        private FrameworkName _Framework;

        [Required]
        public string OutputDirectory { get; set; }

        [Required]
        public string PackageOutputPath { get; set; }

        [Required]
        public string AssemblyName { get; set; }

        [Required]
        public string SolutionDirectory { get; set; }

        [Required]
        public string ProjectFile { get; set; }

        [Required]
        public string TargetFrameworkVersion { get; set; }

        public string TargetFrameworkProfile { get; set; }


        public override bool Execute()
        {
            using (new BuildLogScope(Log, Path.GetFileNameWithoutExtension(ProjectFile)))
            {
                Logger.Info("Building package");
                _Framework = FrameworkVersionCompatibility.ParseVersionFromProject(TargetFrameworkVersion, TargetFrameworkProfile);
                var fullPath = Path.GetFullPath(ProjectFile);

                var project = MSBuildHelpers.FindMSBuildProject(fullPath);

                var assemblyReferences = project.GetAssemblyReferences().Select(i => i.Item2).ToArray();


                var solutionDirectory = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(fullPath), SolutionDirectory));
                var manager = new InstalledPackageManager(solutionDirectory);
                var md = GetMetadata(Path.Combine(Path.GetDirectoryName(ProjectFile), OutputDirectory, AssemblyName + ".dll"));

                _Package = new ZippedPackage(AssemblyName, md.Version)
                    {
                        Authors = md.Company,
                        Owners = md.Company,
                        Title = md.Title,
                        Description = md.Description
                    };

                var nuspec = Path.ChangeExtension(fullPath, "nuspec");
                if (File.Exists(nuspec))
                {
                    _Package.AddNuspec(File.ReadAllText(nuspec));
                }

                AddFileItem(AssemblyName + ".dll");
                AddFileItem(AssemblyName + ".pdb");

                foreach (var package in manager.InstalledPackges)
                {
                    var source = GetSource(package);
                    if (IsReferenced(assemblyReferences, source, package, manager, _Framework))
                    {
                        var targetPackage = package;
                        if (package.OriginalVersionNumber == null)
                        {
                            targetPackage = source.GetPackage(package.PackageId, package.Version.BaseVersion, null).Spec;
                        }
                        _Package.AddDependency(targetPackage);
                    }
                }

                var output = Path.Combine(PackageOutputPath, AssemblyName + ".nupkg");
                Logger.Info("Packaging files in to " + output);
                using (var file = File.Open(output, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
                {
                    file.SetLength(0);
                    _Package.Save(file);
                }
            }

            return true;
        }

        /// <summary>
        /// Adds a file to the nupkg
        /// </summary>
        /// <param name="item"></param>
        private void AddFileItem(string item)
        {
            var fullPath = Path.Combine(Path.GetDirectoryName(ProjectFile), OutputDirectory, item);
            Log.LogMessage(fullPath);
            if (!File.Exists(fullPath))
            {
                Log.LogMessage("File does not exist");
                return;
            }
            var extension = Path.GetExtension(item);
            
            switch (extension)
            {
                case ".dll":
                    _Package.AddAssembly(fullPath, item, _Framework);
                    break;
                case ".pdb":
                    _Package.AddAssembly(fullPath, item, _Framework);
                    break;
            }
        }


        /// <summary>
        /// Extracts metadata from a built DLL, using a temporary appdomain to load it.
        /// </summary>
        /// <param name="assemblyPath"></param>
        /// <returns></returns>
        private AssemblyMetadata GetMetadata(string assemblyPath)
        {
            var setup = new AppDomainSetup
            {
                ApplicationBase = Path.GetDirectoryName(BuildEngine.ProjectFileOfTaskNode)
            };

            AppDomain domain = AppDomain.CreateDomain("metadata", AppDomain.CurrentDomain.Evidence, setup);
            try
            {
                var t = typeof(MetadataExtractor);
                var extractor = (MetadataExtractor) domain.CreateInstanceAndUnwrap(t.Assembly.FullName, t.FullName);
                return extractor.GetMetadata(assemblyPath);
            }
            finally
            {
                AppDomain.Unload(domain);
            }
        }

        /// <summary>
        /// Handy class mostly taken from nuget to extract assembly data and return it over an appdomain boundry.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "It's constructed using CreateInstanceAndUnwrap in another app domain")]
        private sealed class MetadataExtractor : MarshalByRefObject
        {
            [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "It's a marshal by ref object used to collection information in another app domain")]
            [SuppressMessage("Microsoft.Reliability", "CA2001:AvoidCallingProblematicMethods", MessageId = "System.Reflection.Assembly.LoadFrom", Justification = "We need to load the assembly to extract metadata")]
            public AssemblyMetadata GetMetadata(string path)
            {
                Assembly assembly = Assembly.LoadFrom(path);
                AssemblyName assemblyName = assembly.GetName();

                return new AssemblyMetadata
                {
                    Name = assemblyName.Name,
                    Version = assemblyName.Version.ToString(),
                    Title = GetAttributeValueOrDefault<AssemblyTitleAttribute>(assembly, a => a.Title),
                    Company = GetAttributeValueOrDefault<AssemblyCompanyAttribute>(assembly, a => a.Company),
                    Description = GetAttributeValueOrDefault<AssemblyDescriptionAttribute>(assembly, a => a.Description),
                    Copyright = GetAttributeValueOrDefault<AssemblyCopyrightAttribute>(assembly, a => a.Copyright)
                };
            }

            private static string GetAttributeValueOrDefault<T>(Assembly assembly, Func<T, string> selector) where T : Attribute
            {
                // Get the attribute
                var attribute = assembly.GetCustomAttributes(true).OfType<T>().FirstOrDefault();
                if (attribute != null)
                {
                    string value = selector(attribute);
                    // Return the value only if it isn't null or empty so that we can use ?? to fall back
                    if (!String.IsNullOrEmpty(value))
                    {
                        return value;
                    }
                }
                return null;
            }
        }
    }

    [Serializable]
    internal class AssemblyMetadata
    {
        public string Name { get; set; }

        public string Version { get; set; }

        public string Title { get; set; }

        public string Company { get; set; }

        public string Description { get; set; }

        public string Copyright { get; set; }
    }
}
