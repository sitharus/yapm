﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Versioning;
using Microsoft.Build.Utilities;
using YAPM.Configuration;
using YAPM.Management;
using YAPM.Source;
using YAPM.Utilities;

namespace YAPM.BuildTasks
{
    public abstract class TaskBase : Task
    {
        private static readonly ConcurrentDictionary<string, CachingPackageSource> Sources = new ConcurrentDictionary<string, CachingPackageSource>();

        protected CachingPackageSource GetSource(PackageSpec spec)
        {
            if (!Sources.ContainsKey(spec.OriginalLocation))
            {
                Sources[spec.OriginalLocation] = new CachingPackageSource(new RemotePackageSource(spec.OriginalLocation));
            }
            return Sources[spec.OriginalLocation];
        }

        protected bool IsReferenced(IEnumerable<AssemblyName> assemblyReferences, IPackageSource source, PackageSpec package, InstalledPackageManager ipm, FrameworkName framework)
        {
            var dlls = ipm.DLLPaths(package, source, framework).Select(AssemblyName.GetAssemblyName);
            return dlls.Any(d => assemblyReferences.Any(a => a.Matches(d)));
        }
    }
}
