﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace YAPM.BuildTasks
{
    public class TaskLogTarget : TargetWithLayout
    {
        private readonly TaskLoggingHelper _Helper;

        public TaskLogTarget(TaskLoggingHelper helper)
        {
            _Helper = helper;
        }

        protected override void Write(LogEventInfo logEvent)
        {
            _Helper.LogMessage(MessageImportance.High, Layout.Render(logEvent));
        }

        internal static LoggingConfiguration StandardLogConfig(TaskLoggingHelper taskLoggingHelper, string prefix)
        {
            var target = new TaskLogTarget(taskLoggingHelper)
                {
                    Layout = prefix + ": ${level} ${message} ${exception:format=ToString,StackTrace}${newline}"
                };
            
            var logConfig = new LoggingConfiguration();
            logConfig.AddTarget("msbuild", target);
            logConfig.LoggingRules.Add(new LoggingRule("*", LogLevel.Info, target));
            return logConfig;
        }
    }

    public class BuildLogScope : IDisposable
    {
        private readonly LoggingConfiguration _OriginalConfiguration;

        public BuildLogScope(TaskLoggingHelper log, string projectFile)
        {
            _OriginalConfiguration = LogManager.Configuration;
            var config = TaskLogTarget.StandardLogConfig(log, Path.GetFileNameWithoutExtension(projectFile));
            if (_OriginalConfiguration != null)
            {
                var rules = new List<LoggingRule>(config.LoggingRules);
                foreach (var rule in rules)
                {
                    config.LoggingRules.Add(rule);
                }
            }
            LogManager.Configuration = config;
        }

        public void Dispose()
        {
            LogManager.Configuration = _OriginalConfiguration;
        }
    }

}
