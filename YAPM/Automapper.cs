﻿using AutoMapper;
using YAPM.Package;

namespace YAPM
{
    class Automapper
    {
        private static bool _Configured;
        private static void Configure()
        {
            if (_Configured) return;
            Mapper.CreateMap<IPackage, LocalPackage>().ForMember(dto => dto.LocalPath, opt => opt.Ignore())
                .ForMember(dto => dto.OriginalVersion, opt => opt.MapFrom(o => o.VersionString));
            _Configured = true;
        }

        public static TTo Map<TTo, TFrom>(TFrom from)
        {
            Configure();
            return Mapper.Map<TTo>(from);
        }
    }
}
