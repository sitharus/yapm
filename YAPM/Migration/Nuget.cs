﻿using System;
using System.Runtime.Versioning;
using System.Xml.Linq;
using NLog;
using YAPM.Management;
using YAPM.Package;
using YAPM.Source;

namespace YAPM.Migration
{
    /// <summary>
    /// Helper class for migration from nuget
    /// </summary>
    public class Nuget
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Migrate to nuget by reading a packages.config and installing all its packages.
        /// </summary>
        /// <param name="source">A caching package source</param>
        /// <param name="ipm">The installed package manager for the solution</param>
        /// <param name="config">The nuget packages.config</param>
        /// <param name="targetFramework">The .NET framework we're after</param>
        /// <param name="addReference">A callback to update references in the solution</param>
        public void Migrate(IPackageSource source, 
            InstalledPackageManager ipm, 
            XElement config, 
            FrameworkName targetFramework, 
            InstalledPackageManager.ModifyFile addReference)
        {
            Logger.Info("Beginning migration of nuget packages");
            foreach (var package in config.Descendants("package"))
            {
                var id = package.Attribute("id").Value;
                var version = new SemanticVersion(package.Attribute("version").Value);
                var newPackage = source.GetPackage(id, version, null);
                ipm.InstallPackage(newPackage, source, targetFramework, addReference);
            }
            Logger.Info("Nuget migration completed");
        }
    }
}
