﻿using System.ComponentModel;

namespace YAPM.TypeConversion
{
    /// <summary>
    /// Converts a string to a SemanticVersion for serialisation
    /// </summary>
    public class SemanticVersionTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, System.Type sourceType)
        {
            return sourceType == typeof(string);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            var versionString = value as string;
            if (versionString != null)
            {
                return new SemanticVersion(versionString);
            }
            return null;
        }
    }
}