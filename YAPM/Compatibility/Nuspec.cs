﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using YAPM.Configuration;

namespace YAPM.Compatibility
{
    /// <summary>
    /// A .nuspec file so we can read them to extract metadata
    /// </summary>
    public class Nuspec
    {
        public Nuspec(Stream file)
        {
            XElement doc = XDocument.Load(file).Root;
            ParseNuspec(doc);
        }

        public Nuspec(string spec)
        {
            XElement doc = XDocument.Parse(spec).Root;
            ParseNuspec(doc);
        }

        public PackageSpec[] Dependencies { get; set; }

        public string[] References { get; set; }

        public string Id { get; set; }

        public string Version { get; private set; }
        public string Description { get; private set; }
        public string Owners { get; private set; }
        public string Title { get; private set; }

        public string Tags { get; private set; }

        public string Authors { get; private set; }

        private void ParseNuspec(XElement doc)
        {
            XElement metadata = doc.Get("metadata");
            Id = metadata.Get("id").Value;

            XElement dependencies = doc.Get("dependencies");
            if (dependencies != null)
            {
                Dependencies = dependencies.GetAll("dependency").Select(d => new PackageSpec
                    {
                        PackageId = d.Attribute("id").Value,
                        Version = d.Attribute("version") != null
                                      ? new VersionConstraint(d.Attribute("version").Value, ConstraintType.AnyGreater)
                                      : new VersionConstraint("0.0.0.0", ConstraintType.AnyGreater),
                        Satisfier = new AssemblyPackageSatisifier(),
                        OriginalVersionNumber = d.Attribute("version") != null ? d.Attribute("version").Value : ""
                    }).ToArray();
            }

            XElement references = doc.Get("references");
            if (references != null)
            {
                References = references.GetAll("reference").Select(r => r.Attribute("file").Value).ToArray();
            }

            Version = doc.GetText("version");
            Description = doc.GetText("description");
            Owners = doc.GetText("owners");
            Authors = doc.GetText("authors");
            Tags = doc.GetText("tags");
            Title = doc.GetText("title");
        }
    }

    internal static class XDocHelpers
    {
        public static XElement Get(this XElement e, string name)
        {
            return e.Descendants().FirstOrDefault(d => d.Name.LocalName == name);
        }

        public static string GetText(this XElement e, string name)
        {
            XElement el = e.Descendants().FirstOrDefault(d => d.Name.LocalName == name);
            if (el == null)
            {
                return null;
            }
            return el.Value;
        }

        public static IEnumerable<XElement> GetAll(this XElement e, string name)
        {
            return e.Descendants().Where(d => d.Name.LocalName == name);
        }
    }
}
