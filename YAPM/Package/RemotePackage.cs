﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using YAPM.Configuration;
using YAPM.Source;

namespace YAPM
{
    /// <summary>
    /// A package on the package server.
    /// </summary>
    public class RemotePackage : IPackage
    {
        public RemotePackageEntity Source { get; set; }

        public RemotePackage(RemotePackageEntity source, RemotePackageSource resolver)
            : this(source, resolver, true)
        {
            
        }

        public RemotePackage(RemotePackageEntity source, RemotePackageSource resolver, bool includeDependencies)
        {
            _Resolver = resolver;
            Source = source;
            Id = source.Id;
            Version = new VersionConstraint(new SemanticVersion(source.Version), source.Version.StartsWith("[") ? ConstraintType.Fixed : ConstraintType.AnyGreater);
            _SourceVersion = source.Version;
            Title = source.Title;
            PackageHash = source.PackageHash;
            PackageHashAlgorithm = source.PackageHashAlgorithm;
            PackageSize = source.PackageSize;
            Authors = source.Authors != null ? source.Authors.Split(',').ToList() : new List<string>();
            Owners = source.Owners != null ? source.Owners.Split(',').ToList() : new List<string>();
            RequireLicenseAcceptance = source.RequireLicenseAcceptance;
            IsLatestVersion = source.IsLatestVersion;
            IsAbsoluteLatestVersion = source.IsAbsoluteLatestVersion;
            IsPrerelease = source.IsPrerelease;
            Description = source.Description;
            Summary = source.Summary;
            ReleaseNotes = source.ReleaseNotes;
            Language = source.Language;
            Tags = source.Tags != null ? source.Tags.Split(',').ToList() : new List<string>();
            Copyright = source.Copyright;
            IconUrl = source.IconUrl != null ? new Uri(source.IconUrl) : null;
            LicenseUrl = !string.IsNullOrWhiteSpace(source.LicenseUrl) ? new Uri(source.LicenseUrl) : null;
            ProjectUrl = !string.IsNullOrWhiteSpace(source.ProjectUrl) ? new Uri(source.ProjectUrl) : null;
            Created = source.Created;
            Published = source.Published;
            FrameworkAssemblies = new List<object>();
            _DependencyString = source.Dependencies;

            Dependencies = includeDependencies ? ResolveDependencies((id, v, t) => _Resolver.GetPackage(id, v), new string[0]) : null;
            TargettedFrameworks = new List<FrameworkName>();
            ReportAbuseUrl = source.ReportAbuseUrl != null ? new Uri(source.ReportAbuseUrl) : null;
            GalleryDetailsUrl = source.GalleryDetailsUrl != null ? new Uri(source.GalleryDetailsUrl) : null;
        }

        internal List<PackageSpec> ResolveDependencies(Func<string, SemanticVersion, string[], IPackage> resolvePackage, string[] trace)
        {
            return _DependencyString.Split('|').Where(s => !string.IsNullOrEmpty(s)).Select(s => s.Split(':')).Select(p =>
                {
                    var t = trace.Concat(new[] {string.Format("{0} {1}", Id, Version.BaseVersion)}).ToArray();
                    var package = resolvePackage(p[0], new SemanticVersion(p[1]), t);
                    return package == null ? null : package.Spec;
                }).Where(p => p != null).ToList();
        }

        private readonly string _DependencyString;

        private readonly RemotePackageSource _Resolver;

        private readonly string _SourceVersion;

        public string VersionString
        {
            get { return _SourceVersion; }
        }

        public string Id { get; private set; }
        public VersionConstraint Version { get; private set; }
        public string Title { get; private set; }
        public string PackageHash { get; private set; }
        public string PackageHashAlgorithm { get; private set; }
        public int PackageSize { get; private set; }
        public List<string> Authors { get; private set; }
        public List<string> Owners { get; private set; }
        public bool RequireLicenseAcceptance { get; private set; }
        public bool IsLatestVersion { get; private set; }
        public bool IsAbsoluteLatestVersion { get; private set; }
        public bool IsPrerelease { get; private set; }
        public string Description { get; private set; }
        public string Summary { get; private set; }
        public string ReleaseNotes { get; private set; }
        public string Language { get; private set; }
        public List<string> Tags { get; private set; }
        public string Copyright { get; private set; }
        public Uri IconUrl { get; private set; }
        public Uri LicenseUrl { get; private set; }
        public Uri ProjectUrl { get; private set; }
        public DateTimeOffset? Created { get; private set; }
        public DateTimeOffset? Published { get; private set; }
        public List<object> FrameworkAssemblies { get; private set; }
        public List<PackageSpec> Dependencies { get; private set; }
        public Uri ReportAbuseUrl { get; private set; }
        public Uri GalleryDetailsUrl { get; private set; }
        public List<FrameworkName> TargettedFrameworks { get; private set; }

        public PackageSpec Spec
        {
            get
            {
                return new PackageSpec
                    {
                        PackageId = Id,
                        Version = Version,
                        OriginalLocation = _Resolver.Location,
                        Satisfier = new AssemblyPackageSatisifier(),
                        OriginalVersionNumber = _SourceVersion 
                    };
            }
        }
    }
}