﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Versioning;
using System.Xml.Serialization;
using YAPM.Configuration;

namespace YAPM.Package
{
    /// <summary>
    /// A package that is in the local cache
    /// </summary>
    [Serializable]
    [DebuggerDisplay("Local Package {Id} {Version.BaseVersion}")]
    public class LocalPackage : IPackage
    {
        public string Id { get; set; }
        public string OriginalVersion { get; set; }
        public VersionConstraint Version { get; set; }
        public string Title { get; set; }
        public string PackageHash { get; set; }
        public string PackageHashAlgorithm { get; set; }
        public int PackageSize { get; set; }
        public bool RequireLicenseAcceptance { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
        public string ReleaseNotes { get; set; }
        public string Language { get; set; }
        public string Copyright { get; set; }
        public List<string> Authors { get; set; }
        public List<string> Owners { get; set; }
        public List<string> Tags { get; set; }

        [XmlIgnore]
        public Uri IconUrl { get; set; }
        [XmlIgnore]
        public Uri LicenseUrl { get; set; }
        [XmlIgnore]
        public Uri ProjectUrl { get; set; }
        [XmlIgnore]

        public string VersionString
        {
            get { return OriginalVersion; }
        }


        [XmlElement("IconUrl")]
        public string IconUrlString
        {
            get { return IconUrl == null ? null : IconUrl.AbsoluteUri; }
            set { IconUrl = value == null ? null : new Uri(value); }
        }

        [XmlElement("LicenseUrl")]
        public string LicenseUrlString
        {
            get { return LicenseUrl == null ? null : LicenseUrl.AbsoluteUri; }
            set { LicenseUrl = value == null ? null : new Uri(value); }
        }

        [XmlElement("ProjectUrl")]
        public string ProjectUrlString
        {
            get { return ProjectUrl == null ? null : ProjectUrl.AbsoluteUri; }
            set { ProjectUrl = value == null ? null : new Uri(value); }
        }

        public DateTimeOffset? Created { get; set; }
        public DateTimeOffset? Published { get; set; }
        public List<object> FrameworkAssemblies { get; set; }
        public List<PackageSpec> Dependencies { get; set; }

        [XmlIgnore]
        public List<FrameworkName> TargettedFrameworks { get; set; }

        [XmlElement("TargettedFrameworks")]
        public List<string> TargettedFrameworksStrings
        {
            get { return TargettedFrameworks == null ? null : TargettedFrameworks.Select(t => t.FullName).ToList(); }
            set { TargettedFrameworks = value == null ? null : value.Select(t => new FrameworkName(t)).ToList(); }
        }


        public PackageSpec Spec
        {
            get
            {
                return new PackageSpec
                {
                    PackageId = Id,
                    Version = Version,
                    Satisfier = new AssemblyPackageSatisifier(),
                    OriginalVersionNumber = OriginalVersion
                };
            }
        }

        public string LocalPath { get; set; }
    }
}
