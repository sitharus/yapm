﻿using System;
using System.ComponentModel;
using System.Linq;
using YAPM.TypeConversion;

namespace YAPM
{
    /// <summary>
    /// A semantic version is a version string that has a priority assigned
    /// to each number. Majors aren't binary compatible, but minors are.
    /// 
    /// This is extended to have a subbuild and branch identifier.
    /// 
    /// There are also some nuget quirks accounted for such as versions
    /// with [] and , in them.
    /// </summary>
    [Serializable]
    [TypeConverter(typeof(SemanticVersionTypeConverter))]
    public class SemanticVersion : IComparable<SemanticVersion>, IEquatable<SemanticVersion>
    {
        public string Value { get; set; }

        public int Major { get; set; }
        public int Minor { get; set; }
        public int Build { get; set; }
        public int SubBuild { get; set; }
        public string Branch { get; set; }

        private SemanticVersion()
        {
            
        }

        public SemanticVersion(string version)
        {
            if (string.IsNullOrEmpty(version))
            {
                return;
            }
            if (version.StartsWith("["))
            {
                version = version.Substring(1, version.Length - 2);
            }
            if (version.Contains(","))
            {
                version = version.Split(',')[0];
            }
            if (version.Contains("-"))
            {
                var parts = version.Split('-');
                version = parts[0];
                Branch = parts[1];
            }
            var split = version.Split('.').Select(Int32.Parse).ToArray();
            Major = split[0];
            Minor = split.Length >= 2 ? split[1] : 0;
            Build = split.Length >= 3 ? split[2] : 0; 
            SubBuild = split.Length >= 4 ? split[3] : 0;
            Value = StringValue();
        }

        public override string ToString()
        {
            return StringValue();
        }

        private string StringValue()
        {
            return String.Format("{0}.{1}.{2}.{3}{4}", Major, Minor, Build, SubBuild, string.IsNullOrEmpty(Branch) ? "" : "-" + Branch);
        }

        public int CompareTo(SemanticVersion other)
        {
            if (Major > other.Major)
            {
                return 1;
            }
            if (other.Major > Major)
            {
                return -1;
            }

            if (Minor > other.Minor)
            {
                return 1;
            }
            if (other.Minor > Minor)
            {
                return -1;
            }

            if (Build > other.Build)
            {
                return 1;
            }
            if (other.Build > Build)
            {
                return -1;
            }

            if (SubBuild > other.SubBuild)
            {
                return 1;
            }
            if (other.SubBuild > SubBuild)
            {
                return -1;
            }

            return 0;
        }

        public bool Equals(SemanticVersion other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Major == Major && other.Minor == Minor && other.Build == Build && other.SubBuild == SubBuild && other.Branch == Branch;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (SemanticVersion)) return false;
            return Equals((SemanticVersion) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = Major;
                result = (result*397) ^ Minor;
                result = (result*397) ^ Build;
                result = (result*397) ^ SubBuild;
                if (Branch != null)
                    result = (result*397) ^ Branch.GetHashCode();
                return result;
            }
        }

        public static bool operator ==(SemanticVersion left, SemanticVersion right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(SemanticVersion left, SemanticVersion right)
        {
            return !Equals(left, right);
        }
    }
}