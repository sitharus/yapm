﻿using System;
using System.Data.Services.Common;

namespace YAPM
{
    [DataServiceKey("Id", "Version")]
    [EntityPropertyMapping("Id", SyndicationItemProperty.Title, SyndicationTextContentKind.Plaintext, keepInContent: false)]
    [EntityPropertyMapping("Authors", SyndicationItemProperty.AuthorName, SyndicationTextContentKind.Plaintext, keepInContent: false)]
    [EntityPropertyMapping("Summary", SyndicationItemProperty.Summary, SyndicationTextContentKind.Plaintext, keepInContent: false)]
    public class RemotePackageEntity
    {
        public string Id { get; set; }
        public string Version { set; get; }
        public string Title { get; set; }
        public string PackageHash { get; set; }
        public string PackageHashAlgorithm { get; set; }
        public int PackageSize { get; set; }
        public string Authors { get;  set; }
        public string Owners { get;  set; }
        public bool RequireLicenseAcceptance { get; set; }
        public bool IsLatestVersion { get; set; }
        public bool IsAbsoluteLatestVersion { get; set; }
        public bool IsPrerelease { get; set; }
        public string Description { get;  set; }
        public string Summary { get;  set; }
        public string ReleaseNotes { get;  set; }
        public string Language { get;  set; }
        public string Tags { get;  set; }
        public string Copyright { get;  set; }
        public string FrameworkAssemblies { get; set; }
        public string Dependencies { get; set; }
        public DateTimeOffset? Created { get; set; }
        public DateTimeOffset? Published { get; set; }
        public int DownloadCount { get; set; }
        public int VersionDownloadCount { get; set; }
        public string ReportAbuseUrl { get; set; }
        public string GalleryDetailsUrl { get; set; }
        public string IconUrl { get; set; }
        public string LicenseUrl { get; set; }
        public string ProjectUrl { get; set; }
    }
}