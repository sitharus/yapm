﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Versioning;
using YAPM.Configuration;

namespace YAPM
{
    public interface IPackage
    {
        string Id { get; }
        string VersionString { get; }
        VersionConstraint Version { get; }
        string Title { get; }
        string PackageHash { get; }
        string PackageHashAlgorithm { get; }
        int PackageSize { get; }
        bool RequireLicenseAcceptance { get; }
        string Description { get; }
        string Summary { get; }
        string ReleaseNotes { get; }
        string Language { get; }
        string Copyright { get; }

        List<string> Authors { get; }
        List<string> Owners { get; }
        List<string> Tags { get; }

        Uri IconUrl { get; }
        Uri LicenseUrl { get; }
        Uri ProjectUrl { get; }

        DateTimeOffset? Created { get; }
        DateTimeOffset? Published { get; }

        List<object> FrameworkAssemblies { get; }
        List<PackageSpec> Dependencies { get; }
        List<FrameworkName> TargettedFrameworks { get; }

        PackageSpec Spec { get; }
    }
}
