﻿using System;
using System.Diagnostics.Contracts;

namespace YAPM
{
    [Serializable]
    public enum ConstraintType
    {
        // Fixed version - we must have exactly the version specified
        Fixed,
        // Same major version - 2.0, 2.1, 2.3.4 etc are compatible, but 1.0 and 3.0 are not
        SameMajor,
        // Any greater value is acceptable.
        AnyGreater
    }

    /// <summary>
    /// A version constraint, this specifies a version of a package and what other
    /// versions could be used instead.
    /// </summary>
    /// <remarks>
    /// <para>
    ///     This is used to resolve the problem of two packages referencing the same
    ///     dependency, but different versions. Packages can now specify that they
    ///     need at least version 1.0, but not 2.0 etc.
    /// </para>
    /// </remarks>
    [Serializable]
    public class VersionConstraint
    {
        /// <summary>
        /// The underlying version we actually want
        /// </summary>
        public SemanticVersion BaseVersion { get; set; }

        /// <summary>
        /// The constraint that lets us take another version
        /// </summary>
        public ConstraintType ConstraintType { get; set; }

        private VersionConstraint()
        {
            // For serialization.
        }

        public VersionConstraint(SemanticVersion baseVersion, ConstraintType constraintType)
        {
            Contract.Requires(baseVersion != null);
            ConstraintType = constraintType;
            BaseVersion = baseVersion;
        }

        public VersionConstraint(string baseVersion, ConstraintType constraintType)
            : this(new SemanticVersion(baseVersion), constraintType)
        {
        }

        public bool Equals(VersionConstraint other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.BaseVersion, BaseVersion) && Equals(other.ConstraintType, ConstraintType);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (VersionConstraint)) return false;
            return Equals((VersionConstraint) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (BaseVersion.GetHashCode()*397) ^ ConstraintType.GetHashCode();
            }
        }

        public static bool operator ==(VersionConstraint left, VersionConstraint right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(VersionConstraint left, VersionConstraint right)
        {
            return !Equals(left, right);
        }

        /// <summary>
        /// Checks if this is compatible with another version. See ConstraintType for details.
        /// </summary>
        /// <param name="requiredVersion"></param>
        /// <returns></returns>
        public bool CompatibleWith(SemanticVersion requiredVersion)
        {
            switch (ConstraintType)
            {
                case ConstraintType.Fixed:
                    return BaseVersion == requiredVersion;
                case ConstraintType.SameMajor:
                    return BaseVersion.Major == requiredVersion.Major && requiredVersion.CompareTo(BaseVersion) >= 0;
                case ConstraintType.AnyGreater:
                    return requiredVersion.CompareTo(BaseVersion) >= 0;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}