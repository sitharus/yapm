using System;
using System.Xml.Serialization;

namespace YAPM.Configuration
{
    public interface IPackageSatisfier : IComparable<IPackageSatisfier>
    {
    }

    [Serializable]
    [XmlInclude(typeof(AssemblyPackageSatisifier))]
    [XmlInclude(typeof(ProjectPackageSatisification))]
    public abstract class PackageSatisfier : IPackageSatisfier
    {
        public abstract int CompareTo(IPackageSatisfier other);

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PackageSatisfier) obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        protected bool Equals(PackageSatisfier other)
        {
            return false;
        }

        public static bool operator ==(PackageSatisfier left, PackageSatisfier right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PackageSatisfier left, PackageSatisfier right)
        {
            return !Equals(left, right);
        }
    }
}