﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace YAPM.Configuration
{
    [Serializable]
    public class Config 
    {
        public string Name;
        public string Version;
        public List<PackageSpec> InstalledPackages = new List<PackageSpec>();
        public List<PackageSpec> AllDependencies = new List<PackageSpec>();
        public List<PackageBuilder> ProvidesPackages = new List<PackageBuilder>();

    }
}