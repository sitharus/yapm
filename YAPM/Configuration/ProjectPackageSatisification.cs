﻿using System;
using System.Xml.Serialization;

namespace YAPM.Configuration
{
    /// <summary>
    /// Specifies that a package is resolved by a project in the current solution
    /// </summary>
    [Serializable]
    public class ProjectPackageSatisification : PackageSatisfier
    {
        [XmlAttribute]
        public string ProjectName { get; set; }

        public override int CompareTo(IPackageSatisfier other)
        {
            return 1;
        }

        protected bool Equals(ProjectPackageSatisification other)
        {
            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ProjectPackageSatisification)obj);
        }

        public override int GetHashCode()
        {
            return typeof(ProjectPackageSatisification).GetHashCode();
        }

        public static bool operator ==(ProjectPackageSatisification left, ProjectPackageSatisification right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ProjectPackageSatisification left, ProjectPackageSatisification right)
        {
            return !Equals(left, right);
        }
    }
}
