﻿using System;
using System.Xml.Serialization;

namespace YAPM.Configuration
{
    /// <summary>
    /// Specifies that a package is resolved by installing an assembly
    /// </summary>
    [Serializable]
    public class AssemblyPackageSatisifier : PackageSatisfier
    {
        public override int CompareTo(IPackageSatisfier other)
        {
            return 0;
        }

        protected bool Equals(AssemblyPackageSatisifier other)
        {
            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((AssemblyPackageSatisifier) obj);
        }

        public override int GetHashCode()
        {
            return typeof(AssemblyPackageSatisifier).GetHashCode();
        }

        public static bool operator ==(AssemblyPackageSatisifier left, AssemblyPackageSatisifier right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(AssemblyPackageSatisifier left, AssemblyPackageSatisifier right)
        {
            return !Equals(left, right);
        }
    }
}