using System;

namespace YAPM.Configuration
{
    /// <summary>
    /// A specification for an installed package
    /// </summary>
    [Serializable]
    public class PackageSpec
    {
        /// <summary>
        /// The package identifier, normally its name
        /// </summary>
        public string PackageId;

        /// <summary>
        /// The version of the package to look for, with constraints on what
        /// this can be replaced with
        /// </summary>
        public VersionConstraint Version;

        /// <summary>
        /// How the package can be found in the current solution
        /// </summary>
        public PackageSatisfier Satisfier;

        /// <summary>
        /// The original version number specified from the remote source, pre-normalisation.
        /// </summary>
        public string OriginalVersionNumber;

        /// <summary>
        /// If a downloaded package, where it was originally downloaded from
        /// </summary>
        public string OriginalLocation;

        protected bool Equals(PackageSpec other)
        {
            return string.Equals(PackageId, other.PackageId) && Equals(Version, other.Version);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PackageSpec) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((PackageId != null ? PackageId.GetHashCode() : 0)*397) ^ (Version != null ? Version.GetHashCode() : 0);
            }
        }

        public static bool operator ==(PackageSpec left, PackageSpec right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PackageSpec left, PackageSpec right)
        {
            return !Equals(left, right);
        }
    }
}