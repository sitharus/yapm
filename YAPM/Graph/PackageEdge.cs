﻿using System.Linq;
using YAPM.Core.Utilities;

namespace YAPM.Graph
{
    /// <summary>
    /// An edge between two packages. Handles matching up the
    /// verson and satisifier for the vertices.
    /// </summary>
    public class PackageEdge 
    {
        public void ResolveVersion(PackageVertex left, PackageVertex right)
        {
            if (left.TargettedVersion == null)
            {
                return; // TODO log?
            }

            if (right.TargettedVersion == null)
            {
                right.TargettedVersion = left.TargettedVersion;
            }
            else
            {
                var versions = new [] {left.TargettedVersion, right.TargettedVersion};
                right.TargettedVersion = FrameworkVersionCompatibility.HighestCompatibleVersion(versions.First(), versions);
            }
        }

        public void ResolveDependencyVersion(PackageVertex source, PackageVertex target)
        {
            var package = source.AllDependencies.First(d => d.PackageId == target.PackageId);
            target.AddConstraint(package.Version, package.Satisfier);
        }
    }
}