﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Versioning;
using NLog;
using YAPM.Configuration;
using YAPM.Core.Utilities;
using YAPM.Source;
using YAPM.Utilities;

namespace YAPM.Graph
{
    /// <summary>
    /// A vertex in the package graph. The graph should have one vertex per package id,
    /// and this vertex has all the version constraints as a set. The verticies then
    /// attempt to resolve their versions to the highest permissable version.
    /// </summary>
    public class PackageVertex
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly PackageSpec _InstalledPackage;
        private readonly IPackageSource _PackageSource;
        private readonly ISet<VersionConstraint> _AllowedVersions = new HashSet<VersionConstraint>();
        private readonly string _PackageId;

        public PackageSpec InstalledPackage
        {
            get { return _InstalledPackage; }
        }

        public string PackageId
        {
            get { return _PackageId; }
        }

        public IPackage Package
        {
            get { return _Package; }
        }

        private PackageSatisfier _CurrentSatisfier;
        private IPackage _Package;

        private FrameworkName _CurrentTarget;

        public FrameworkName TargettedVersion
        {
            get { return _CurrentTarget; }
            set { _CurrentTarget = FrameworkVersionCompatibility.HighestCompatibleVersion(value, _PossibleFrameworkNames); }
        }

        private List<FrameworkName> _PossibleFrameworkNames;

        /// <summary>
        /// The framework that we have to provide a package compatible with.
        /// Filled in on graph traversal.
        /// </summary>
        public FrameworkName RequiredCompatibleFramework { get; set; }

        /// <summary>
        /// If not null the resolver will try to keep this version of the spec active.
        /// </summary>
        public PackageSpec AnchorTo { get; set; }

        public PackageVertex(PackageSpec installedPackage, IPackageSource packageSource)
        {
            _InstalledPackage = installedPackage;
            _PackageSource = packageSource;
            _PackageId = installedPackage.PackageId;
            _CurrentSatisfier = installedPackage.Satisfier;
            _AllowedVersions.Add(installedPackage.Version);
            IsResolvable = true;
        }

        /// <summary>
        /// Gets the current package spec for this package. Will return something even if
        /// this isn't resolvable, not reliable until graph annotation is complete.
        /// </summary>
        public PackageSpec PackageSpec
        {
            get;
            set;
        }

        /// <summary>
        /// Returns all direct dependencies of the currently resolvable version.
        /// </summary>
        public ReadOnlyCollection<PackageSpec> AllDependencies
        {
            get { return _Package == null ? new PackageSpec[0].ToReadOnly() : _Package.Dependencies.ToReadOnly(); }
        }

        public ReadOnlyCollection<PackageSpec> DependenciesForAllVersions
        {
            get
            {
                lock (this)
                {
                    if (_CurrentSatisfier is ProjectPackageSatisification)
                    {
                        return new ReadOnlyCollection<PackageSpec>(new PackageSpec[0]);
                    }

                    var allVersions = new HashSet<PackageSpec>();
                    foreach (var version in _AllowedVersions)
                    {
                        var package = _PackageSource.GetPackage(_PackageId, version.BaseVersion, _PackageSource);
                        foreach (var spec in package.Dependencies)
                        {
                            allVersions.Add(spec);
                        }
                    }
                    return new ReadOnlyCollection<PackageSpec>(allVersions.ToList());
                }
            }
        }

        /// <summary>
        /// If true we can establish a single package that meets all of the constraints specified
        /// </summary>
        public bool IsResolvable
        {
            get;
            set;
        }

        internal void Resolve()
        {
            var targetVersion = ResolveVersion();
            if (targetVersion == null)
            {
                IsResolvable = false;
                Logger.Error("Cannot determine the version of " + PackageId);
                return;
            }
            IsResolvable = true;
            PackageSpec = new PackageSpec { PackageId = _PackageId, Satisfier = _CurrentSatisfier, Version = targetVersion, OriginalLocation = _PackageSource.Location };
            if (_CurrentSatisfier is ProjectPackageSatisification)
            {
                return;
            }
            _Package = _PackageSource.GetPackage(_PackageId, targetVersion.BaseVersion, null);
            _PossibleFrameworkNames = _Package.TargettedFrameworks;
        }

        /// <summary>
        /// Evaluates all the allowed version constrains and returns the highest allowable version, or null
        /// if a suitable version number cannot be found.
        /// </summary>
        /// <returns></returns>
        private VersionConstraint ResolveVersion()
        {
            // Projects are a fixed version, since they aren't really versioned.
            if (_CurrentSatisfier is ProjectPackageSatisification)
            {
                return new VersionConstraint("1.0.0.0", ConstraintType.Fixed);
            }

            // If we have any fixed versions we check to see that we only have one fixed version
            var allFixed = _AllowedVersions.Where(v => v.ConstraintType == ConstraintType.Fixed).ToList();
            if (allFixed.Count() > 1)
            {
                return null;
            }
            // And if we have only one fixed version we check to see that it's compatible with
            // all the other versions.
            if (allFixed.Any())
            {
                var requiredVersion = allFixed.First().BaseVersion;
                if (_AllowedVersions.All(v => v.CompatibleWith(requiredVersion)))
                {
                    return allFixed.First();
                }
                return null;
            }

            // If we have same major version constrains and we have different major versions in them
            // we return null
            var allSameMajor = _AllowedVersions.Where(v => v.ConstraintType == ConstraintType.SameMajor).ToArray();
            if (allSameMajor.Select(v => v.BaseVersion.Major).Distinct().Count() > 1)
            {
                return null;
            }
            // Else we find the lowest same major constraint and ensure that all other packages are compatible.
            if (allSameMajor.Any())
            {
                var minSameMajor = allSameMajor.OrderBy(v => v.BaseVersion).First();
                if (_AllowedVersions.All(v => minSameMajor.CompatibleWith(v.BaseVersion)))
                {
                    return _AllowedVersions.OrderByDescending(v => v.BaseVersion).First();
                }
                return null;
            }

            // Else we only have greater than constraints, so we return the highest version
            return _AllowedVersions.OrderByDescending(v => v.BaseVersion).First();
        }

        /// <summary>
        /// Adds a package to the version set for this node
        /// </summary>
        /// <param name="version">The package specification</param>
        /// <param name="satisfier"> </param>
        public bool AddConstraint(VersionConstraint version, PackageSatisfier satisfier)
        {
            lock (this)
            {
                if (_CurrentSatisfier is ProjectPackageSatisification || (_AllowedVersions.Contains(version) && satisfier == _CurrentSatisfier))
                {
                    return false;
                }

                if (satisfier is ProjectPackageSatisification)
                {
                    _AllowedVersions.Clear();
                    _AllowedVersions.Add(version);
                    _CurrentSatisfier = satisfier;
                    return true;
                }

                _AllowedVersions.Add(version);
                if (satisfier != null && (_CurrentSatisfier == null || satisfier.CompareTo(_CurrentSatisfier) > 0))
                {
                    _CurrentSatisfier = satisfier;
                }
                return true;
            }
        }
    }
}
