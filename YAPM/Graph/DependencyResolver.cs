﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using NLog;
using QuickGraph;
using QuickGraph.Algorithms.ConnectedComponents;
using QuickGraph.Algorithms.Search;
using YAPM.Configuration;
using YAPM.Source;

namespace YAPM.Graph
{
    /// <summary>
    /// Handles the resolution of the dependency graph. Given a root set it finds
    /// the set of dependencies that fit all the requirements specified.
    /// 
    /// If IsResolvable is false after construction then there are conflicted version
    /// constraints in the graph. Check UnresolvedDependencies for the problems.
    /// </summary>
    public class DependencyResolver
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly AdjacencyGraph<PackageVertex, TaggedEdge<PackageVertex, PackageEdge>> _packageGraph;
        private readonly ConcurrentDictionary<string, PackageVertex> _createdNodes = new ConcurrentDictionary<string, PackageVertex>();
        private readonly ISet<PackageVertex> _roots = new HashSet<PackageVertex>();
        private readonly IPackageSource _packageSource;

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="currentPackages">The root set of packages. This should only be the direct dependencies, not the whole set</param>
        /// <param name="packageSource">A package source that can resolve all dependencies</param>
        public DependencyResolver(ISet<PackageSpec> currentPackages, IPackageSource packageSource)
        {
            _packageSource = packageSource;
            _packageGraph = new AdjacencyGraph<PackageVertex, TaggedEdge<PackageVertex, PackageEdge>>();
            foreach (var package in currentPackages)
            {
                AddSpec(package);
            }
            ResolveGraph();
        }

        /// <summary>
        /// If true then all the dependencies have been resolved. If false then the graph can't be resolved
        /// and the ResolvedDependencies property won't be useful.
        /// </summary>
        public bool IsResolvable
        {
            get { return _packageGraph.Vertices.All(v => v.IsResolvable); }
        }

        /// <summary>
        /// If the graph is resolvable then a list of all the dependencies required in no particular order.
        /// If the graph isn't resolvable then an empty list.
        /// </summary>
        public IEnumerable<IPackage> ResolvedDependencies {
            get { return IsResolvable ? _packageGraph.Vertices.Select(v => v.Package).ToList() : new List<IPackage>(); }
        }

        /// <summary>
        /// A list of all the dependendies that cannot be resolved.
        /// </summary>
        // TODO: Constraint failure list
        public IEnumerable<PackageSpec> UnresolvedDependencies
        {
            get { return _packageGraph.Vertices.Where(v => v.IsResolvable == false).Select(v => v.PackageSpec).ToList(); }
        }

        /// <summary>
        /// Add a package spec to the graph and resolve it
        /// </summary>
        /// <param name="spec"></param>
        /// <returns></returns>
        public PackageSpec AddPackageWithSpec(PackageSpec spec)
        {
            var vertex = AddSpec(spec);
            ResolveGraph();
            return vertex.PackageSpec;
        }

        /// <summary>
        /// Update an existing package in the graph
        /// </summary>
        /// <param name="spec"></param>
        /// <returns></returns>
        public UpdateResult UpdatePackageWithSpec(PackageSpec spec)
        {
            var vertexs = UpdateSpec(spec);
            ResolveGraph();
            var requiredUpdates = _roots.Where(r => r.PackageSpec.Version != r.InstalledPackage.Version)
                                        .Select(r => new PackageUpdate
                                            {
                                                FromSpec = r.InstalledPackage,
                                                ToSpec = r.PackageSpec
                                            }).ToArray();
            return new UpdateResult(vertexs.Item1.PackageSpec, vertexs.Item2, requiredUpdates, this);
        }

        /// <summary>
        /// Remove a package from the graph
        /// </summary>
        /// <param name="spec"></param>
        /// <returns></returns>
        public PackageSpec RemovePackageWithSpec(PackageSpec spec)
        {
            return RemoveSpec(spec);
        }

        private void ResolveGraph()
        {
            AddAllDependencies();
            CheckForCycles();
            ResolvePackageVersions();
            AnnotateWithFrameworkVersions();
        }

        /// <summary>
        /// Add a spec from the given spec to the graph, checking if it's
        /// already there.
        /// </summary>
        /// <param name="spec"></param>
        /// <returns></returns>
        private PackageVertex AddSpec(PackageSpec spec)
        {
            var existing = _packageGraph.Vertices.FirstOrDefault(v => v.PackageId == spec.PackageId);
            if (existing != null)
            {
                existing.AddConstraint(spec.Version, spec.Satisfier);
                if (_roots.All(p => p.PackageId != spec.PackageId))
                {
                    _roots.Add(existing);
                }
                return existing;
            }

            var node = new PackageVertex(spec, _packageSource);
            _packageGraph.AddVertex(node);
            _createdNodes[spec.PackageId] = node;
            _roots.Add(node);
            return node;
        }

        /// <summary>
        /// Update the spec, this is really just adding and removing
        /// </summary>
        /// <param name="spec"></param>
        /// <returns></returns>
        private Tuple<PackageVertex, PackageSpec> UpdateSpec(PackageSpec spec)
        {
            var oldSpec = RemoveSpec(spec);
            return Tuple.Create(AddSpec(spec), oldSpec);
        }

        /// <summary>
        /// Remove the spec then clean the graph, removing orphaned packages
        /// </summary>
        /// <param name="spec"></param>
        /// <returns></returns>
        private PackageSpec RemoveSpec(PackageSpec spec)
        {
            var existing = _createdNodes[spec.PackageId];
            _roots.Remove(existing);
            _packageGraph.RemoveVertex(existing);
            CleanGraph();
            return existing.PackageSpec;
        }

        /// <summary>
        /// Searchs the graph and removes all packages that aren't reachable
        /// from the direct dependencies
        /// </summary>
        private void CleanGraph()
        {
            var vertices = _packageGraph.Edges.SelectMany(e => new[] {e.Source, e.Target}).Distinct().ToArray();
            var toRemove = _createdNodes.Values.Except(_roots).Except(vertices).ToList();
            PackageVertex removed;
            foreach (var node in toRemove)
            {
                _packageGraph.RemoveVertex(node);
                _createdNodes.TryRemove(node.PackageId, out removed);
            }

            var nullVertices = _packageGraph.Vertices.Where(r => r.PackageSpec == null).ToList();
            foreach (var node in nullVertices)
            {
                _packageGraph.RemoveVertex(node);
                _createdNodes.TryRemove(node.PackageId, out removed);
            }

            var nullRoots = _roots.Where(r => r.PackageSpec == null).ToList();
            foreach (var r in nullRoots)
            {
                _roots.Remove(r);
            }
        }

        /// <summary>
        /// Start from the direct dependencies and recurse through adding all
        /// their dependencies
        /// </summary>
        private void AddAllDependencies()
        {
            Logger.Info("Adding all dependencies");

            var topLevel = _createdNodes.Values.ToList();
            bool packagesAddedToGraph;
            do
            {
                packagesAddedToGraph = topLevel.Aggregate(false, (current, node) => AddDependencies(node) || current);
            } while (packagesAddedToGraph);
        }

        /// <summary>
        /// Add the dependencies of vertext to the graph
        /// </summary>
        /// <param name="vertex"></param>
        /// <returns>true if new verticies or package versions were added to the graph</returns>
        private bool AddDependencies(PackageVertex vertex)
        {
            Logger.Info("Adding dependencies for "+ vertex.PackageId);
            vertex.Resolve();
            var dependencies = vertex.DependenciesForAllVersions;
            return dependencies.Aggregate(false, (current, dep) => AddDependency(vertex.PackageSpec, dep) || current);
        }

        /// <summary>
        /// Adds dependency dep to the graph, finding an existing vertex
        /// or creating new one as needed
        /// </summary>
        /// <param name="packageSpec">the parent package</param>
        /// <param name="dep">the dependency</param>
        /// <returns>True if a vertex has been added or altered</returns>
        private bool AddDependency(PackageSpec packageSpec, PackageSpec dep)
        {
            bool didAddSpec = false;
            bool isNew = false;

            _createdNodes.AddOrUpdate(dep.PackageId, key =>
            {
                var newNode = new PackageVertex(dep, _packageSource);
                isNew = true;
                lock (_packageGraph)
                {
                    _packageGraph.AddVertex(newNode);
                }
                return newNode;
            }, (key, old) =>
            {
                didAddSpec = _createdNodes[dep.PackageId].AddConstraint(dep.Version, dep.Satisfier);
                return old;
            });

            lock (_packageGraph)
            {
                _packageGraph.AddEdge(new TaggedEdge<PackageVertex, PackageEdge>(_createdNodes[packageSpec.PackageId], _createdNodes[dep.PackageId], new PackageEdge()));
            }
            if (isNew)
            {
                AddDependencies(_createdNodes[dep.PackageId]);
            }
            return isNew || didAddSpec;
        }

        /// <summary>
        /// Check to see if there are any cycles in the graph. We need a
        /// DAG so cycles are bad!
        /// </summary>
        private void CheckForCycles()
        {
            var connectionFinder =
                new StronglyConnectedComponentsAlgorithm<PackageVertex, TaggedEdge<PackageVertex, PackageEdge>>(_packageGraph);
            int strongConnections = connectionFinder.ComponentCount;
            if (strongConnections > 0)
            {
                //var connections = connectionFinder.Components;
                // TODO: error message
                Logger.Error("Detected cycles in the graph");
                throw new InvalidOperationException("This graph has cycles!");
            }
        }

        /// <summary>
        /// Go through the graph and find the highest possible version
        /// for every reference.
        /// </summary>
        private void ResolvePackageVersions()
        {
            var search = new BreadthFirstSearchAlgorithm<PackageVertex, TaggedEdge<PackageVertex, PackageEdge>>(_packageGraph);
            search.StartVertex += packageNode => packageNode.Resolve();
            search.ExamineEdge += edge => edge.Tag.ResolveDependencyVersion(edge.Source, edge.Target);
        }

        /// <summary>
        /// Find the framework version allowable for each reference, this should
        /// be the lowest common version.
        /// </summary>
        private void AnnotateWithFrameworkVersions()
        {
            // breadth first search!
            var search = new BreadthFirstSearchAlgorithm<PackageVertex, TaggedEdge<PackageVertex, PackageEdge>>(_packageGraph);
            search.ExamineEdge += edge => edge.Tag.ResolveVersion(edge.Source, edge.Target);
        }

        /// <summary>
        /// Gets a flattened list of all the specs required in the graph
        /// </summary>
        public List<PackageSpec> FlattenedDependencyGraph
        {
            get
            {
                return _packageGraph.Vertices.Select(v => v.PackageSpec).OrderBy(p => p.PackageId).ToList();
            }
        }
        /// <summary>
        /// Gets a list of root level specs
        /// </summary>
        public List<PackageSpec> DirectDependencies
        {
            get { return _roots.Select(r => r.PackageSpec).OrderBy(p => p.PackageId).ToList(); }
        }

        public AdjacencyGraph<PackageVertex, TaggedEdge<PackageVertex, PackageEdge>> Graph
        {
            get { return _packageGraph; }
        }
    }

    public class PackageUpdate
    {
        public PackageSpec FromSpec { get; set; }
        public PackageSpec ToSpec { get; set; }

        public string PackageId
        {
            get { return FromSpec.PackageId; }
        }

        public string FromVersion
        {
            get { return FromSpec.Version.BaseVersion.ToString(); }
        }

        public string ToVersion
        {
            get { return ToSpec.Version.BaseVersion.ToString(); }
        }
    }

    public class UpdateResult
    {
        public PackageSpec NewSpec { get; private set; }
        public PackageSpec OldSpec { get; private set; }
        public DependencyResolver Graph { get; private set; }
        public ICollection<PackageUpdate> RequiredUpdates { get; set; }

        public UpdateResult(PackageSpec newSpec, PackageSpec oldSpec, ICollection<PackageUpdate> requiredUpdates, DependencyResolver graph)
        {
            NewSpec = newSpec;
            OldSpec = oldSpec;
            RequiredUpdates = requiredUpdates;
            Graph = graph;
        }
    }
}
