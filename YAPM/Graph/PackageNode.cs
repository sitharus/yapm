﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.Versioning;
using YAPM.Configuration;

namespace YAPM.Graph
{
    public class PackageNode
    {
        private readonly ISet<VersionConstraint> _AllowedVersions = new HashSet<VersionConstraint>();
        private readonly string packageId;
        private IPackageSatisfier _CurrentSatisfier;

        /// <summary>
        /// The framework that we have to provide a package compatible with.
        /// Filled in on graph traversal.
        /// </summary>
        public FrameworkName RequiredCompatibleFramework { get; set; }

        public PackageNode(PackageSpec initialPackage)
        {
            packageId = initialPackage.PackageId;
            _CurrentSatisfier = initialPackage.Satisfier;
            _AllowedVersions.Add(initialPackage.Version);
        }

        /// <summary>
        /// Gets the current package spec for this package. Will return something even if
        /// this isn't resolvable, not reliable until graph annotation is complete.
        /// </summary>
        public PackageSpec PackageSpec
        {
            get { return new PackageSpec { PackageId = packageId, Satisfier = _CurrentSatisfier, Version = ResolveVersion() }; }
        }

        /// <summary>
        /// Returns all direct dependencies of the currently resolvable version.
        /// </summary>
        public List<PackageSpec> AllDependencies
        {
            get { return null; }
        }

        /// <summary>
        /// If true we can establish a single package that meets all of the constraints specified
        /// </summary>
        public bool IsResolvable
        {
            get { return false; }
        }

        /// <summary>
        /// Evaluates all the allowed version constrains and returns the highest allowable version, or null
        /// if a suitable version number cannot be found.
        /// </summary>
        /// <returns></returns>
        private VersionConstraint ResolveVersion()
        {
            // If we have any fixed versions we check to see that we only have one fixed version
            var allFixed = _AllowedVersions.Where(v => v.ConstraintType == ConstraintType.Fixed).ToList();
            if (allFixed.Count() > 1)
            {
                return null;
            }
            // And if we have only one fixed version we check to see that it's compatible with
            // all the other versions.
            if (allFixed.Any())
            {
                var requiredVersion = allFixed.First().BaseVersion;
                if (_AllowedVersions.All(v => v.CompatibleWith(requiredVersion)))
                {
                    return allFixed.First();
                }
                return null;
            }

            // If we have same major version constrains and we have different major versions in them
            // we return null
            var allSameMajor = _AllowedVersions.Where(v => v.ConstraintType == ConstraintType.SameMajor);
            if (allSameMajor.Select(v => v.BaseVersion.Major).Distinct().Count() > 1)
            {
                return null;
            }
            // Else we find the lowest same major constraint and ensure that all other packages are compatible.
            if (allSameMajor.Any())
            {
                var minSameMajor = allSameMajor.OrderBy(v => v.BaseVersion).First();
                if (_AllowedVersions.All(v => minSameMajor.CompatibleWith(v.BaseVersion)))
                {
                    return _AllowedVersions.OrderByDescending(v => v.BaseVersion).First();
                }
                return null;
            }

            // Else we only have greater than constraints, so we return the highest version
            return _AllowedVersions.OrderByDescending(v => v.BaseVersion).First();
        }

        /// <summary>
        /// Adds a package to the version set for this node
        /// </summary>
        /// <param name="package">The package specification</param>
        public void AddPackage(PackageSpec package)
        {
            Contract.Assume(package.PackageId == packageId);
            _AllowedVersions.Add(package.Version);
            if (package.Satisfier.CompareTo(_CurrentSatisfier) > 0)
            {
                _CurrentSatisfier = package.Satisfier;
            }
        }
    }
}
