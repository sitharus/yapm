﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using NLog;

namespace YAPM.Utilities
{
    /// <summary>
    /// Use a file on disk to implement cross-process mutexes on files. It does this by
    /// using a file on disk to provide an atomic operation to synchronise on. 
    /// 
    /// This is a spinlock.
    /// </summary>
    public class FileWithLock : IDisposable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly string _Path;
        private readonly FileMode _Mode;
        private readonly FileAccess _Access;
        private readonly bool _Exponential;
        private readonly string _LockFile;
        private FileStream _Value;
        private bool _Deleted;


        /// <summary>
        ///  Initialiser for the lock with standard values for wait time and retries
        /// </summary>
        /// <param name="path"></param>
        /// <param name="mode"></param>
        /// <param name="access"></param>
        private FileWithLock(string path, FileMode mode, FileAccess access) : this(path, mode, access, 100, 40, false)
        {

        }

        /// <summary>
        ///  Default constructor
        /// </summary>
        /// <param name="path">The path to create a lock file for</param>
        /// <param name="mode">The mode to open the locked file with</param>
        /// <param name="access">The file access type to open the locked file with</param>
        /// <param name="waitTime">How long to wait between attempts to acquire the lock, in milliseconds</param>
        /// <param name="tries">The number of attempts to lock the file before we time out</param>
        /// <param name="exponential">If true uses a simple exponential backoff algorithm on lock acquiring</param>
        private FileWithLock(string path, FileMode mode, FileAccess access, int waitTime, int tries, bool exponential)
        {
            LockTime = waitTime;
            LockTries = tries;
            _Path = path;
            _Mode = mode;
            _Access = access;
            _Exponential = exponential;
            _LockFile = Path.GetFullPath(path + ".lock");
            Logger.Trace("Acquiring lock file " + _LockFile);
            if (GetLock()) return;

            // If we fail to obtain the lock we check to see if a dead
            // process has left the file in place, if so we delete it.
            try
            {
                var content = File.ReadAllText(_LockFile);
                int pid;
                if (Int32.TryParse(content, out pid))
                {
                    try
                    {
                        Logger.Trace("Clearing stale lock file at " + _Path);
                        Process.GetProcessById(pid);
                    }
                    catch (ArgumentException)
                    {
                        File.Delete(_LockFile);
                        if (GetLock())
                        {
                            Logger.Info("Cleared stale lock file at " + _Path);
                            return;
                        }
                    }
                }
            }
            catch (IOException e)
            {
                Logger.ErrorException("Failed to gain lock file for " + _Path, e);
            }
            throw new Exception("Failed to gain lock file for " + _Path);
        }

        /// <summary>
        /// Attempt to get the lock by opening the lockfile with exclusive access, sleeping
        /// for LockTime between attempts to open it.
        /// </summary>
        /// <returns></returns>
        private bool GetLock()
        {
            for (var i = 0; i < LockTries; i++)
            {
                if (!File.Exists(_LockFile))
                {
                    try
                    {
                        using (FileStream l = File.Open(_LockFile, FileMode.CreateNew, FileAccess.Write, FileShare.None))
                        using (var tw = new StreamWriter(l))
                        {
                            tw.Write(Process.GetCurrentProcess().Id);
                        }
                        Logger.Trace("Acquired lock file " + _LockFile);
                        return true;
                    }
                    catch (IOException)
                    {
                        // This happens if another thread/process has created the file before us
                    }
                }
                Thread.Sleep(LockTime);
                if (_Exponential)
                {
                    LockTime += LockTime;
                }
            }
            return false;
        }

        /// <summary>
        /// The number of times we'll attempt to gain the lock file before failing
        /// </summary>
        public int LockTries { get; set; }

        /// <summary>
        /// The time to wait between attempts to acquire the lock file
        /// </summary>
        public int LockTime { get; set; }

        /// <summary>
        /// The filestream for the file guarded by the lockfile. Opened lazily.
        /// </summary>
        public FileStream Value
        {
            get
            {
                if (_Deleted)
                {
                    throw new InvalidOperationException("Attempting to access synchronised file after release");
                }
                return _Value ?? (_Value = File.Open(_Path, _Mode, _Access, FileShare.None));
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Make sure we delete the lock file if someone
        /// doesn't wrap this with a using. Finaliser is
        /// justified by the lock file being an unmanaged
        /// resource.
        /// </summary>
        ~FileWithLock()
        {
            Dispose(false);
        }

        private void Dispose(bool fromDispose)
        {
            if (fromDispose && _Value != null)
            {
                _Value.Close();
            }

            if (!_Deleted && File.Exists(_LockFile))
            {
                File.Delete(_LockFile);
                _Deleted = true;
                Logger.Trace("Lock file " + _LockFile + " released ");
            }
        }

        public static FileWithLock Open(string path, FileMode mode, FileAccess access)
        {
            return new FileWithLock(path, mode, access);
        }

        public static FileWithLock Open(string path, FileMode mode, FileAccess access, int waitTime, int tries, bool exponential)
        {
            return new FileWithLock(path, mode, access, waitTime, tries, exponential);
        }
    }
}
