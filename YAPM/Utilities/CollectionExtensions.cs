﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace YAPM.Utilities
{
    /// <summary>
    /// Helper to create a readonly collection from any enumerable
    /// </summary>
    public static class CollectionExtensions
    {
        public static ReadOnlyCollection<T> ToReadOnly<T>(this IEnumerable<T> e)
        {
            return e.ToList().AsReadOnly();
        }
    }
}
