﻿using System;
using System.Linq;

namespace YAPM.Utilities
{
    /// <summary>
    /// Utilities to help compare assemblies
    /// </summary>
    public static class AssemblyNameExtensions
    {
        public static bool Matches(this System.Reflection.AssemblyName name1, System.Reflection.AssemblyName name2)
        {
            return name1.Name.Equals(name2.Name, StringComparison.OrdinalIgnoreCase) &&
                   EqualsIfNotNull(name1.Version, name2.Version) &&
                   EqualsIfNotNull(name1.CultureInfo, name2.CultureInfo) &&
                   EqualsIfNotNull(name1.GetPublicKeyToken(), name2.GetPublicKeyToken(), Enumerable.SequenceEqual);
        }

        private static bool EqualsIfNotNull<T>(T obj1, T obj2)
        {
            return EqualsIfNotNull(obj1, obj2, (a, b) => a.Equals(b));
        }

        private static bool EqualsIfNotNull<T>(T obj1, T obj2, Func<T, T, bool> equals)
        {
            // If both objects are non null do the equals
            if (obj1 != null && obj2 != null)
            {
                return @equals(obj1, obj2);
            }

            // Otherwise consider them equal if either of the values are null
            return true;
        }
    }
}
