﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Text.RegularExpressions;

namespace YAPM.Core.Utilities
{
    /// <summary>
    /// Utilities to help with comparing .NET framework version
    /// </summary>
    public class FrameworkVersionCompatibility
    {
        /// <summary>
        /// Determines if the framework specified in applicationVersion can load a 
        /// DLL compiled against referenceVersion
        /// </summary>
        /// <param name="applicationVersion"></param>
        /// <param name="referenceVersion"></param>
        /// <returns></returns>
        public static bool IsCompatible(FrameworkName applicationVersion, FrameworkName referenceVersion)
        {
            // If we don't have any versions we are compatible (probably)
            if (applicationVersion == null && referenceVersion == null)
            {
                return true;
            }

            // If either (but not both) is null then we can't check compatibility
            if (applicationVersion == null || referenceVersion == null)
            {
                return false;
            }

            // Silverlight can only load Silverlight assemblies for our purposes
            if ((applicationVersion.Identifier != "Silverlight" && referenceVersion.Identifier == "Silverlight")
                || (applicationVersion.Identifier == "Silverlight" && referenceVersion.Identifier != "Silverlight"))
            {
                return false;
            }

            // .NET full profile DLLs can execute client profile DLLs, but not the other way around
            if (applicationVersion.Profile == "Client" && referenceVersion.Profile != "Client")
            {
                return false;
            }

            // .NET 1.0/1.1 can talk to each other, but nothing else can
            if ((applicationVersion.Version.Major == 1 && referenceVersion.Version.Major != 1)
                || (referenceVersion.Version.Major == 1 && applicationVersion.Version.Major != 1))
            {
                return false;
            }

            // Otherwise higher level DLLs can call lower level DLLs.
            // (not strictly true - 2.0 can technically call 3.5 but we consider this an error)
            if (referenceVersion.Version > applicationVersion.Version)
            {
                return false;
            }

            // And if we get this far we're compatible!
            return true;
        }

        /// <summary>
        /// Determines the highest compatible framework for a given framework.
        /// </summary>
        /// <param name="sourceFramework">The framework we need to be compatible with</param>
        /// <param name="frameworks">The list of available frameworks</param>
        /// <returns>A FrameworkName, or null if no match could be found</returns>
        public static FrameworkName HighestCompatibleVersion(FrameworkName sourceFramework, IEnumerable<FrameworkName> frameworks)
        {
            return frameworks.Where(f => IsCompatible(sourceFramework, f)).OrderByDescending(f => f.Version).FirstOrDefault();
        }

        private static readonly Regex _Numbers = new Regex("^(\\d+)");

        /// <summary>
        /// Parses a nuget framework specifier (eg net40, sl20) in to
        /// the appropriate FrameworkName
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static FrameworkName ParseNameFromString(string s)
        {
            s = s.ToLower();

            var identifier = ".NETFramework";
            var profile = "";

            if (s.StartsWith("sl"))
            {
                identifier = "Silverlight";
                s = s.Substring(2);
            }

            if (s.StartsWith("net"))
            {
                s = s.Substring(3);
            }

            var version = new Version(1, 0);

            var match = _Numbers.Match(s);
            if (match.Captures.Count == 1)
            {
                var versionString = match.Captures[0].Value;
                switch (versionString)
                {
                    case "45":
                        version = new Version(4, 5);
                        break;
                    case "4":
                    case "40":
                        version = new Version(4, 0);
                        break;
                    case "35":
                        version = new Version(3, 5);
                        break;
                    case "3":
                    case "30":
                        version = new Version(3, 0);
                        break;
                    case "2":
                    case "20":
                        version = new Version(2, 0);
                        break;
                    case "11":
                        version = new Version(1, 1);
                        break;
                    case "1":
                    case "10":
                        version = new Version(1, 0);
                        break;
                }
            }

            if (s.EndsWith("client"))
            {
                profile = "Client";
            }

            return new FrameworkName(identifier, version, profile);
        }

        /// <summary>
        /// Builds a NuGet framework string from a FrameworkName
        /// </summary>
        /// <param name="framework"></param>
        /// <returns></returns>
        public static string NugetString(FrameworkName framework)
        {
            if (framework.Identifier != ".NETFramework")
            {
                throw new ArgumentException("Only.NET is supported", "framework");
            }
            var builder = new StringBuilder();
            builder.Append("net");
            builder.AppendFormat("{0}{1}", framework.Version.Major, framework.Version.Minor);
            if (framework.Profile == "Client")
            {
                builder.Append("client");
            }

            return builder.ToString();
        }

        /// <summary>
        /// Parses a FrameworkName from the two part identifier found in csproj files
        /// </summary>
        /// <param name="version"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        public static FrameworkName ParseVersionFromProject(string version, string profile)
        {
            var versionParts = version.Replace("v", "").Split('.').Select(Int32.Parse).ToList();

            var frameworkVersion = new Version(versionParts[0], versionParts[1], versionParts.Count > 2 ? versionParts[2] : 0);
            return new FrameworkName(".NETFramework", frameworkVersion, profile ?? "");
        }
    }
}
