﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Versioning;
using System.Security.Cryptography;
using System.Xml.Serialization;
using Ionic.Zip;
using NLog;
using YAPM.Compatibility;
using YAPM.Configuration;
using YAPM.Core.Utilities;
using YAPM.Graph;
using YAPM.Package;
using YAPM.Source;
using YAPM.Utilities;

namespace YAPM.Management
{
    public enum FileKind
    {
        Reference,
        Content
    }

    public class PackagesUpdatedEventArgs : EventArgs
    {
    }

    /// <summary>
    /// This class manages the interaction between the package graph and Visual Studio/BuildTasks
    /// </summary>
    public class InstalledPackageManager
    {
        /// <summary>
        ///     Callback to modify a reference in some manner
        /// </summary>
        /// <param name="entry">The file entry to add</param>
        public delegate void ModifyFile(ClassifiedEntry entry);

        public delegate void PackagesUpdated(PackagesUpdatedEventArgs ipm);

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly object InstallLock = new object();
        private readonly Config _Config;
        private readonly string _ConfigPath;
        private readonly string _LibraryPath;

        public InstalledPackageManager(string solutionPath)
        {
            OnPackagesUpdated += a => { };
            _ConfigPath = ConfigPath(solutionPath);
            _LibraryPath = LibraryPath(solutionPath);

            if (File.Exists(_ConfigPath) && new FileInfo(_ConfigPath).Length > 0)
            {
                using (FileStream stream = File.Open(_ConfigPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    var deserialiser = new XmlSerializer(typeof (Config));
                    _Config = (Config) deserialiser.Deserialize(stream);
                }
            }
            else
            {
                _Config = new Config();
            }
        }

        public PackageSpec[] InstalledPackges
        {
            get { return _Config.InstalledPackages.ToArray(); }
        }

        public string ConfigFilePath
        {
            get { return _ConfigPath; }
        }

        /// <summary>
        /// The full list of dependencies, including all indirect ones
        /// </summary>
        public List<PackageSpec> Dependencies
        {
            get { return _Config.AllDependencies; }
        }


        /// <summary>
        /// The list of dependencies required to build all projects in the
        /// solution.
        /// </summary>
        public List<PackageSpec> BuildDependencies
        {
            get { return _Config.InstalledPackages; }
        }

        /// <summary>
        /// The path to the unpacked packages
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <returns></returns>
        public static string LibraryPath(string solutionPath)
        {
            return Path.Combine(solutionPath, "YAPM Packages");
        }

        /// <summary>
        /// The path to the YAPM packages configuration file
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <returns></returns>
        public static string ConfigPath(string solutionPath)
        {
            return Path.Combine(solutionPath, "yapm.config");
        }

        public event PackagesUpdated OnPackagesUpdated;

        /// <summary>
        /// Save the configuration file
        /// </summary>
        public void Save()
        {
            using (FileStream stream = File.Open(_ConfigPath, FileMode.OpenOrCreate, FileAccess.Write))
            {
                stream.SetLength(0);
                var serialiser = new XmlSerializer(typeof (Config));
                serialiser.Serialize(stream, _Config);
            }
        }

        /// <summary>
        ///     Installs the given package to the project
        /// </summary>
        /// <param name="local">The package to install</param>
        /// <param name="source">The source to get details from</param>
        /// <param name="baseFramework">The .NET framework the project is targetting</param>
        /// <param name="addFile">A callback to add files to the project</param>
        public void InstallPackage(LocalPackage local, IPackageSource source, FrameworkName baseFramework, ModifyFile addFile)
        {
            lock (InstallLock)
            {
                Logger.Info("Installing package " + local.Id);
                string packagePath = GetPackagePath(local);

                var newSpec = new PackageSpec
                {
                    PackageId = local.Id,
                    Version = local.Version,
                    Satisfier = new AssemblyPackageSatisifier()
                };

                DependencyResolver graph = CreateResolver(source);
                graph.AddPackageWithSpec(newSpec);
                if (graph.IsResolvable)
                {
                    _Config.InstalledPackages = graph.DirectDependencies;
                    _Config.AllDependencies = graph.FlattenedDependencyGraph;

                    ExtractPackage(local, baseFramework, packagePath, addFile, false);

                    Save();
                    SendPackagesUpdated();
                }
                else
                {
                    throw new Exception("Could not resolve graph!");
                }
            }
        }

        /// <summary>
        /// Install a project instead of a binary package. This is for projects
        /// that have project -> package -> project dependency loops.
        /// </summary>
        /// <param name="package">The package spec for the project</param>
        /// <param name="source">The source to use to resolve other packages</param>
        public void InstallProjectFromSpec(PackageSpec package, IPackageSource source)
        {
            Logger.Info("Starting graph resolution");
            DependencyResolver graph = CreateResolver(source);
            graph.AddPackageWithSpec(package);
            if (graph.IsResolvable)
            {
                Logger.Info("Graph resolved, updating");
                _Config.InstalledPackages = graph.DirectDependencies;
                _Config.AllDependencies = graph.FlattenedDependencyGraph;
                Save();
                SendPackagesUpdated();
            }
            else
            {
                Logger.Info("Could not resolve graph! FIXME: add info");
                throw new Exception("Could not resolve graph!");
            }
        }

        /// <summary>
        /// Get the graph created by the dependency resolver for display or
        /// further analysis.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public DependencyResolver GetGraph(IPackageSource source)
        {
            DependencyResolver graph = CreateResolver(source);
            return graph;
        }

        /// <summary>
        ///     Unzips the package to the project's directory
        /// </summary>
        /// <param name="package"></param>
        public void RestorePackage(LocalPackage package)
        {
            string packagePath = GetPackagePath(package);
            ExtractZip(package, packagePath);
        }

        /// <summary>
        ///     Extracts the package to the packages directory, calling callbacks to add references when needed
        /// </summary>
        /// <param name="local"></param>
        /// <param name="baseFramework"></param>
        /// <param name="packagePath"></param>
        /// <param name="modifyFile"></param>
        /// <param name="forceExtraction"></param>
        private static void ExtractPackage(LocalPackage local, FrameworkName baseFramework, string packagePath, ModifyFile modifyFile, bool forceExtraction)
        {
            PackageEntries entries = ExtractAndClassifyZipEntries(local, baseFramework, packagePath, forceExtraction);
            CallbackForEntries(modifyFile, entries);
        }

        /// <summary>
        /// Invokes the modification callback for the entries in sequence.
        /// </summary>
        /// <param name="modifyFile"></param>
        /// <param name="entries"></param>
        private static void CallbackForEntries(ModifyFile modifyFile, PackageEntries entries)
        {
            foreach (ClassifiedEntry entry in entries.ContentFiles)
            {
                modifyFile(entry);
            }

            foreach (ClassifiedEntry fileReference in entries.DLLReferences)
            {
                modifyFile(fileReference);
            }
        }

        /// <summary>
        /// Reads the contents of the zip file and classifies them in to
        /// references or content files.
        /// </summary>
        /// <param name="local"></param>
        /// <param name="baseFramework"></param>
        /// <param name="packagePath"></param>
        /// <returns>A list of entries in the zip file that should be added to the project</returns>
        private static PackageEntries ClassifyZipEntries(LocalPackage local, FrameworkName baseFramework, string packagePath)
        {
            IEnumerable<ClassifiedEntry> entries;

            using (FileWithLock zipStream = FileWithLock.Open(local.LocalPath, FileMode.Open, FileAccess.Read))
            using (ZipFile zip = ZipFile.Read(zipStream.Value))
            {
                ZipEntry nuspec = zip.Entries.FirstOrDefault(e => e.FileName.EndsWith("nuspec"));
                IEnumerable<string> references = null;

                if (nuspec != null)
                {
                    Logger.Trace("Using nuspec to determine DLLs for {0}", local.Id);
                    using (Stream file = nuspec.OpenReader())
                    {
                        var spec = new Nuspec(file);
                        references = spec.References;
                    }
                }
                entries = zip.EntryFileNames.Select(c => ClassifyEntry(c, references, packagePath, local.Spec)).Where(c => c != null).ToArray();
            }
            return ClassifyEntries(baseFramework, entries);
        }

        /// <summary>
        /// Performs file classification then extracts to disk.
        /// </summary>
        /// <param name="local"></param>
        /// <param name="baseFramework"></param>
        /// <param name="packagePath"></param>
        /// <param name="forceExtraction"></param>
        /// <returns></returns>
        private static PackageEntries ExtractAndClassifyZipEntries(LocalPackage local, FrameworkName baseFramework, string packagePath, bool forceExtraction)
        {
            Logger.Trace("Extracting package {0} {1}", local.Id, local.Version.BaseVersion);
            string lib = Path.Combine(packagePath, "lib");
            if (Directory.Exists(lib) && !forceExtraction)
            {
                return ClassifyZipEntries(local, baseFramework, packagePath);
            }
            if (Directory.Exists(lib) && forceExtraction)
            {
                Directory.Delete(packagePath, true);
            }
            IEnumerable<ClassifiedEntry> entries = ExtractZip(local, packagePath);

            return ClassifyEntries(baseFramework, entries);
        }

        /// <summary>
        /// Classifies the entries in to content files or DLLs appropriate for the target framework required.
        /// </summary>
        /// <param name="baseFramework">The framework we're targetting</param>
        /// <param name="entries">The entries from the DLL</param>
        /// <returns>Content and DLLs appropriate for the target framework</returns>
        private static PackageEntries ClassifyEntries(FrameworkName baseFramework, IEnumerable<ClassifiedEntry> entries)
        {
            var allEntries = new PackageEntries
            {
                DLLReferences = new ClassifiedEntry[0],
                ContentFiles = new ClassifiedEntry[0]
            };

            ILookup<FileKind, ClassifiedEntry> grouped = entries.ToLookup(g => g.Kind, g => g);

            allEntries.ContentFiles = grouped[FileKind.Content].ToArray();


            Dictionary<MaybeFramework, IGrouping<FrameworkName, ClassifiedEntry>> byFramework = grouped[FileKind.Reference]
                .Where(r => r.IsReference)
                .GroupBy(r => r.Framework)
                .ToDictionary(r => (MaybeFramework) r.Key, r => r);

            MaybeFramework[] compatible = byFramework.Keys
                .Where(f => !f.HasValue || FrameworkVersionCompatibility.IsCompatible(baseFramework, f.Framework))
                .ToArray();
            if (compatible.Any())
            {
                MaybeFramework targetFramework = compatible
                    .OrderByDescending(f => f.HasValue ? f.Framework.Version : baseFramework.Version)
                    .First();
                allEntries.DLLReferences = byFramework[targetFramework].Where(fileReference => fileReference != null).ToArray();
            }
            return allEntries;
        }

        /// <summary>
        /// Extracts the zip to the package directory
        /// </summary>
        /// <param name="local"></param>
        /// <param name="packagePath"></param>
        /// <returns></returns>
        private static IEnumerable<ClassifiedEntry> ExtractZip(LocalPackage local, string packagePath)
        {
            IEnumerable<ClassifiedEntry> entries;
            using (FileWithLock zipStream = FileWithLock.Open(local.LocalPath, FileMode.Open, FileAccess.Read))
            using (ZipFile zip = ZipFile.Read(zipStream.Value))
            {
                zip.ExtractAll(packagePath, ExtractExistingFileAction.DoNotOverwrite);
                string nuspec = Directory.GetFiles(packagePath).FirstOrDefault(f => f.EndsWith("nuspec"));
                IEnumerable<string> references = null;

                if (nuspec != null)
                {
                    Logger.Trace("Using nuspec to determine DLLs for {0}", local.Id);
                    using (FileStream file = File.Open(nuspec, FileMode.Open, FileAccess.Read, FileShare.Delete))
                    {
                        var spec = new Nuspec(file);
                        references = spec.References;
                    }
                }
                entries = zip.EntryFileNames.Select(c => ClassifyEntry(c, references, packagePath, local.Spec)).Where(c => c != null).ToArray();
                Dictionary<string, ClassifiedEntry> l = entries.ToDictionary(c => c.EntryPath, c => c);
                IEnumerable<ZipEntry> files = zip.Entries.Where(c => l.ContainsKey(c.FileName));
                foreach (ZipEntry f in files)
                {
                    try
                    {
                        f.Extract(packagePath, ExtractExistingFileAction.OverwriteSilently);
                    }
                    catch (IOException)
                    {
                        Logger.Info("Failed to extract {0}, file is probably in use", f.FileName);
                    }
                }
            }
            return entries;
        }

        private static ClassifiedEntry ClassifyEntry(string s, IEnumerable<string> references, string packagePath, PackageSpec source)
        {
            string normalisedPath = s.Replace('/', Path.DirectorySeparatorChar);
            string entryPath = Path.Combine(packagePath, normalisedPath);
            if (s.StartsWith("lib") && s.EndsWith("dll"))
            {
                FrameworkName framework = null;
                string[] parts = s.Split('/');
                if (parts.Length >= 3)
                {
                    framework = FrameworkVersionCompatibility.ParseNameFromString(parts[1]);
                }
                return new ClassifiedEntry
                {
                    EntryPath = entryPath,
                    Framework = framework,
                    Kind = FileKind.Reference,
                    TargetPath = parts[parts.Length - 1],
                    IsReference = references == null || references.Any(normalisedPath.EndsWith),
                    Source = source
                };
            }
            if (s.StartsWith("content"))
            {
                return new ClassifiedEntry
                {
                    EntryPath = entryPath,
                    TargetPath = normalisedPath.Substring(8),
                    Kind = FileKind.Content,
                    Source = source
                };
            }

            if (s.EndsWith("dacpac"))
            {
                return new ClassifiedEntry
                {
                    EntryPath = entryPath,
                    Framework = null,
                    Kind = FileKind.Reference,
                    TargetPath = s.Split('/').Last(),
                    IsReference = references == null || references.Any(normalisedPath.EndsWith),
                    Source = source
                };
            }
            return null;
        }

        /// <summary>
        ///     Gets the path to the extrated packages directory
        /// </summary>
        /// <param name="local"></param>
        /// <returns></returns>
        private string GetPackagePath(LocalPackage local)
        {
            string packagePath = Path.Combine(_LibraryPath, local.Id);
            if (!Directory.Exists(packagePath))
            {
                Directory.CreateDirectory(packagePath);
            }
            return packagePath;
        }

        /// <summary>
        ///     Creates the dependency resolver for the given package source
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private DependencyResolver CreateResolver(IPackageSource source)
        {
            IEnumerable<PackageSpec> installedSpecs =
                _Config.InstalledPackages
                    .Select(p => _Config.AllDependencies
                        .First(d => d.PackageId == p.PackageId && d.Version == p.Version));

            var graph = new DependencyResolver(new HashSet<PackageSpec>(installedSpecs), source);
            return graph;
        }

        /// <summary>
        ///     Updates the given package the the version supplied
        /// </summary>
        /// <param name="baseFramework"></param>
        /// <param name="local"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public PackageSpec Update(FrameworkName baseFramework, LocalPackage local, IPackageSource source)
        {
            UpdateResult result = CheckUpdate(local, source);
            FinaliseUpdate(baseFramework, source, result);
            return result.NewSpec;
        }

        /// <summary>
        ///     Checks to see if the update will succeed
        /// </summary>
        /// <param name="local"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public UpdateResult CheckUpdate(LocalPackage local, IPackageSource source)
        {
            lock (InstallLock)
            {
                var newSpec = new PackageSpec
                {
                    PackageId = local.Id,
                    Version = local.Version,
                    Satisfier = new AssemblyPackageSatisifier(),
                    OriginalLocation = source.Location
                };

                DependencyResolver graph = CreateResolver(source);
                UpdateResult result = graph.UpdatePackageWithSpec(newSpec);
                if (graph.IsResolvable)
                {
                    return result;
                }
                Logger.Error("Failed to resolve graph while in update");
                throw new Exception("Graph is not resolvable");
            }
        }

        /// <summary>
        ///     Finalises an update checked by CheckUpdate
        /// </summary>
        /// <param name="baseFramework"></param>
        /// <param name="source"></param>
        /// <param name="result"></param>
        public void FinaliseUpdate(FrameworkName baseFramework, IPackageSource source,
            UpdateResult result)
        {
            PackageSpec newSpec = result.NewSpec;
            DependencyResolver graph = result.Graph;
            var package = source.GetPackage(newSpec.PackageId, newSpec.Version.BaseVersion, null);
            string packagePath = GetPackagePath(package);
            ExtractZip(package, packagePath);
            ExtractPackage(package, baseFramework, packagePath, e => { }, true);

            _Config.InstalledPackages = graph.DirectDependencies;
            _Config.AllDependencies = graph.FlattenedDependencyGraph;

            Save();
            SendPackagesUpdated();
        }

        /// <summary>
        ///     Extracts the paths of DLLs in the package useable by the supplied framework
        /// </summary>
        /// <param name="local"></param>
        /// <param name="baseFramework"></param>
        /// <returns></returns>
        public List<string> DLLPaths(LocalPackage local, FrameworkName baseFramework)
        {
            string packagePath = Path.Combine(_LibraryPath, local.Id);
            PackageEntries entries = ClassifyZipEntries(local, baseFramework, packagePath);
            return entries.DLLReferences.Select(e => e.EntryPath).ToList();
        }

        public List<string> DLLPaths(PackageSpec spec, IPackageSource source, FrameworkName baseFramework)
        {
            var package = source.GetPackage(spec.PackageId, spec.Version.BaseVersion, source);
            return DLLPaths(package, baseFramework);
        }

        /// <summary>
        ///     Removes the given package from the project
        /// </summary>
        public void RemovePackage(PackageSpec spec, IPackageSource source, Action<string> removeReference)
        {
            DependencyResolver graph = CreateResolver(source);
            PackageSpec removed = graph.RemovePackageWithSpec(spec);
            _Config.InstalledPackages = graph.DirectDependencies;
            _Config.AllDependencies = graph.FlattenedDependencyGraph;

            var package = source.GetPackage(removed.PackageId, removed.Version.BaseVersion, null);

            IEnumerable<string> entries;
            using (FileWithLock zipStream = FileWithLock.Open(package.LocalPath, FileMode.Open, FileAccess.Read))
            using (ZipFile zip = ZipFile.Read(zipStream.Value))
            {
                entries = zip.EntryFileNames.Where(s => s.StartsWith("lib"));
            }

            List<PackageEntry> dllEntries = (from entry in entries
                let parts = entry.Split('/')
                let normalisedPath = entry.Replace('/', Path.DirectorySeparatorChar)
                select
                    parts.Length == 3
                        ? new PackageEntry(normalisedPath, FrameworkVersionCompatibility.ParseNameFromString(parts[1]))
                        : new PackageEntry(normalisedPath, null)).ToList();


            foreach (PackageEntry reference in dllEntries)
            {
                removeReference(reference.Path);
            }
            Save();
            SendPackagesUpdated();
        }

        private void SendPackagesUpdated()
        {
            if (OnPackagesUpdated != null)
            {
                OnPackagesUpdated(new PackagesUpdatedEventArgs());
            }
        }

        /// <summary>
        ///     Removes a package if the project no longer has a reference to any of the contained DLLs
        /// </summary>
        /// <param name="path">The path of the reference, to determine if it's a managed reference</param>
        /// <param name="name">The DLL's file name</param>
        /// <param name="source">The package source to check against</param>
        /// <param name="allReferences">All the references in the current project</param>
        /// <param name="framework">The base framework for the project</param>
        public void RemovePackageIfUnreferenced(string path, string name, IPackageSource source, List<string> allReferences, FrameworkName framework)
        {
            string[] referencedDlls = allReferences.Select(Path.GetFileName).ToArray();
            IEnumerable<Tuple<LocalPackage, List<string>>> installedDlls = _Config.InstalledPackages
                .Select(p => source.GetPackage(p.PackageId, p.Version.BaseVersion, null))
                .Select(p => Tuple.Create(p, DLLPaths(p, framework)));

            if (string.IsNullOrEmpty(path))
            {
                string dllName = name + ".dll"; // Best guess
                RemoveIfUnused(source, referencedDlls, installedDlls, dllName);
            }
            else if (path.Contains(_LibraryPath))
            {
                string dllName = Path.GetFileName(path);
                RemoveIfUnused(source, referencedDlls, installedDlls, dllName);
            }
        }

        /// <summary>
        ///     Removes the package if it isn't referenced and no other DLLs from the package are referenced
        /// </summary>
        /// <param name="source">The package source to use</param>
        /// <param name="allReferences">All references in the containing project</param>
        /// <param name="installedDlls">All installed DLLs from all packages</param>
        /// <param name="dllName">The DLL we're checkign</param>
        private void RemoveIfUnused(IPackageSource source, IEnumerable<string> allReferences, IEnumerable<Tuple<LocalPackage, List<string>>> installedDlls, string dllName)
        {
            Tuple<LocalPackage, List<string>> package = installedDlls.FirstOrDefault(i => i.Item2.Any(p => p.EndsWith(dllName)));

            if (package != null)
            {
                string[] allDlls = package.Item2.Select(Path.GetFileName).ToArray();
                if (!allReferences.Any(allDlls.Contains))
                {
                    RemovePackage(package.Item1.Spec, source, s => { });
                }
            }
        }

        /// <summary>
        ///     Adds a reference to the config file if the given path is managed
        /// </summary>
        /// <param name="path"></param>
        /// <param name="source"></param>
        public void AddReferenceIfInLibrary(string path, IPackageSource source)
        {
            string fullLibrary = Path.GetFullPath(_LibraryPath);
            if (path.StartsWith(Path.GetFullPath(_LibraryPath)))
            {
                string relative = path.Substring(fullLibrary.Length + 1); // To trim off the starting \
                string[] parts = relative.Split(Path.DirectorySeparatorChar);
                string packageId = parts[0];
                string packageVerison = parts[1];
                var spec = new PackageSpec
                {
                    OriginalLocation = packageId,
                    Version = new VersionConstraint(new SemanticVersion(packageVerison), ConstraintType.AnyGreater)
                };
                DependencyResolver graph = CreateResolver(source);
                graph.AddPackageWithSpec(spec);
                if (graph.IsResolvable)
                {
                    _Config.InstalledPackages = graph.DirectDependencies;
                    _Config.AllDependencies = graph.FlattenedDependencyGraph;
                    Save();
                    SendPackagesUpdated();
                }
            }
        }

        /// <summary>
        /// Validates that a package's files on disk are the same as the
        /// ones in the zip.
        /// </summary>
        /// <param name="source"></param>
        public void ValidatePackageFiles(IPackageSource source)
        {
            foreach (PackageSpec spec in _Config.AllDependencies)
            {
                var package = source.GetPackage(spec.PackageId, spec.Version.BaseVersion, null);
                string extractPath = GetPackagePath(package);
                MD5 hash = MD5.Create();
                using (FileWithLock zipStream = FileWithLock.Open(package.LocalPath, FileMode.Open, FileAccess.Read))
                using (ZipFile zip = ZipFile.Read(zipStream.Value))
                {
                    string[] entries = zip.EntryFileNames.Where(s => s.StartsWith("lib")).ToArray();
                    foreach (string e in entries)
                    {
                        bool target = File.Exists(Path.Combine(extractPath, e));
                        if (target)
                        {
                            byte[] existingHash;
                            using (FileStream file = File.Open(e, FileMode.Open, FileAccess.Read, FileShare.Read))
                            {
                                existingHash = hash.ComputeHash(file);
                            }

                            hash.Clear();
                            byte[] zipHash = hash.ComputeHash(zip.Entries.First(z => z.FileName == e).InputStream);
                            if (existingHash != zipHash)
                            {
                                using (FileStream file = File.Open(e, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
                                {
                                    file.SetLength(0);
                                    zip.Entries.First(z => z.FileName == e).Extract(file);
                                }
                            }
                        }
                        else
                        {
                            using (FileStream file = File.Open(e, FileMode.Create, FileAccess.Write, FileShare.None))
                            {
                                zip.Entries.First(z => z.FileName == e).Extract(file);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets all DLL references in all dependencies for the given target framework
        /// </summary>
        /// <param name="source"></param>
        /// <param name="baseFramework"></param>
        /// <returns></returns>
        public IEnumerable<DllReference> GetAllDllReferences(IPackageSource source, FrameworkName baseFramework)
        {
            return _Config.AllDependencies
                .Where(spec => spec.Satisfier is AssemblyPackageSatisifier)
                .Select(spec => source.GetPackage(spec.PackageId, spec.Version.BaseVersion, null))
                .Select(local => ClassifyZipEntries(local, baseFramework, Path.Combine(_LibraryPath, local.Id)))
                .SelectMany(entry => entry
                    .DLLReferences
                    .Select(d => new DllReference(Path.GetFileNameWithoutExtension(d.EntryPath), d.EntryPath, d.Source)));
        }

        /// <summary>
        /// Checks if  a given path is within the packages directory
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool IsInPackagePath(string path)
        {
            if (path.StartsWith(_LibraryPath))
            {
                return true;
            }
            return false;
        }


        public class ClassifiedEntry
        {
            public string EntryPath { get; set; }

            public string TargetPath { get; set; }

            public FileKind Kind { get; set; }

            public FrameworkName Framework { get; set; }

            public bool IsReference { get; set; }

            public PackageSpec Source { get; set; }
        }

        internal class MaybeFramework
        {
            private MaybeFramework(FrameworkName framework)
            {
                Framework = framework;
            }

            public bool HasValue
            {
                get { return Framework != null; }
            }

            public FrameworkName Framework { get; private set; }

            public static implicit operator MaybeFramework(FrameworkName framework)
            {
                return new MaybeFramework(framework);
            }
        }

        public class PackageEntries
        {
            public ClassifiedEntry[] ContentFiles;
            public ClassifiedEntry[] DLLReferences;
        }

        internal class PackageEntry
        {
            internal PackageEntry(string path, FrameworkName framework)
            {
                Path = path;
                FrameworkName = framework;
            }

            public string Path { get; private set; }
            public FrameworkName FrameworkName { get; private set; }
        }
        /// <summary>
        ///     Unzips the package to the project's directory
        /// </summary>
        /// <param name="packageSource"></param>
        public void RestoreAllPackages(IPackageSource packageSource)
        {
            if (Directory.Exists(_LibraryPath)) 
            {
                try
                {
                    Directory.Delete(_LibraryPath, true);
                }
                catch (IOException)
                {
                }
            }
            Directory.CreateDirectory(_LibraryPath);

            foreach (var packageSpec in _Config.AllDependencies)
            {
                var package = packageSource.GetPackage(packageSpec.PackageId, packageSpec.Version.BaseVersion, packageSource);
                string packagePath = GetPackagePath(package);
                ExtractZip(package, packagePath);
            }
        }
    }
    
}
