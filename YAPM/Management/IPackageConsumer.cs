﻿using System.Collections.Generic;

namespace YAPM.Management
{
    /// <summary>
    /// A package consumer is the item that uses the packages, typically to compile a product.
    /// This will normally be a Visual Studio project of some kind.
    /// </summary>
    /// <remarks>
    /// It is expected that each type of project will need its own package consumer as
    /// they are implemented differently inside visual studio.
    /// </remarks>
    public interface IPackageConsumer
    {
        /// <summary>
        /// This should return all the references for the consumer, whether they're satisified
        /// by yapm or not. 
        /// </summary>
        List<object> RefersTo { get; }

        /// <summary>
        /// The path to the consumer. This is used to generate the relative paths for packages
        /// </summary>
        string BasePath { get; }

        /// <summary>
        /// Add a reference to the specified assembly. This does not need to do any management, you can assume
        /// that the package is fully resolved.
        /// </summary>
        /// <param name="path">A path relative to the BasePath</param>
        /// <remarks>
        /// <para>
        /// If an exception is thrown the the operation will be aborted. This could leave the project in an unknown state.
        /// </para>
        /// </remarks>
        void AddReference(string path);

        /// <summary>
        /// Remove a reference to the given assembly.
        /// </summary>
        /// <param name="path">A path relative to the BasePath</param>
        /// <remarks>
        /// <para>
        /// If an exception is thrown the the operation will be aborted. This could leave the project in an unknown state.
        /// </para>
        /// </remarks>
        void RemoveReference(string path);

    }
}