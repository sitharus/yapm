﻿using YAPM.Configuration;

namespace YAPM.Management
{
    /// <summary>
    /// A mapping between a DLL on disk and the package that contains it
    /// </summary>
    public class DllReference
    {
        internal DllReference(string name, string relativePath, PackageSpec sourcePackage)
        {
            SourcePackage = sourcePackage;
            Name = name;
            RelativePath = relativePath;
        }

        public string Name { get; private set; }
        public string RelativePath { get; private set; }
        public PackageSpec SourcePackage { get; private set; }
    }
}