﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using NLog;
using YAPM.Package;
using YAPM.Utilities;

namespace YAPM.Source
{
    /// <summary>
    /// Implements a caching package source that downloads packages from a server and
    /// saves them to disk. Since everything really needs a zipfile on disk this is the
    /// class everything uses. 
    /// </summary>
    /// <remarks>
    /// <para>
    /// This assume that a version of the package will never change. This should
    /// be enforced by application of a LART or clue-by-four.
    /// </para>
    /// </remarks>
    public class CachingPackageSource : IPackageSource
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly RemotePackageSource _Upstream;
        private readonly string _CacheLocation;
        private List<LocalPackage> _CurrentPackages;
        private static readonly object SaveLock = new object();
        private static DateTime? LastModification;

        public string Location
        {
            get { return _Upstream.Location; }
        }


        /// <summary>
        /// Standard constructor
        /// </summary>
        /// <param name="upstream">The fallback when we don't have a package in the cache</param>
        public CachingPackageSource(RemotePackageSource upstream)
        {
            _Upstream = upstream;
            string localAppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            _CacheLocation = Path.Combine(localAppDataPath, "YAPM", "PackageCache");
            if (!Directory.Exists(_CacheLocation))
            {
                Directory.CreateDirectory(_CacheLocation);
            }
            LoadPackages();
        }

        private string DatFile
        {
            get
            {
                return Path.Combine(_CacheLocation, "packages.dat");
            }
        }

        /// <summary>
        /// Loads the list of cached packages from the cache file
        /// </summary>
        private void LoadPackages()
        {
            lock (SaveLock)
            {
                var lastWrite = DateTime.MinValue;
                try
                {
                    lastWrite = File.GetLastWriteTime(DatFile);
                }
                catch (Exception)
                {
                }

                if (_CurrentPackages != null && LastModification == lastWrite && lastWrite > DateTime.MinValue) return;

                using (var fileWithLock = FileWithLock.Open(DatFile, FileMode.Open, FileAccess.Read))
                {
                    var packages = new List<LocalPackage>();
                    if (File.Exists(DatFile) && new FileInfo(DatFile).Length > 0)
                    {
                        var fmt = new XmlSerializer(typeof (PackageCache));
                        try
                        {
                            var cache = (PackageCache) fmt.Deserialize(fileWithLock.Value);
                            if (cache.Version >= PackageCache.CurrentVersion && cache.Packages != null)
                            {
                                packages = cache.Packages;
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                    Thread.MemoryBarrier();
                    _CurrentPackages = packages;
                }

                try
                {
                    LastModification = File.GetLastWriteTime(DatFile);
                }
                catch (Exception)
                {
                    LastModification = DateTime.MaxValue;
                }

            }
        }

        
        public LocalPackage GetPackage(string packageName, SemanticVersion version, IPackageSource dependencyResolverSource)
        {
            return GetPackage(packageName, version, dependencyResolverSource, new string[0]);
        }

        public LocalPackage GetPackage(string packageName, SemanticVersion version, IPackageSource dependencyResolverSource, string[] trace)
        {
            LoadPackages();
            var local = _CurrentPackages.FirstOrDefault(p => p.Id == packageName && p.Version.BaseVersion == version);
            if (local != null)
            {
                return local;
            }

            var traceString = String.Join(" -> ", trace);

            Logger.Info(String.Format("Package {0} {1} not found locally, downloading {2}", packageName, version, traceString));
            var remote = _Upstream.GetPackage(packageName, version);
            if (remote == null)
            {
                remote = _Upstream.VersionsForPackage(packageName)
                                  .Where(p => p.Version.BaseVersion.CompareTo(version) >= 0)
                                  .OrderBy(p => p.Version.BaseVersion)
                                  .FirstOrDefault();
                if (remote == null)
                {
                    throw new PackageNotFoundException(packageName, version);
                }

                if (version != remote.Version.BaseVersion)
                {
                    Logger.Warn(String.Format("Package {0} {1} not found, substituted with {2}", packageName, version, remote.Version.BaseVersion));
                }
            }
            AddToCache(remote);
            var remotePackage = remote as RemotePackage;
            if (remotePackage != null)
            {
                var deps = remotePackage.ResolveDependencies((id, v, t) => GetPackage(id, v, this, t), trace);
                _CurrentPackages.First(a => a.Id == remotePackage.Id && a.Version == remotePackage.Version).Dependencies = deps;
                
            }
            Save();

            return _CurrentPackages.First(a => a.Id == remote.Id && a.Version == remote.Version);
        }

        public LocalPackage GetLatestPackage(string packageId)
        {
            var remote = _Upstream.GetLatestPackage(packageId);
            return AddToCache(remote);
        }

        public IEnumerable<IPackage> SearchPackage(string searchString)
        {
            // Todo combine with local cache.
            return _Upstream.SearchPackage(searchString);
        }

        public IEnumerable<string> GetDLLsForPackage(LocalPackage package)
        {
            return null;
        }

        public Stream GetPackageSourceStream(LocalPackage package)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IPackage> VersionsForPackage(string packageId)
        {
            return _Upstream.VersionsForPackage(packageId);
        }

        /// <summary>
        /// Fetches a package frmo the remote source and downloads it to disk
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        private LocalPackage AddToCache(IPackage package)
        {
            using (var source = _Upstream.GetPackageSourceStream(package))
            {
                if (source != null)
                {
                    var destination = Path.Combine(_CacheLocation, package.Id);
                    if (!Directory.Exists(destination))
                    {
                        Directory.CreateDirectory(destination);
                    }
                    var packageFile = Path.Combine(destination, package.Version.BaseVersion.ToString()) + ".pkg";
                    var local = Automapper.Map<LocalPackage, IPackage>(package);
                    local.LocalPath = Path.GetFullPath(packageFile);

                    using (FileWithLock.Open(Path.Combine(_CacheLocation, "access"), FileMode.Create, FileAccess.Write))
                    {
                        // If we've already downloaded this file just use it.
                        if (!File.Exists(packageFile))
                        {
                            using (var file = File.OpenWrite(packageFile))
                            {
                                source.CopyTo(file);
                            }
                        }
                    }
                    _CurrentPackages.Add(local);
                    Save();
                    return local;
                }
                throw new PackageNotFoundException("", null);
            }
        }

        /// <summary>
        /// Saves the packages list to the dat file
        /// </summary>
        private void Save()
        {
            lock (SaveLock)
            {
                using (var stream = FileWithLock.Open(DatFile, FileMode.Create, FileAccess.ReadWrite))
                {
                    var fmt = new XmlSerializer(typeof (PackageCache));
                    if (stream.Value.Length > 0)
                    {
                        try
                        {
                            var inFile = (PackageCache) fmt.Deserialize(stream.Value);
                            if (inFile.Version < PackageCache.CurrentVersion)
                            {
                                foreach (var pkg in inFile.Packages.Where(pkg => !_CurrentPackages.Any(p => p.Id == pkg.Id && p.Version == pkg.Version)))
                                {
                                    _CurrentPackages.Add(pkg);
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }

                    stream.Value.Seek(0, SeekOrigin.Begin);
                    stream.Value.SetLength(0);

                    fmt.Serialize(stream.Value, new PackageCache(_CurrentPackages));
                }
            }
        }
    }

    class PackageCacheEqualityComparer : IEqualityComparer<LocalPackage>
    {
        public bool Equals(LocalPackage x, LocalPackage y)
        {
            return x.Id == y.Id && x.Version.BaseVersion == y.Version.BaseVersion;
        }

        public int GetHashCode(LocalPackage obj)
        {
            return obj.Id.GetHashCode() ^ (397*obj.Version.BaseVersion.GetHashCode());
        }
    }

    [Serializable]
    public class PackageCache
    {
        public const int CurrentVersion = 3;
        public int Version { get; set; }
        public List<LocalPackage> Packages { get; set; }

        public PackageCache(List<LocalPackage> packages)
        {
            Version = CurrentVersion;
            Packages = packages;
        }

        public PackageCache()
        {
            
        }

    }
}