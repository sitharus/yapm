﻿using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;

namespace YAPM.Source
{
    public class RemotePackageSource
    {
        public string ApiBase { get; private set; }
        private readonly DataServiceContext _Context;

        public string Location
        {
            get { return ApiBase; }
        }

        public RemotePackageSource(string apiBase)
        {
            Contract.Requires(apiBase != null);
            ApiBase = apiBase;
            _Context = new DataServiceContext(new Uri(apiBase))
            {
                MergeOption = MergeOption.OverwriteChanges
            };
        }

        public IPackage GetPackage(string packageName, SemanticVersion version)
        {
            return GetPackage(packageName, version, new string[0]);
        }

        public IPackage GetPackage(string packageName, SemanticVersion version, string[] trace)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(packageName));
            Contract.Requires(version != null);
            Contract.Ensures(Contract.Result<IEnumerable<IPackage>>() != null);
            var query = _Context.CreateQuery<RemotePackageEntity>("FindPackagesById");
            query = query.AddQueryOption("id", "'" + packageName + "'");
            return query.Where(p => p.Version == version.ToString()).Select(p => new RemotePackage(p, this, false)).FirstOrDefault();
        }

        public IPackage GetLatestPackage(string packageId)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(packageId));
            Contract.Ensures(Contract.Result<IEnumerable<IPackage>>() != null);
            var query = _Context.CreateQuery<RemotePackageEntity>("Search");
            query = query.AddQueryOption("includePrerelease", "false");
            query = query.AddQueryOption("searchTerm", "'" + packageId + "'");
            return query.Where(p => p.IsLatestVersion).Select(p => new RemotePackage(p, this)).FirstOrDefault();
        }

        public IEnumerable<IPackage> SearchPackage(string searchString)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(searchString));
            Contract.Ensures(Contract.Result<IEnumerable<IPackage>>() != null);
            var query = _Context.CreateQuery<RemotePackageEntity>("Search");
            query = query.AddQueryOption("searchTerm", "'" + searchString + "'");
            query = query.AddQueryOption("includePrerelease", "true");
            return query.Where(p => p.IsAbsoluteLatestVersion || p.IsLatestVersion)
                .Select(p => new RemotePackage(p, this, false))
                .ToList()
                .GroupBy(p => p.Id)
                .Select(p => p.OrderBy(i => i.IsLatestVersion).First())
                .ToList();
        }

        public Stream GetPackageSourceStream(IPackage package)
        {
            var r = package as RemotePackage;
            if (r != null)
            {
                var source = _Context.GetReadStream(r.Source);
                if (source!= null)
                {
                    return source.Stream;
                }
            }
            return null;
        }

        public IEnumerable<IPackage> VersionsForPackage(string packageId)
        {
            var query = _Context.CreateQuery<RemotePackageEntity>("FindPackagesById");
            var queryResults = ((DataServiceQuery<RemotePackageEntity>)
                                query.AddQueryOption("id", "'" + packageId + "'")
                                     .OrderByDescending(p => p.Version)
                                     .Take(400)).Execute();

            var packages = queryResults.Select(p => new RemotePackage(p, this, false)).ToList();
            while (((QueryOperationResponse)queryResults).GetContinuation() != null)
            {
                queryResults = _Context.Execute<RemotePackageEntity>(
                    ((QueryOperationResponse)queryResults).GetContinuation().NextLinkUri
                    );
                packages.AddRange(queryResults.Select(p => new RemotePackage(p, this, false)).ToList());

            }

            return packages;
        }

        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(!string.IsNullOrEmpty(ApiBase));
            Contract.Invariant(_Context != null);
        }
    }
}
