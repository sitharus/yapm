﻿using System;
using System.Collections.Generic;
using System.IO;
using YAPM.Package;

namespace YAPM.Source
{
    class LocalPackageSource : IPackageSource
    {
        private string RepositoryPath;

        public string Location
        {
            get { return null; }
        }
        
        public LocalPackage GetPackage(string packageName, SemanticVersion version, IPackageSource dependencyResolverSource)
        {
            return GetPackage(packageName, version, dependencyResolverSource, new string[0]);
        }

        public LocalPackage GetPackage(string packageName, SemanticVersion version, IPackageSource dependencyResolverSource, string[] trace)
        {
            throw new NotImplementedException();
        }

        public LocalPackage GetLatestPackage(string packageId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IPackage> SearchPackage(string searchString)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetDLLsForPackage(LocalPackage package)
        {
            throw new NotImplementedException();
        }

        public Stream GetPackageSourceStream(LocalPackage package)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IPackage> VersionsForPackage(string packageId)
        {
            throw new NotImplementedException();
        }
    }
}
