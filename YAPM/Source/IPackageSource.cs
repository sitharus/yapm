﻿using System.Collections.Generic;
using System.IO;
using YAPM.Package;

namespace YAPM.Source
{
    public interface IPackageSource
    {
        string Location { get; }
        LocalPackage GetPackage(string packageId, SemanticVersion version, IPackageSource dependencyResolverSource);
        LocalPackage GetPackage(string packageId, SemanticVersion version, IPackageSource dependencyResolverSource, string[] trace);
        LocalPackage GetLatestPackage(string packageId);
        IEnumerable<IPackage> SearchPackage(string searchString);
        IEnumerable<string> GetDLLsForPackage(LocalPackage package);
        Stream GetPackageSourceStream(LocalPackage package);

        IEnumerable<IPackage> VersionsForPackage(string packageId);
    }
}
