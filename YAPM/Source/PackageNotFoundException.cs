using System;

namespace YAPM.Source
{
    internal class PackageNotFoundException : Exception
    {
        public string PackageId { get; set; }
        public SemanticVersion Version { get; set; }

        public PackageNotFoundException(string packageId, SemanticVersion version)
            : base(String.Format("Could not find package {0} version {1}", packageId, version))
        {
            Version = version;
            PackageId = packageId;
        }
    }
}