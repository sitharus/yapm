﻿using System.IO;
using System.Xml.Linq;

namespace YAPM.NugetCompat
{
    internal class Nuspec
    {
        public string ID { get; set; }

        public string Version { get; set; }

        public string Title { get; set; }

        public string Authors { get; set; }

        public string Owners { get; set; }

        public string LicenseUrl { get; set; }

        public string ProjectUrl { get; set; }

        public bool RequireLicenseAcceptance { get; set; }

        public string Description { get; set; }

        public string Summary { get; set; }

        public string Language { get; set; }

        public string Tags { get; set; }

        public string[] References { get; set; }

        public static Nuspec FromFile(string path)
        {
            using (var file = File.Open(path, FileMode.Open, FileAccess.Read))
            {
                XDocument d = XDocument.Load(file);

                return null;
            }
        }
    }
}
