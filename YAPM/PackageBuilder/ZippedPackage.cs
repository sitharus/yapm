﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Versioning;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Ionic.Zip;
using YAPM.Compatibility;
using YAPM.Configuration;
using YAPM.Core.Utilities;
using YAPM.Management;

namespace YAPM.PackageBuilder
{
    public class ZippedPackage
    {
        private readonly string _PackageName;
        private string _Version;
        private readonly ZipFile _Zip;
        private static readonly XNamespace ns = "http://schemas.microsoft.com/packaging/2011/08/nuspec.xsd";
        private static readonly XNamespace coreProperties = "http://schemas.openxmlformats.org/package/2006/metadata/core-properties";
        private static readonly XNamespace dc = "http://purl.org/dc/elements/1.1/";
        private static readonly XNamespace ct = "http://schemas.openxmlformats.org/package/2006/content-types";
        private static readonly XNamespace rels = "http://schemas.openxmlformats.org/package/2006/relationships";

        private List<PackageSpec> _Dependencies = new List<PackageSpec>();
        private readonly string _CoreProperties = "package/services/metadata/core-properties/" + Guid.NewGuid().ToString("N") + ".psmdcp";
        private readonly string _SpecName;
        public string Authors { get; set; }
        public string Owners { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public ZippedPackage(string packageName, string version)
        {
            _PackageName = packageName;
            Title = _PackageName;
            _Version = version;
            _Zip = new ZipFile();
            _Zip.AddDirectoryByName("_rels");
            _Zip.AddDirectoryByName("lib");
            _Zip.AddDirectoryByName("tools");
            _SpecName = _PackageName + ".nuspec";
        }

        public void AddAssembly(string existing, string entryPath, FrameworkName version)
        {
            var frameworkString = FrameworkVersionCompatibility.NugetString(version);
            var fullEntryPath = Path.Combine("lib", frameworkString, entryPath);
            _Zip.AddFile(existing, Path.GetDirectoryName(fullEntryPath));
        }

        public void AddDependencies(InstalledPackageManager manager)
        {
            _Dependencies.AddRange(manager.BuildDependencies);
        }

        public void AddNuspec(string nuspec)
        {
            var spec = new Nuspec(nuspec);
            _Version = spec.Version ?? _Version;
            Title = spec.Title ?? Title;
            Authors = spec.Authors ?? Authors;
            Owners = spec.Owners ?? Owners;
            Description = spec.Description ?? Description;
            Tags = spec.Tags ?? Tags;
            _Dependencies.AddRange(spec.Dependencies);
        }

        protected string Tags { get; set; }

        private void AddManifest()
        {
            var root = new XElement(ns + "package");
            var metadata = new XElement(ns + "metadata");
            metadata.Add(new XElement(ns + "id", _PackageName));
            metadata.Add(new XElement(ns + "version", _Version));
            metadata.Add(new XElement(ns + "title", Title));
            metadata.Add(new XElement(ns + "authors", Authors));
            metadata.Add(new XElement(ns + "owners", Owners));
            metadata.Add(new XElement(ns + "requireLicenseAcceptance", "false"));
            metadata.Add(new XElement(ns + "description", Description));
            metadata.Add(new XElement(ns + "tags", Tags));
            var dependencies = new XElement(ns + "dependencies");
            foreach (var dep in _Dependencies)
            {
                dependencies.Add(new XElement(ns + "dependency",
                    new XAttribute("id", dep.PackageId),
                    new XAttribute("version", dep.OriginalVersionNumber)));
            }

            metadata.Add(dependencies);
            root.Add(metadata);

            var manifest = new XDocument(root);
            using (var buffer = new Utf8StringWriter())
            using (var writer = XmlWriter.Create(buffer, new XmlWriterSettings { Encoding = Encoding.UTF8, Indent = true}))
            {
                manifest.WriteTo(writer);
                writer.Flush();
                _Zip.AddEntry(_SpecName, buffer.GetStringBuilder().ToString(), Encoding.UTF8);
            }
        }

        private void AddCoreProperties()
        {
            var root = new XElement(coreProperties + "coreProperties", 
                new XElement(dc + "creator", Authors),
                new XElement(dc + "description", Description),
                new XElement(dc + "identifier", _PackageName),
                new XElement(coreProperties + "version", _Version),
                new XElement(coreProperties + "keywords", ""),
                new XElement(dc + "title", Title)
                );

            using (var buffer = new StringWriter())
            using (var writer = XmlWriter.Create(buffer, StandardXmlSettings))
            {
                root.WriteTo(writer);
                writer.Flush();
                _Zip.AddEntry(_CoreProperties, buffer.GetStringBuilder().ToString(), Encoding.Unicode);
            }
        }

        private void AddContentTypes()
        {
            var root = new XElement(ct + "Types",
                new XElement(ct + "Default",
                    new XAttribute("Extension", "rels"),
                    new XAttribute("ContentType", "application/vnd.openxmlformats-package.relationships+xml")),
                new XElement(ct + "Default",
                    new XAttribute("Extension", "nuspec"),
                    new XAttribute("ContentType", "application/octet")),
                new XElement(ct + "Default",
                    new XAttribute("Extension", "dll"),
                    new XAttribute("ContentType", "application/octet")),
                new XElement(ct + "Default",
                    new XAttribute("Extension", "psmdcp"),
                    new XAttribute("ContentType", "application/vnd.openxmlformats-package.core-properties+xml"))
                    );
            using (var buffer = new StringWriter())
            using (var writer = XmlWriter.Create(buffer, StandardXmlSettings))
            {
                root.WriteTo(writer);
                writer.Flush();
                _Zip.AddEntry("[Content_Types].xml", buffer.GetStringBuilder().ToString(), Encoding.Unicode);
            }
        }

        private void AddRelationships()
        {
            var root = new XElement(rels + "Relationships",
                new XElement(rels + "Relationship",
                    new XAttribute("Type", "http://schemas.microsoft.com/packaging/2010/07/manifest"),
                    new XAttribute("Target", "/" + _PackageName + ".nuspec"),
                    new XAttribute("Id", "Rf4b6a1e82c6a4fb2")),
                new XElement(rels + "Relationship",
                    new XAttribute("Type", "http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"),
                    new XAttribute("Target", _CoreProperties),
                    new XAttribute("Id", "R8a122b6ed8d84df4")));
            using (var buffer = new StringWriter())
            using (var writer = XmlWriter.Create(buffer, StandardXmlSettings))
            {
                root.WriteTo(writer);
                writer.Flush();
                _Zip.AddEntry("_rels/.rels", buffer.GetStringBuilder().ToString(), Encoding.Unicode);
            }

        }

        private XmlWriterSettings StandardXmlSettings
        {
            get
            {
                return new XmlWriterSettings
                    {
                        Indent = true,
                        Encoding = Encoding.Unicode
                    };
            }
        }

        public void Save(Stream output)
        {
            AddManifest();
            AddCoreProperties();
            AddContentTypes();
            AddRelationships();
            _Zip.Save(output);
        }

        public void AddDependency(PackageSpec package)
        {
            _Dependencies.Add(package);
        }

        // ARGH.
        public class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding { get { return Encoding.UTF8; } }
        }
    }
}
